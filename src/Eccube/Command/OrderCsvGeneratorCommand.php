<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Eccube\Command;

use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Eccube\Util\LogUtil;

/**
 * SFAシステム連携のCSVを出力するコマンド
 *
 */
class OrderCsvGeneratorCommand extends Command
{
    /** @var \Eccube\Application $app */
    protected $app;
    protected function configure()
    {
        $this
            ->setName('csv-generator')
            //->addUsage('--from="Y-m-d H:i:s" --to="Y-m-d H:i:s"')
            ->addOption('from', 'f', InputOption::VALUE_REQUIRED, 'If set, SQL Where From Date Add')
            ->addOption('to', 't', InputOption::VALUE_REQUIRED, 'If set, SQL Where To Date Add')
            ->setDescription('CSV Generator')
            ->setHelp(
                <<<EOF
The <info>%command.name%</info> command CSV Generator
Ex) Query Last Date
  <info>php %command.full_name%</info>

Ex) Query 2018-11-01 10:00:00 - 2018-11-01 18:00:00
  <info>php %command.full_name% --from="2018-11-01 10:00:00" --to="2018-11-02 18:00:00"</info>
EOF
            );
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var \Eccube\Application $app */
        $this->app = $this->getSilexApplication();

        $dt = new \DateTime();
        $dt->setTimezone(new \DateTimeZone('Asia/Tokyo'));
        $start = $dt->format('Y-m-d H:i:s');

        $from = $input->getOption('from');
        $to = $input->getOption('to');

        $this->writeLog("---------------------------------------------------------", $output, false);
        $this->writeLog("[開始]{$start}", $output, false);

        //引数エラー（ログに出せない）
        if (($from && !$to) || (!$from && $to)) {
            $output->writeln("<error>{引数は無し、もしくは2つ必要です。}</error>");
            return;
        }

        if (empty($from) && empty($to)) {
            $to = $dt->format('Y-m-d 00:00:00');
            $from = $dt->modify('-1 days')->format('Y-m-d 00:00:00');
        } else {
            $dt_from = $dt->createFromFormat('Y-m-d H:i:s', $from);
            $dt_to = $dt->createFromFormat('Y-m-d H:i:s', $to);

            if ($dt_from === false || $dt_to === false) {
                $erro_text = "[エラー] 引数が正しい日時ではありません。";
                $this->writeLog($erro_text, $output);
                return;
            }
            $from = $dt_from->format('Y-m-d H:i:s');
            $to = $dt_to->format('Y-m-d H:i:s');
        }

        if ($from > $to) {
            $erro_text = "[エラー] 第2引数が第1引数より小さい値が設定されました。";
            $this->writeLog($erro_text, $output);
            return;
        }

        try {
            $datas = $this->app['eccube.repository.order']->getOrderCsvData($this->app['orm.em'], $from, $to);
        } catch (\Exception $e) {
            $this->writeLog("データの取得でエラーが発生しました。詳細:\n{$e->getMessage()}\n{$e->getTraceAsString()}", $output);
            return;
        }
        try {
            $this->putCsv($datas);
        } catch (\Exception $e) {
            $this->writeLog("CSVファイルの出力でエラーが発生しました。", $output);
            return;
        }

        try {
            $file_name = config('command_log_output_path') . '/sfa/ecshop_sales.end';
            LogUtil::putNewFile("", $file_name);
        } catch (\Exception $e) {
            $this->writeLog("Endファイルの出力でエラーが発生しました。", $output);
            return;
        }

        $this->writeLog("CSV出力処理は正常に終了しました。", $output, false);
        $this->writeLog("[終了] {$dt->modify('now')->format('Y-m-d H:i:s')}", $output, false);
        $this->writeLog("---------------------------------------------------------", $output, false);
    }

    public function putCsv($rows)
    {

        $csvFileName = config('csv_output_path') . '/sfa/ecshop_sales.csv';

        try {
            $fp = fopen($csvFileName, 'a');//新規作成可・オープン時に削除しない
        } catch (\Exception $e) {
            throw $e;
        }

        if (!flock($fp, LOCK_EX)) {
            throw new \Exception("ファイルロックできませんでした");
        }

        ftruncate($fp, 0); //ファイルの中身を削除
        foreach ($rows as $row) {
            $cols = implode(',', $this->removeNgStrInCSV($row));
            fwrite($fp, $this->convertSJIS($cols) . "\n"); //ファイルの中身を出力
            fflush($fp);//バッファを強制書き出し
        }
        flock($fp, LOCK_UN);
        fclose($fp);
    }

    public function convertSJIS($row_str)
    {
        return mb_convert_encoding($row_str, 'SJIS-win', 'UTF-8');
    }

    public function removeNgStrInCSV($row_str)
    {
        return str_replace(array("\r\n", "\r", "\n", ","), '', $row_str);
    }

    public function writeLog($body, OutputInterface $output, $is_error = true)
    {
        $text = <<<EOF
{$body}
EOF;
        $file_name = config('command_log_output_path') . '/sfa/' . date("ym") . '_ecshop_sales.log';
        LogUtil::putFile($text, $file_name);
        if ($is_error) {
            $output->writeln("<error>{$body}</error>");
        }
    }
}
