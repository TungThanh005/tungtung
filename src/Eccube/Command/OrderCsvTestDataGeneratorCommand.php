<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Eccube\Command;

use Eccube\Doctrine\Common\DataFixtures\ForTest\MemberFixture;
use Eccube\Doctrine\Common\DataFixtures\ForTest\BaseInfoFixture;
use Eccube\Doctrine\Common\DataFixtures\ForTest\OrderFixture;
use Eccube\Doctrine\Common\DataFixtures\ForTest\ShippingFixture;
use Eccube\Doctrine\Common\DataFixtures\ForTest\ShippingItemFixture;

use Eccube\Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Eccube\Doctrine\Common\DataFixtures\ORMLoader;

use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;

class OrderCsvTestDataGeneratorCommand extends Command
{
    protected function configure()
    {
        /*
            10000回実行する例
            i=1; while [ $i -le 10000 ]; do php app/console csv-test-fixture; i=$(expr $i + 1);done
        */
        $this
            ->setName('csv-test-fixture')
            ->addUsage('--pre_truncate=true')
            ->addOption('pre_truncate', null, InputOption::VALUE_NONE, 'If set, truncate target table data')
            ->setDescription('CSV TestData Generator')
            ->setHelp(
                <<<EOF
The <info>%command.name%</info> command CSV TestData Generator,

Ex) 
  <info>php %command.full_name%</info>

Ex) 
  <info>php %command.full_name% --pre_truncate=true</info>
EOF
            );
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var \Eccube\Application $app */
        $app = $this->getSilexApplication();

        if (empty($app['debug'])) {
            $output->writeln("<error>This Command DebugMode Only.</error>");
            return;
        }

        $is_pre_truncate = $input->getOption('pre_truncate');

        if ($is_pre_truncate) {
            $sql = '
                set foreign_key_checks = 0;
                TRUNCATE TABLE dtb_member;
                TRUNCATE TABLE dtb_base_info;
                TRUNCATE TABLE dtb_order;
                TRUNCATE TABLE dtb_shipping;
                TRUNCATE TABLE dtb_shipment_item;
                set foreign_key_checks = 1;
            ';
            $Connection = $app['orm.em']->getConnection();
            $Connection->executeQuery($sql);
        }

        $loader = new ORMLoader();
        $Executor = new ORMExecutor($app['orm.em']);
        $loader->addFixture(new MemberFixture());
        $loader->addFixture(new BaseInfoFixture());
        $loader->addFixture(new OrderFixture());
        $loader->addFixture(new ShippingFixture());
        $loader->addFixture(new ShippingItemFixture());

        $fixtures = $loader->getFixtures();
        $Executor->execute($fixtures);
    }
}
