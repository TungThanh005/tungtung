<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Eccube\Command;

use Eccube\Doctrine\Common\CsvDataFixtures\CsvCopyToFixture;
use Eccube\Doctrine\Common\CsvDataFixtures\UpdateByCsvFixture;
use Eccube\Doctrine\Common\CsvDataFixtures\CustomerRelationByCsvFixture;
use Eccube\Doctrine\Common\CsvDataFixtures\Executor\DbalExecutor;
use Eccube\Doctrine\Common\CsvDataFixtures\Loader;
use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Eccube\Util\LogUtil;

class CsvCustomLoaderCommand extends Command
{
    /** @var \Eccube\Application $app */
    protected $app;
    protected function configure()
    {
        $this
            ->setName('csv-requirement-loader')
            ->addUsage('--file=csvfilename OR -f csvfilename')
            ->addOption('file', 'f', InputOption::VALUE_REQUIRED, 'Csv File Name.')
            ->addOption('sid', 's', InputOption::VALUE_REQUIRED, 'target ShopId.')
            ->setDescription('CSV Loader by Requirements')
            ->setHelp(
                <<<EOF
The <info>%command.name%</info> command CSV loader,
  <info>php %command.full_name% --file=[CSV File Name]</info>
  OR
  <info>php %command.full_name% -f [CSV File Name]</info>
  OR
  <info>php %command.full_name% --sid=[Shop Id]</info>
  OR
  <info>php %command.full_name% -s [Shop Id]</info>
EOF
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->app = $this->getSilexApplication();

        $dt = new \DateTime();
        $dt->setTimezone(new \DateTimeZone('Asia/Tokyo'));
        $start = $dt->format('Y-m-d H:i:s');

        $file = $input->getOption('file');
        $shop_id = $input->getOption('sid');    
        if ($shop_id) {
            $file = "customer_{$shop_id}.csv";
        }
         //引数エラー（ログに出せない）
        if (!$file) {
            $output->writeln("<error>{ファイル名の指定がありません}</error>");
            return;
        }
        //$file_name = preg_replace("/\./", "", basename($file, "csv"));
        $file_name = rtrim(basename($file, "csv"), '.');

        $file_path = config('root_dir'). config('csv_file_path').'/'. $this->get_sub_dir($file) . '/'. $file;
        //$file_name = preg_replace("/\.[^.]+$/", "", basename($file, "csv"));

        $this->writeLog("---------------------------------------------------------", $output, false, $file_name);
        $this->writeLog("[開始]{$start}", $output, false, $file_name);

        if (!file_exists($file_path)) {
            $erro_text = "[エラー] 指定のファイルが存在しませんでした。";
            $this->writeLog($erro_text, $output, true, $file_name);
            return;
        }

        $file_obj = new \SplFileObject($file_path);

        if (!$file_obj->isFile() || !$file_obj->getFilename()) {
            $erro_text = "[エラー] ファイルオープンでエラーが発生しました。";
            $this->writeLog($erro_text, $output, true, $file_name);
            return;
        }

        $csv_structure = isset(config('csv')[$file_name]) ? config('csv')[$file_name] : "";
        
        if (!$csv_structure) {
            $erro_text = "[エラー] CSV構造が設定ファイルに定義されていません。(config/csv.yml.dist)";
            $this->writeLog($erro_text, $output, true, $file_name);
            return;
        }

        //トランザクションは各Fixture内で行っている
        try {
            $output->writeln(sprintf("<comment>CSV Load %s</comment>", $file_obj->getFilename()));
            $loader = new Loader();
            $Executor = new DbalExecutor($this->app['orm.em']);
            $fixture = $csv_structure['fixture'];
            $fixture = "Eccube\\Doctrine\\Common\\CsvDataFixtures\\{$fixture}";
            $fixture_ins = new $fixture($file_obj->openFile(), $csv_structure);
            $loader->addFixture($fixture_ins);
            $fixtures = $loader->getFixtures();
            $Executor->execute($fixtures);
        } catch (\Exception $e) {
            $this->writeLog("{$e->getMessage()}", $output, true, $file_name);
            return;
        }

        $this->writeLog("CSV取り込み処理は正常に終了しました。", $output, false, $file_name);
        $this->writeLog("[終了] {$dt->modify('now')->format('Y-m-d H:i:s')}", $output, false, $file_name);
        $this->writeLog("---------------------------------------------------------", $output, false, $file_name);
    }

    public function writeLog($body, OutputInterface $output, $is_error = true, $file_name)
    {
        $text = <<<EOF
{$body}
EOF;
        $file_name = config('command_log_output_path') . '/' . $this->get_sub_dir($file_name) . '/' . date("ym") . "_{$file_name}.log";
        LogUtil::putFile($text, $file_name);
        if ($is_error) {
            $output->writeln("<error>{$body}</error>");
        }
    }

    public function get_sub_dir($file_name)
    {
        if (preg_match("/customer/", $file_name)) {
            return "customer";
        } else if (preg_match("/real_estate/", $file_name) || preg_match("/office/", $file_name)) {
            return "real_estate";
        }

        return "";
    }
}
