<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Form\Type\Admin;

use Carbon\Carbon;
use Eccube\Entity\PageLayout;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Eccube\Form\DataTransformer;

class MainEditType extends AbstractType
{
    public $app;

    public function __construct(\Silex\Application $app)
    {
        $this->app = $app;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $app = $this->app;
        $transformer = new DataTransformer\IntegerToBooleanTransformer();

        $builder
            ->add($builder->create('share_flg', 'checkbox', array(
                'label' => '共通ページ',
                'value' => '1',
                'required' => false,
            ))->addModelTransformer($transformer))
            ->add('name', 'text', array(
                'label' => '名称',
                'required' => true,
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Length(array(
                        'max' => $app['config']['sltext_len'],
                    ))
                )
            ))
            ->add('start_date', 'datetime', array(
                'label' => '開始日',
                'required' => false,
                'input' => 'datetime',
                'date_widget' => 'single_text',
                'time_widget' => 'choice',
                'empty_value' => array('year' => '- 年 -', 'month' => '- 月 -', 'day' => '- 日 -',
                    'hour' => '- 時 -', 'minute' => '- 分 -'
                ),
            ))
            ->add('end_date', 'datetime', array(
                'label' => '終了日',
                'required' => false,
                'input' => 'datetime',
                'date_widget' => 'single_text',
                'time_widget' => 'choice',
                'empty_value' => array('year' => '- 年 -', 'month' => '- 月 -', 'day' => '- 日 -',
                    'hour' => '- 時 -', 'minute' => '- 分 -'
                )
            ))
            ->add('url', 'text', array(
                'label' => 'URL',
                'required' => true,
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Length(array(
                        'max' => $app['config']['stext_len'],
                    )),
                    new Assert\Regex(array(
                        'pattern' => '/^([0-9a-zA-Z_\-]+\/?)+(?<!\/)$/',
                    )),
                )
            ))
            ->add('file_name', 'text', array(
                'label' => 'ファイル名',
                'required' => true,
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Length(array(
                        'max' => $app['config']['stext_len'],
                    )),
                    new Assert\Regex(array(
                        'pattern' => '/^([0-9a-zA-Z_\-]+\/?)+$/',
                    )),
                )
            ))
            ->add('tpl_data', 'textarea', array(
                'label' => false,
                'mapped' => false,
                'required' => true,
                'constraints' => array()
            ))
            ->add('author', 'text', array(
                'label' => 'author',
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => $app['config']['sltext_len'],
                    ))
                )
            ))
            ->add('description', 'text', array(
                'label' => 'description',
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => $app['config']['sltext_len'],
                    ))
                )
            ))
            ->add('keyword', 'text', array(
                'label' => 'keyword',
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => $app['config']['sltext_len'],
                    ))
                )
            ))
            ->add('meta_robots', 'text', array(
                'label' => 'robots',
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => $app['config']['sltext_len'],
                    ))
                )
            ))->add('meta_tags', 'textarea', array(
                'label' => '追加metaタグ',
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => $app['config']['lltext_len'],
                    ))
                )
            ))
            ->add('DeviceType', 'entity', array(
                'class' => 'Eccube\Entity\Master\DeviceType',
                'property' => 'id',
            ))
            ->add('id', 'hidden')
            ->addEventListener(FormEvents::POST_SUBMIT, function ($event) use ($app) {
                $form = $event->getForm();

                /** @var PageLayout $Banner */
                $PageLayout = $form->getData();
                if (!empty($PageLayout) && !empty($PageLayout->getEndDate()) && !empty($PageLayout->getStartDate())) {
                    $fromDate = Carbon::instance($PageLayout->getStartDate());
                    $toDate = Carbon::instance($PageLayout->getEndDate());
                    if ($fromDate && $fromDate->gt($toDate) || ($fromDate && !$toDate)) {
                        $form['start_date']->addError(new FormError('有効期間に誤りがあります。'));
                    }
                } else {
                    if ((!empty($PageLayout->getEndDate()) && empty($PageLayout->getStartDate())) || (empty($PageLayout->getEndDate()) && !empty($PageLayout->getStartDate()))) {
                        $form['start_date']->addError(new FormError('開始日と終了日は両方とも入力するか両方とも未入力にしてください。'));
                    }
                }

                $url = $form['url']->getData();
                $DeviceType = $form['DeviceType']->getData();
                $page_id = $form['id']->getData();

                $qb = $app['orm.em']->createQueryBuilder();
                $qb->select('p')
                    ->from('Eccube\\Entity\\PageLayout', 'p')
                    ->where('p.url = :url')
                    ->setParameter('url', $url)
                    ->andWhere('p.DeviceType = :DeviceType')
                    ->setParameter('DeviceType', $DeviceType);
                if (is_null($page_id)) {
                    $qb
                        ->andWhere('p.id IS NOT NULL');
                } else {
                    $qb
                        ->andWhere('p.id <> :page_id')
                        ->setParameter('page_id', $page_id);
                }
                //共通ページ時対応
                if (!$form['share_flg']->getData()) {
                    $qb->andWhere('(p.Shop = :Shop OR p.Shop IS NULL)');
                    $qb->setParameter('Shop', admin_current_shop());
                }

                $PageLayout = $qb
                    ->getQuery()
                    ->getResult();
                if (count($PageLayout) > 0) {
                    $form['url']->addError(new FormError('※ 同じURLのデータが存在しています。別のURLを入力してください。'));
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eccube\Entity\PageLayout',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'main_edit';
    }
}
