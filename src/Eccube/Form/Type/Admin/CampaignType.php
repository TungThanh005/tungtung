<?php

namespace Eccube\Form\Type\Admin;

use Carbon\Carbon;
use Eccube\Entity\Banner;
use Eccube\Entity\Campaign;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CampaignType extends AbstractType
{
    private $config;
    private $app;

    public function __construct($app)
    {
        $this->config = $app['config'];
        $this->app = $app;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $app = $this->app;

        $builder
            ->add('campaign_name', 'text', array(
                'label' => 'キャンペーン名称',
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => $this->config['stext_len'],
                    )),
                ),
            ))
            ->add('start_date', 'datetime', array(
                'label' => '開始日',
                'required' => true,
                'input' => 'datetime',
                'date_widget' => 'single_text',
                'time_widget' => 'choice',
                'empty_value' => array('year' => '- 年 -', 'month' => '- 月 -', 'day' => '- 日 -',
                    'hour' => '- 時 -', 'minute' => '- 分 -'
                ),
            ))
            ->add('shop1_flg', 'checkbox', array(
                'required' => false,
                'label' => 'サーパスショップ（契約者）',
                'value' => '1',
            ))
            ->add('shop2_flg', 'checkbox', array(
                'required' => false,
                'label' => 'サーパスショップ（入居者）',
                'value' => '1',
            ))
            ->add('shop3_flg', 'checkbox', array(
                'required' => false,
                'label' => 'くらしスクエアショップ',
                'value' => '1',
            ))
            ->add('free_shipping_flag', 'checkbox', array(
                'required' => false,
                'label' => '送料無料',
                'value' => '1',
            ))
            ->add('end_date', 'datetime', array(
                'label' => '終了日',
                'required' => true,
                'input' => 'datetime',
                'date_widget' => 'single_text',
                'time_widget' => 'choice',
                'empty_value' => array('year' => '- 年 -', 'month' => '- 月 -', 'day' => '- 日 -',
                    'hour' => '- 時 -', 'minute' => '- 分 -'
                ),
            ))
            ->add('point_rate', 'number', array(
                'label' => 'ポイント倍率',
                'precision' => 0,
                'scale' => 0,
                'grouping' => true,
                'required' => false,
                'constraints' => array(
                    new Assert\Range(array(
                        'max' => 100,
                        'min' => 0,
                    )),
                    new Assert\Regex(array(
                        'pattern' => "/^\d+$/u",
                        'message' => 'form.type.numeric.invalid'
                    )),
                ),
            ))
            ->add('point_amount', 'number', array(
                'label' => '固定ポイント',
                'precision' => 0,
                'scale' => 0,
                'grouping' => true,
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => 100,
                        'min' => 0,
                    )),
                    new Assert\Regex(array(
                        'pattern' => "/^\d+$/u",
                        'message' => 'form.type.numeric.invalid'
                    )),
                ),
            ))
            ->add('discount_amount', 'number', array(
                'label' => '割引額',
                'precision' => 0,
                'scale' => 0,
                'grouping' => true,
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => 100,
                        'min' => 0,
                    )),
                    new Assert\Regex(array(
                        'pattern' => "/^\d+$/u",
                        'message' => 'form.type.numeric.invalid'
                    )),
                ),
            ))
            ->add('office_select_flg', 'choice', array(
                'required' => false,
                'multiple' => false,
                'empty_value' => false,
                'label' => '商品選択',
                'expanded' => true,
                'choices' => array(
                    0 => "全事業所対象",
                    1 => "対象事業所指定",
                ),
            ))
            ->add('product_select_flg', 'choice', array(
                'required' => false,
                'multiple' => false,
                'empty_value' => false,
                'label' => '商品選択',
                'expanded' => true,
                'choices' => array(
                    0 => "全商品対象",
                    1 => "対象商品指定",
                ),
            ))
            ->add('campaign_type', 'choice', array(
                'required' => false,
                'multiple' => false,
                'empty_value' => false,
                'label' => '商品選択',
                'expanded' => true,
                'choices' => array(
                    0 => "なし",
                    1 => "ポイント倍率",
                    2 => "固定ポイント",
                    3 => "割引額",
                    4 => "送料無料",
                ),
            ))
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($app) {
                $form = $event->getForm();
                /** @var Campaign $Campaign */
                $Campaign = $form->getData();
                if (!empty($Campaign) && !empty($Campaign->getEndDate())) {
                    $fromDate = Carbon::instance($Campaign->getStartDate());
                    $toDate = Carbon::instance($Campaign->getEndDate());
                    if ($fromDate->gt($toDate)) {
                        $form['start_date']->addError(new FormError('有効期間に誤りがあります。'));
                    }
                }
                if (!$Campaign->getShop1Flg() && !$Campaign->getShop2Flg() && !$Campaign->getShop3Flg()) {
                    $form['shop1_flg']->addError(new FormError('ショップを一つ選択してください。'));
                }

                $checkAllProductCampaign = $app['eccube.repository.campaign']->checkAllProductCampaign($Campaign);

                if ($checkAllProductCampaign) {
                    $form['start_date']->addError(new FormError('重複期間内に全商品対象のキャンペーンが存在する為登録できません。'));
                }

            });
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eccube\Entity\Campaign',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'admin_campaign';
    }
}
