<?php

namespace Eccube\Form\Type\Admin;

use Carbon\Carbon;
use Eccube\Entity\Banner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class BannerType extends AbstractType
{
    private $config;
    private $app;

    public function __construct($app)
    {
        $this->config = $app['config'];
        $this->app = $app;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $app = $this->app;
        $arrCampaign = $this->app['eccube.repository.campaign']->findAll();

        $builder
            ->add('name', 'text', array(
                'label' => 'タイトル',
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => $this->config['stext_len'],
                    )),
                ),
            ))
            ->add('banner_position', 'banner_position', array(
                'label' => '設置場所',
                'multiple' => false,
                'expanded' => false,
                'constraints' => array(
                    new Assert\NotBlank(),
                ),
            ))
            ->add('Campaign', 'entity', array(
                'required' => false,

                'class' => 'Eccube\Entity\Campaign',
                'property' => 'campaign_name',
                'label' => 'キャンペーン',
                'empty_value' => "指定なし",
                'multiple' => false,
                'mapped' => false,
                // Choices list (overdrive mapped)
                'choices' => $arrCampaign,
            ))
            ->add('caption', 'textarea', array(
                'label' => 'キャプション',
                'required' => false,
                'constraints' => array(
                    new Assert\Length(array(
                        'max' => $this->config['stext_len'],
                    )),
                ),
            ))
            ->add('image_path', 'file', array(
                'label' => 'バナー画像',
                'mapped' => false,
                'required' => false,
            ))
            ->add('link', 'text', array(
                'label' => 'リンク先',
                'required' => true,
                'constraints' => array(
//                    new Assert\Url(),
                    new Assert\NotBlank(),
                ),
            ))
            ->add('link_method', 'checkbox', array(
                'required' => false,

                'label' => '別ウィンドウを開く',
                'value' => '1',
            ))
            ->add('start_date', 'datetime', array(
                'label' => '開始日',
                'required' => false,
                'empty_data' => null,
                'input' => 'datetime',
                'date_widget' => 'single_text',
                'time_widget' => 'choice',
                'empty_value' => array('year' => '- 年 -', 'month' => '- 月 -', 'day' => '- 日 -',
                    'hour' => '- 時 -', 'minute' => '- 分 -'
                ),
            ))
            ->add('end_date', 'datetime', array(
                'label' => '終了日',
                'required' => false,
                'empty_data' => null,
                'input' => 'datetime',
                'date_widget' => 'single_text',
                'time_widget' => 'choice',
                'empty_value' => array('year' => '- 年 -', 'month' => '- 月 -', 'day' => '- 日 -',
                    'hour' => '- 時 -', 'minute' => '- 分 -'
                ),
            ))
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($app) {
                $form = $event->getForm();

                /** @var Banner $Banner */
                $Banner = $form->getData();
                if (!empty($Banner) && !empty($Banner->getEndDate()) && !empty($Banner->getStartDate())) {
                    $fromDate = Carbon::instance($Banner->getStartDate());
                    $toDate = Carbon::instance($Banner->getEndDate());
                    if ($fromDate->gt($toDate)) {
                        $form['start_date']->addError(new FormError('有効期間に誤りがあります。'));
                    }
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eccube\Entity\Banner',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'admin_banner';
    }
}
