<?php

namespace Eccube\Form\Type\Admin;

use Eccube\Entity\ProductRanking;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormEvents;
use Eccube\Form\DataTransformer;

/**
 * Class ProductRecommendType.
 */
class ProductRecommendType extends AbstractType
{
    /**
     * @var \Eccube\Application
     */
    private $app;

    /**
     * ProductRecommendType constructor.
     *
     * @param \Silex\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Build config type form.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $app = $this->app;
        $builder
            ->add('id', 'text', array(
                'label' => '商品',
                'required' => false,
                'attr' => array('readonly' => 'readonly'),
            ));

        $builder->add(
            $builder
                ->create('Product', 'hidden')
                ->addModelTransformer(new DataTransformer\EntityToIdTransformer($this->app['orm.em'], '\Eccube\Entity\Product'))
        );

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($app) {
            $form = $event->getForm();
            $data = $form->getData();
            $Product = $data->getProduct();
            if (empty($Product)) {
                $form['comment']->addError(new FormError($app->trans('admin.product.recommend.type.product.not_found')));
                return;
            }
        });
    }

    /**
     *  {@inheritdoc}
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ProductRanking::class,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_product_recommend';
    }
}
