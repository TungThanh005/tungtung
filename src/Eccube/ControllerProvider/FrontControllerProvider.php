<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Eccube\ControllerProvider;

use Silex\Application;
use Silex\ControllerProviderInterface;

class FrontControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $c = $app['controllers_factory'];

        // 強制SSL
        if ($app['config']['force_ssl'] == \Eccube\Common\Constant::ENABLED) {
            $c->requireHttps();
        }
        $c->post('/shopping/aeonregi/result', '\Eccube\Controller\ShoppingController::aeonregiListener')->bind('aeonregi_listener');

        // user定義
        $c->match('/' . $app['config']['user_data_route'] . '/{route}', '\Eccube\Controller\UserDataController::index')->assert('route', '([0-9a-zA-Z_\-]+\/?)+(?<!\/)')->bind('user_data');

        $c->match('/login', '\Eccube\Controller\SurpassController::login')->bind('surpass_login');
        $c->match('/SLogout', '\Eccube\Controller\SurpassController::logout')->bind('surpass_logout');

        // root
        $c->match('/', '\Eccube\Controller\TopController::index')->bind('homepage');
        $c->match('/', '\Eccube\Controller\TopController::index')->bind('top'); // deprecated since 3.0.0, to be removed in 3.1
        $c->match('/', '\Eccube\Controller\TopController::index')->bind('index'); // deprecated since 3.0.0, to be removed in 3.1
//        $c->match('/spssp5/sm/', '\Eccube\Controller\TopController::smtIndex')->bind('smt_homepage');

        $c->match('/maintenance', '\Eccube\Controller\TopController::maintenance')->bind('maintenance');

        // News
        $c->match('/news', '\Eccube\Controller\NewsController::index')->bind('news');
        $c->match('/news/{id}', '\Eccube\Controller\NewsController::detail')->assert('id', '\d+')->bind('news_detail');

        // cart
        $c->match('/cart', '\Eccube\Controller\CartController::index')->bind('cart');
        $c->post('/cart/add', '\Eccube\Controller\CartController::add')->bind('cart_add');
        $c->put('/cart/up/{productClassId}', '\Eccube\Controller\CartController::up')->bind('cart_up')->assert('productClassId', '\d+');
        $c->put('/cart/down/{productClassId}', '\Eccube\Controller\CartController::down')->bind('cart_down')->assert('productClassId', '\d+');
        // setquantity deprecated since 3.0.0, to be removed in 3.1
        $c->put('/cart/setQuantity/{productClassId}/{quantity}', '\Eccube\Controller\CartController::setQuantity')->bind('cart_set_quantity')->assert('productClassId', '\d+')->assert('quantity', '\d+');
        $c->put('/cart/remove/{productClassId}', '\Eccube\Controller\CartController::remove')->bind('cart_remove')->assert('productClassId', '\d+');
        $c->match('/cart/buystep', '\Eccube\Controller\CartController::buystep')->bind('cart_buystep');

        // contact
        $c->match('/contact', '\Eccube\Controller\ContactController::index')->bind('contact');
        $c->match('/contact/complete', '\Eccube\Controller\ContactController::complete')->bind('contact_complete');

        // entry
        $c->match('/entry', '\Eccube\Controller\EntryController::index')->bind('entry');
        $c->match('/entry/complete', '\Eccube\Controller\EntryController::complete')->bind('entry_complete');
        $c->match('/entry/activate/{secret_key}', '\Eccube\Controller\EntryController::activate')->bind('entry_activate');

        // forgot
        $c->match('/forgot', '\Eccube\Controller\ForgotController::index')->bind('forgot');
        $c->match('/forgot/complete', '\Eccube\Controller\ForgotController::complete')->bind('forgot_complete');
        $c->match('/forgot/reset/{reset_key}', '\Eccube\Controller\ForgotController::reset')->bind('forgot_reset');

        // block
        $c->match('/block/header', '\Eccube\Controller\Block\BlockController::index')->bind('block_header');
        $c->match('/block/left_login', '\Eccube\Controller\Block\BlockController::leftLogin')->bind('block_left_login');
        $c->match('/block/left_nav', '\Eccube\Controller\Block\BlockController::leftNav')->bind('block_left_nav');
        $c->match('/block/left_banner', '\Eccube\Controller\Block\BlockController::leftBanner')->bind('block_left_banner');
        $c->match('/block/feature_banner', '\Eccube\Controller\Block\BlockController::featureBanner')->bind('block_feature_banner');
        $c->match('/block/campaign_banner', '\Eccube\Controller\Block\BlockController::campaignBanner')->bind('block_campaign_banner');//キャンペーンバナー
        $c->match('/block/season_product', '\Eccube\Controller\Block\BlockController::seasonProduct')->bind('block_season_product');//季節限定商品
        $c->match('/block/feature_product', '\Eccube\Controller\Block\BlockController::featureProduct')->bind('block_feature_product');//オススメ商品
        $c->match('/block/product_ranking', '\Eccube\Controller\Block\BlockController::rankingProduct')->bind('block_product_ranking');
        $c->match('/block/category', '\Eccube\Controller\Block\BlockController::category')->bind('block_category');
        $c->match('/block/top_slider', '\Eccube\Controller\Block\BlockController::topSlider')->bind('block_top_slider');
        $c->match('/block/search_product', '\Eccube\Controller\Block\BlockController::productSearch')->bind('block_search_product');
        $c->match('/block/news', '\Eccube\Controller\Block\BlockController::news')->bind('block_news');

        // 特定商取引 order -> help/traderaw
        $c->match('/help/about', '\Eccube\Controller\HelpController::about')->bind('help_about');
        $c->match('/help/guide', '\Eccube\Controller\HelpController::guide')->bind('help_guide');
        $c->match('/help/privacy', '\Eccube\Controller\HelpController::privacy')->bind('help_privacy');
        $c->match('/help/tradelaw', '\Eccube\Controller\HelpController::tradelaw')->bind('help_tradelaw');
        $c->match('/help/agreement', '\Eccube\Controller\HelpController::agreement')->bind('help_agreement');
        $c->match('/help/faq', '\Eccube\Controller\HelpController::faq')->bind('help_faq');
        $c->match('/help/policy', '\Eccube\Controller\HelpController::policy')->bind('help_policy');
        $c->match('/help/point', '\Eccube\Controller\HelpController::point')->bind('help_point');


        // mypage
        $c->match('/mypage', '\Eccube\Controller\Mypage\MypageController::index')->bind('mypage');
        $c->match('/mypage/login', '\Eccube\Controller\Mypage\MypageController::login')->bind('mypage_login');
        $c->match('/ssl/spssp5/login_from_spsnet.html', '\Eccube\Controller\Mypage\MypageController::auth')->bind('mypage_auth');
        $c->match('/ssl/spssp5/error.html', '\Eccube\Controller\Mypage\MypageController::authError')->bind('mypage_auth_error');
//        $c->match('/ssl/spssp5/sm/sm_login_from_spsnet.php', '\Eccube\Controller\Mypage\MypageController::auth')->bind('mypage_auth_mobile');
        $c->match('/mypage/change', '\Eccube\Controller\Mypage\ChangeController::index')->bind('mypage_change');
        $c->match('/mypage/change_complete', '\Eccube\Controller\Mypage\ChangeController::complete')->bind('mypage_change_complete');

        $c->match('/mypage/delivery', '\Eccube\Controller\Mypage\DeliveryController::index')->bind('mypage_delivery');
        $c->match('/mypage/delivery/new', '\Eccube\Controller\Mypage\DeliveryController::edit')->bind('mypage_delivery_new');
        $c->match('/mypage/delivery/{id}/edit', '\Eccube\Controller\Mypage\DeliveryController::edit')->assert('id', '\d+')->bind('mypage_delivery_edit');
        $c->delete('/mypage/delivery/{id}/delete', '\Eccube\Controller\Mypage\DeliveryController::delete')->assert('id', '\d+')->bind('mypage_delivery_delete');

        $c->match('/mypage/favorite', '\Eccube\Controller\Mypage\MypageController::favorite')->bind('mypage_favorite');
        $c->delete('/mypage/favorite/{id}/delete', '\Eccube\Controller\Mypage\MypageController::delete')->assert('id', '\d+')->bind('mypage_favorite_delete');
        $c->match('/mypage/history/{id}', '\Eccube\Controller\Mypage\MypageController::history')->bind('mypage_history')->assert('id', '\d+');
        $c->put('/mypage/order/{id}', '\Eccube\Controller\Mypage\MypageController::order')->bind('mypage_order')->assert('id', '\d+');
        $c->match('/mypage/withdraw', '\Eccube\Controller\Mypage\WithdrawController::index')->bind('mypage_withdraw');
        $c->match('/mypage/withdraw_complete', '\Eccube\Controller\Mypage\WithdrawController::complete')->bind('mypage_withdraw_complete');

        $c->match('/mypage/point', '\Eccube\Controller\Mypage\MypageController::pointTransaction')->bind('mypage_point_transaction');
        $c->match('/mypage/view', '\Eccube\Controller\Mypage\MypageController::productViewHistory')->bind('mypage_product_view_history');

        // products
        $c->match('/products/list', '\Eccube\Controller\ProductController::index')->bind('product_list');
        $c->match('/products/detail/{id}', '\Eccube\Controller\ProductController::detail')->bind('product_detail')->assert('id', '\d+');
        $c->match('/products/fine_passport/login', '\Eccube\Controller\ProductController::finePassportLogin')->bind('product_fine_passport_login');


        // shopping
        $c->match('/shopping', '\Eccube\Controller\ShoppingController::index')->bind('shopping');
        $c->match('/shopping/confirm', '\Eccube\Controller\ShoppingController::confirm')->bind('shopping_confirm');
        $c->match('/shopping/delivery', '\Eccube\Controller\ShoppingController::delivery')->bind('shopping_delivery');
        $c->match('/shopping/payment', '\Eccube\Controller\ShoppingController::payment')->bind('shopping_payment');
        $c->match('/shopping/payment_process', '\Eccube\Controller\ShoppingController::paymentProcess')->bind('shopping_payment_process');
        $c->match('/shopping/aeonregi/{action}', '\Eccube\Controller\ShoppingController::paymentResult')->assert('action', '(cancel|success|failure)')->bind('shopping_payment_result');
        $c->match('/shopping/shipping_change/{id}', '\Eccube\Controller\ShoppingController::shippingChange')->assert('id', '\d+')->bind('shopping_shipping_change');
        $c->match('/shopping/shipping/{id}', '\Eccube\Controller\ShoppingController::shipping')->assert('id', '\d+')->bind('shopping_shipping');
        $c->match('/shopping/shipping_edit_change/{id}', '\Eccube\Controller\ShoppingController::shippingEditChange')->assert('id', '\d+')->bind('shopping_shipping_edit_change');
        $c->match('/shopping/shipping_edit/{id}', '\Eccube\Controller\ShoppingController::shippingEdit')->assert('id', '\d+')->bind('shopping_shipping_edit');
        $c->match('/shopping/complete', '\Eccube\Controller\ShoppingController::complete')->bind('shopping_complete');
        $c->match('/shopping/login', '\Eccube\Controller\ShoppingController::login')->bind('shopping_login');
        $c->match('/shopping/nonmember', '\Eccube\Controller\ShoppingController::nonmember')->bind('shopping_nonmember');
        $c->match('/shopping/customer', '\Eccube\Controller\ShoppingController::customer')->bind('shopping_customer');
        $c->match('/shopping/shopping_error', '\Eccube\Controller\ShoppingController::shoppingError')->bind('shopping_error');
        $c->match('/shopping/shipping_multiple_change', '\Eccube\Controller\ShoppingController::shippingMultipleChange')->bind('shopping_shipping_multiple_change');
        $c->match('/shopping/shipping_multiple', '\Eccube\Controller\ShoppingController::shippingMultiple')->bind('shopping_shipping_multiple');
        $c->match('/shopping/shipping_multiple_edit', '\Eccube\Controller\ShoppingController::shippingMultipleEdit')->bind('shopping_shipping_multiple_edit');
        $c->match('/shopping/use_point', '\Eccube\Controller\ShoppingController::usePoint')->bind('point_use');

        return $c;
    }
}
