<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * NewsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NewsRepository extends EntityRepository
{
    public function getList()
    {
        $qb = $this->createQueryBuilder('n')
            ->where('n.Shop = :Shop')
            ->setParameter(':Shop', front_shop_id())
            ->orderBy('n.rank', 'DESC');
        return $qb;
    }

    /**
     * News の順位を1上げる.
     *
     * @param  \Eccube\Entity\News $News
     * @return boolean 成功した場合 true
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function up(\Eccube\Entity\News $News)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $rank = $News->getRank();

//            $News2 = $this->findOneBy(array('rank' => $rank + 1));
            $NewsList = $this->createQueryBuilder('n')
                ->where('n.Shop = :Shop AND n.rank > :rank')
                ->setParameter(':Shop', admin_shop_id())
                ->setParameter(':rank', $rank)
                ->orderBy('n.rank', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();

            if (count($NewsList) == 0) {
                throw new \Exception();
            }
            $News2 = $NewsList[0];

            $News2->setRank($rank);
            $em->persist($News2);

            // News更新
            $News->setRank($rank + 1);

            $em->persist($News);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

    /**
     * News の順位を1下げる
     *
     * @param  \Eccube\Entity\News $News
     * @return boolean 成功した場合 true
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function down(\Eccube\Entity\News $News)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $rank = $News->getRank();
//            $News2 = $this->findOneBy(array('rank' => $rank - 1));
            $NewsList = $this->createQueryBuilder('n')
                ->where('n.Shop = :Shop AND n.rank < :rank')
                ->setParameter(':Shop', admin_shop_id())
                ->setParameter(':rank', $rank)
                ->orderBy('n.rank', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();

            if (count($NewsList) == 0) {
                throw new \Exception();
            }
            $News2 = $NewsList[0];

            $News2->setRank($rank);
            $em->persist($News2);

            // News更新
            $News->setRank($rank - 1);

            $em->persist($News);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

    /**
     * News を保存する.
     *
     * @param  \Eccube\Entity\News $News
     * @return boolean 成功した場合 true
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function save(\Eccube\Entity\News $News)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            if (!$News->getId()) {
                $rank = $this->createQueryBuilder('n')
                    ->select('MAX(n.rank)')
                    ->getQuery()
                    ->getSingleScalarResult();
                if (!$rank) {
                    $rank = 0;
                }
                $News
                    ->setRank($rank + 1)
                    ->setDelFlg(0);
            }

            $em->persist($News);
            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }

        return true;
    }

    /**
     * News を削除する.
     *
     * @param  \Eccube\Entity\News $News
     * @return boolean 成功した場合 true
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function delete(\Eccube\Entity\News $News)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
//            $rank = $News->getRank();
//            $em->createQueryBuilder()
//                ->update('Eccube\Entity\News', 'n')
//                ->set('n.rank', 'n.rank - 1')
//                ->where('n.rank > :rank')->setParameter('rank', $rank)
//                ->getQuery()
//                ->execute();
            $News
                ->setDelFlg(1)
                ->setRank(0);

            $em->persist($News);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            return false;
        }

        return true;
    }
}
