<?php

namespace Eccube\Repository;

use Doctrine\ORM\EntityRepository;
use Eccube\Entity\Campaign;
use Eccube\Entity\Customer;

/**
 * CampaignRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CampaignRepository extends EntityRepository
{
    public function search($condition)
    {
        $qb = $this
            ->createQueryBuilder('c');
        if (isset($condition['campaign_name']) && $condition['campaign_name'] != "") {
            $qb->andWhere('c.campaign_name LIKE :Name');
            $qb->setParameter(':Name', "%" . $condition['campaign_name'] . "%");
        }
        if (isset($condition['start_date']) && $condition['start_date'] != "") {
            $qb->andWhere('c.start_date >= :StartDate');
            $qb->setParameter(':StartDate', new \DateTime($condition['start_date']));
        }
        if (isset($condition['end_date']) && $condition['end_date'] != "") {
            $qb->andWhere('c.start_date <= :EndDate');
            $qb->setParameter(':EndDate', new \DateTime($condition['end_date']));
        }

        if (isset($condition['campaign_end_flg']) && $condition['campaign_end_flg'] == true) {

        } else {
            $qb->andWhere('c.end_date > :Now');
            $qb->setParameter(':Now', new \DateTime());
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function getCampaign($ProductId = null)
    {
        $app = app();
        $RealEstateNo = null;
        if ($app->isGranted('ROLE_USER')) {
            /** @var Customer $Customer */
            $Customer = $app->user();
            if ($Customer->getRealEstate()) {
                try {
                    $RealEstateNo = $Customer->getRealEstate()->getRealEstateNo();
                } catch (\Exception $ex) {
                    log_error('物件情報が見つかりません。：' . $ex->getMessage(), $ex->getTrace());
                }
            }
        }

        $qb = $this
            ->createQueryBuilder('c')
            ->leftJoin('c.CampaignTargetOffices', 'cto')
            ->leftJoin('c.CampaignTargetProducts', 'ctp');
        $qb->where('c.campaign_type <> 0 AND c.start_date <= :Now AND c.end_date >= :Now')
            ->setParameter(':Now', date('Y-m-d H:i:s'));

        if (front_shop_id() == 1) {
            $qb->andWhere("c.shop1_flg = 1");
        } elseif (front_shop_id() == 2) {
            $qb->andWhere("c.shop2_flg = 1");
        } elseif (front_shop_id() == 3) {
            $qb->andWhere("c.shop3_flg = 1");
        }


        if ($RealEstateNo) {
            $qb->andWhere('c.office_select_flg = 0 OR (cto.real_estate_no IS NOT NULL AND cto.real_estate_no = :RealEstateNo)')
                ->setParameter('RealEstateNo', $RealEstateNo);
        } else {
            $qb->andWhere('c.office_select_flg = 0');
        }
        if ($ProductId) {
            $qb->andWhere('c.product_select_flg = 0 OR (ctp.Product IS NOT NULL AND ctp.Product = :ProductId)')
                ->setParameter('ProductId', $ProductId);
        } else {
            $qb->andWhere('c.product_select_flg = 0');
        }
        $qb->groupBy('c.id');

        $res = $qb
            ->getQuery()
            ->getResult();

        return $res[0] ?? null;
    }

    public function checkAllProductCampaign(Campaign $Campaign)
    {
        $whereShop = [];
        if ($Campaign->getShop1Flg()) {
            $whereShop[] = ' c.shop1_flg = 1 ';
        }
        if ($Campaign->getShop2Flg()) {
            $whereShop[] = ' c.shop2_flg = 1 ';
        }
        if ($Campaign->getShop3Flg()) {
            $whereShop[] = ' c.shop3_flg = 1 ';
        }
        $where = '';
        if (count($whereShop)) {
            $where .= ' AND (' . implode(' OR ', $whereShop) . ')';
        }
        $sqlVal = [];
        if ($Campaign->getId()) {
            $where .= ' AND campaign_id <> ? ';
            $sqlVal[] = $Campaign->getId();
        }

        $results = app('db')->fetchAll('SELECT * FROM dtb_campaign c WHERE c.del_flg <> 1
                                                  AND (c.start_date BETWEEN ? AND ? OR c.end_date BETWEEN ? AND ? OR ? BETWEEN c.start_date AND c.end_date OR ? BETWEEN c.start_date AND c.end_date)
                                                  AND c.end_date > NOW()
                                                  AND product_select_flg = 0 ' . $where, array_merge([
            $Campaign->getStartDate()->format('Y-m-d H:i:s'),
            $Campaign->getEndDate()->format('Y-m-d H:i:s'),
            $Campaign->getStartDate()->format('Y-m-d H:i:s'),
            $Campaign->getEndDate()->format('Y-m-d H:i:s'),
            $Campaign->getStartDate()->format('Y-m-d H:i:s'),
            $Campaign->getEndDate()->format('Y-m-d H:i:s'),
        ], $sqlVal));

        return $results[0] ?? null;
    }

    public function checkValidProducts($ids, Campaign $Campaign)
    {
        if (count($ids)) {
            $whereShop = [];
            if ($Campaign->getShop1Flg()) {
                $whereShop[] = ' c.shop1_flg = 1 ';
            }
            if ($Campaign->getShop2Flg()) {
                $whereShop[] = ' c.shop2_flg = 1 ';
            }
            if ($Campaign->getShop3Flg()) {
                $whereShop[] = ' c.shop3_flg = 1 ';
            }
            $where = '';
            if (count($whereShop)) {
                $where = ' (' . implode(' OR ', $whereShop) . ')';
            }
            $output = [];

            if ($AllProductCampaign = $this->checkAllProductCampaign($Campaign)) {
                $results = app('db')->fetchAll('SELECT * FROM dtb_product WHERE product_id IN (' . implode(',', $ids) . ')');
                if (($Campaign->getShop1Flg() && $AllProductCampaign['shop1_flg']) || ($Campaign->getShop2Flg() && $AllProductCampaign['shop2_flg']) || ($Campaign->getShop3Flg() && $AllProductCampaign['shop3_flg'])) {
                    foreach ($results as $result) {
                        $output[$result['product_id']] = [
                            'campaign_id' => $AllProductCampaign['campaign_id'],
                            'product_id' => $result['product_id'],
                            'campaign_name' => $AllProductCampaign['campaign_name'],
                            'shop1_flg' => $AllProductCampaign['shop1_flg'],
                            'shop2_flg' => $AllProductCampaign['shop2_flg'],
                            'shop3_flg' => $AllProductCampaign['shop3_flg'],
                            'start_date' => $AllProductCampaign['start_date'],
                            'end_date' => $AllProductCampaign['end_date'],
                            'product_select_flg' => $AllProductCampaign['product_select_flg'],
                        ];
                    }
                }
            } else {
                $results = app('db')->fetchAll('SELECT cp.campaign_id,cp.product_id,c.campaign_name,c.shop1_flg,c.shop2_flg,c.shop3_flg,c.start_date,c.end_date,c.product_select_flg
                                              FROM dtb_product p 
                                              LEFT JOIN dtb_campaign_target_product cp USING(product_id)
                                              LEFT JOIN dtb_campaign c USING(campaign_id) 
                                              WHERE c.del_flg <> 1 
                                              AND c.product_select_flg = 1
                                              AND (c.start_date BETWEEN ? AND ? OR c.end_date BETWEEN ? AND ? OR ? BETWEEN c.start_date AND c.end_date OR ? BETWEEN c.start_date AND c.end_date) 
                                              AND p.product_id IN (' . implode(',', $ids) . ') 
                                              AND ' . $where, [
                    $Campaign->getStartDate()->format('Y-m-d H:i:s'),
                    $Campaign->getEndDate()->format('Y-m-d H:i:s'),
                    $Campaign->getStartDate()->format('Y-m-d H:i:s'),
                    $Campaign->getEndDate()->format('Y-m-d H:i:s'),
                    $Campaign->getStartDate()->format('Y-m-d H:i:s'),
                    $Campaign->getEndDate()->format('Y-m-d H:i:s'),
                ]);

                foreach ($results as $result) {
                    $output[$result['product_id']] = $result;
                }
            }
            return $output;
        }
        return [];
    }


    public function getOffices($campaign_id, $keyword = '', $page = 1, $display = 20)
    {
        $where = '';
        $sqlVal = [];
        if (!empty($keyword)) {
            $where .= " AND (o.office_no LIKE ? ";
            $sqlVal[] = "%$keyword%";
            $where .= " OR o.office_name LIKE ? ";
            $sqlVal[] = "%$keyword%";
            $where .= " OR re.real_estate_name LIKE ? ";
            $sqlVal[] = "%$keyword%";
            $where .= " OR re.jnet_code LIKE ? ";
            $sqlVal[] = "%$keyword%";
            $where .= " OR re.real_estate_no LIKE ? )";
            $sqlVal[] = "%$keyword%";
        }

        $res = db_pagination('o.office_name,o.office_no,re.real_estate_name,re.real_estate_no,re.jnet_code,cto.id', 'dtb_campaign_target_office cto
            INNER JOIN dtb_real_estate re USING(real_estate_no)
            LEFT JOIN dtb_office o ON o.office_no = re.office_no
            INNER JOIN dtb_campaign c USING(campaign_id)',
            'cto.campaign_id = ? ' . $where, array_merge([$campaign_id], $sqlVal), $page, $display);
        return $res;
    }

    public function countRealEstate($campaign_id)
    {
        return (app('db')->fetchAll('SELECT COUNT(*) count FROM dtb_campaign_target_office cto
            INNER JOIN dtb_real_estate re USING(real_estate_no)
            LEFT JOIN dtb_office o ON o.office_no = re.office_no
            INNER JOIN dtb_campaign c USING(campaign_id)
            WHERE campaign_id = ?', [$campaign_id]))[0]['count'];
    }

    public function countProduct($campaign_id)
    {
        return (app('db')->fetchAll('SELECT COUNT(*) count FROM dtb_campaign_target_product WHERE campaign_id = ?', [$campaign_id]))[0]['count'];
    }
}
