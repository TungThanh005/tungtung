<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Eccube\Application;
use Eccube\Entity\Block;
use Eccube\Entity\BlockPosition;
use Eccube\Entity\PageLayout;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181127000000 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'Maker', 'id', 'メーカー(ID)', '39', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'Maker', 'name', 'メーカー(名称)', '40', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'fine_passport_flg', NULL, 'ファインパスポート対象', '41', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'escort_flg', NULL, 'エスコート対象', '42', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'agency', NULL, '提携ショップ', '43', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_page_layout` (`page_id`, `device_type_id`, `shop_id`, `page_name`, `url`, `file_name`, `edit_flg`, `author`, `description`, `keyword`, `update_url`, `create_date`, `update_date`, `meta_robots`, `meta_tags`, `share_flg`, `start_date`, `end_date`) VALUES (NULL, '10', NULL, '商品購入/利用ポイントの設定', 'point_use', 'Shopping/use_point', '2', NULL, NULL, NULL, NULL, '2018-10-03 18:07:51', '2018-10-03 18:07:51', 'noindex', NULL, '1', NULL, NULL);");
        $this->addSql("INSERT INTO `dtb_page_layout` (`page_id`, `device_type_id`, `shop_id`, `page_name`, `url`, `file_name`, `edit_flg`, `author`, `description`, `keyword`, `update_url`, `create_date`, `update_date`, `meta_robots`, `meta_tags`, `share_flg`, `start_date`, `end_date`) VALUES (NULL, '10', NULL, 'サーパスショップログイン', 'surpass_login', 'surpass_login', '2', NULL, NULL, NULL, NULL, '2018-10-03 18:07:46', '2018-10-03 18:07:46', 'noindex', NULL, '1', NULL, NULL);");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
