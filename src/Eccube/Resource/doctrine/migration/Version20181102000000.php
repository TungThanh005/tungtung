<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Eccube\Application;
use Eccube\Entity\Block;
use Eccube\Entity\BlockPosition;
use Eccube\Entity\PageLayout;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181102000000 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $app = \Eccube\Application::getInstance();

        $PageLayouts = $app['eccube.repository.page_layout']->findBy([]);;

        /** @var PageLayout $PageLayout */
        foreach ($PageLayouts as $PageLayout) {
            if (in_array($PageLayout->getUrl(), ['preview', 'homepage'])) continue;

            if (!preg_match("/^(mypage|shopping|cart|contact)/i", $PageLayout->getUrl())) {
                /** @var Block $Block */
                $Block = $app['eccube.repository.block']->findOneBy([
                    'file_name' => 'left_login',
                    'Shop' => null
                ]);
                $BlockPosition = new BlockPosition();
                $BlockPosition
                    ->setPageId($PageLayout->getId())
                    ->setBlockId($Block->getId())
                    ->setBlockRow(1)
                    ->setTargetId(4)
                    ->setBlock($Block)
                    ->setPageLayout($PageLayout)
                    ->setAnywhere(0);
                $app["orm.em"]->persist($BlockPosition);

                /** @var Block $Block */
                $Block = $app['eccube.repository.block']->findOneBy([
                    'file_name' => 'left_banner',
                    'Shop' => null
                ]);
                $BlockPosition = new BlockPosition();
                $BlockPosition
                    ->setPageId($PageLayout->getId())
                    ->setBlockId($Block->getId())
                    ->setBlockRow(2)
                    ->setTargetId(4)
                    ->setBlock($Block)
                    ->setPageLayout($PageLayout)
                    ->setAnywhere(0);
                $app["orm.em"]->persist($BlockPosition);

                /** @var Block $Block */
                $Block = $app['eccube.repository.block']->findOneBy([
                    'file_name' => 'left_nav',
                    'Shop' => null
                ]);
                $BlockPosition = new BlockPosition();
                $BlockPosition
                    ->setPageId($PageLayout->getId())
                    ->setBlockId($Block->getId())
                    ->setBlockRow(3)
                    ->setTargetId(4)
                    ->setBlock($Block)
                    ->setPageLayout($PageLayout)
                    ->setAnywhere(0);
                $app["orm.em"]->persist($BlockPosition);
                ///
                /** @var Block $Block */
                $Block = $app['eccube.repository.block']->findOneBy([
                    'file_name' => 'product_ranking',
                    'Shop' => null
                ]);
                $BlockPosition = new BlockPosition();
                $BlockPosition
                    ->setPageId($PageLayout->getId())
                    ->setBlockId($Block->getId())
                    ->setBlockRow(4)
                    ->setTargetId(4)
                    ->setBlock($Block)
                    ->setPageLayout($PageLayout)
                    ->setAnywhere(0);
                $app["orm.em"]->persist($BlockPosition);
            }
            if (preg_match("/^(product_list)/i", $PageLayout->getUrl())) {
                /** @var Block $Block */
                $Block = $app['eccube.repository.block']->findOneBy([
                    'file_name' => 'feature_banner',
                    'Shop' => $PageLayout->getShopId()
                ]);
                $BlockPosition = new BlockPosition();
                $BlockPosition
                    ->setPageId($PageLayout->getId())
                    ->setBlockId($Block->getId())
                    ->setBlockRow(1)
                    ->setTargetId(5)
                    ->setBlock($Block)
                    ->setPageLayout($PageLayout)
                    ->setAnywhere(0);
                $app["orm.em"]->persist($BlockPosition);
            }

            if (preg_match("/^(shoplist)/i", $PageLayout->getUrl())) {
                /** @var Block $Block */
                $Block = $app['eccube.repository.block']->findOneBy([
                    'file_name' => 'shoplist',
                    'Shop' => $PageLayout->getShopId()
                ]);
                $BlockPosition = new BlockPosition();
                $BlockPosition
                    ->setPageId($PageLayout->getId())
                    ->setBlockId($Block->getId())
                    ->setBlockRow(1)
                    ->setTargetId(5)
                    ->setBlock($Block)
                    ->setPageLayout($PageLayout)
                    ->setAnywhere(0);
                $app["orm.em"]->persist($BlockPosition);
            }
        }
        $app["orm.em"]->flush();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
