<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Eccube\Application;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180730120000 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
        INSERT INTO `dtb_base_info` (`id`, `country_id`, `pref`, `company_name`, `company_kana`, `zip01`, `zip02`, `zipcode`, `addr01`, `addr02`, `tel01`, `tel02`, `tel03`, `fax01`, `fax02`, `fax03`, `business_hour`, `email01`, `email02`, `email03`, `email04`, `shop_code`, `shop_domain`, `shop_name`, `shop_kana`, `shop_name_eng`, `update_date`, `good_traded`, `message`, `latitude`, `longitude`, `delivery_free_amount`, `delivery_free_quantity`, `option_multiple_shipping`, `option_mypage_order_status_display`, `nostock_hidden`, `option_favorite_product`, `option_product_delivery_fee`, `option_product_tax_rule`, `option_customer_activate`, `option_allow_guest`, `option_allow_user_register`, `option_remember_me`, `option_allow_point`, `authentication_key`) VALUES
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin@example.com', 'admin@example.com', 'admin@example.com', 'admin@example.com', 'testsur2', 'ecdev.dev.web-staging.biz', 'サーパスショップ（入居者）', NULL, NULL, '2018-10-18 12:43:39', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin@example.com', 'admin@example.com', 'admin@example.com', 'admin@example.com', 'testkura', 'ec2.com', 'くらしスクエアショップ', NULL, NULL, '2018-10-17 17:37:14', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, NULL);
        ");

        $this->addSql('UPDATE dtb_base_info SET option_multiple_shipping = 1;');

        $this->addSql("INSERT INTO dtb_block (block_id, device_type_id, block_name, file_name, logic_flg, deletable_flg) VALUES (null, 10, '売上ランキング', 'product_sale_ranking', 1, 0);");
        $this->addSql("INSERT INTO dtb_block (block_id, device_type_id, block_name, file_name, logic_flg, deletable_flg) VALUES (null, 10, 'おすすめ商品', 'product_recommend', 1, 0);");


        $this->addSql("INSERT INTO `dtb_customer` (`customer_id`, `shop_id`, `status`, `sex`, `job`, `country_id`, `pref`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `zip01`, `zip02`, `zipcode`, `addr01`, `addr02`, `email`, `tel01`, `tel02`, `tel03`, `fax01`, `fax02`, `fax03`, `birth`, `password`, `salt`, `secret_key`, `first_buy_date`, `last_buy_date`, `buy_times`, `buy_total`, `note`, `reset_key`, `reset_expire`, `create_date`, `update_date`, `staff_flg`, `del_flg`) VALUES (1, 2, 2, 1, 18, NULL, 13, 'テスト', 'テスト', 'テスト', 'テスト', NULL, '160', '0022', 0, '新宿区新宿', 'テスト', 'test@test.com', '0', '0', '0', NULL, NULL, NULL, '2000-01-01 00:00:00', '7a461ca64dd3a16e28483bd6ce98a3efc5657dd5b52eaa645b9d07deab351098', '5f1ba93aaf', '1nJ1d1Xq5jLXSJ8CDLE5fwlfcHGxF7zB', NULL, NULL, '0', '0', NULL, NULL, NULL, '2018-08-09 10:31:18', '2018-08-09 10:31:18', 0, 0);");
        $this->addSql("INSERT INTO `dtb_customer_address` (`customer_address_id`, `customer_id`, `country_id`, `pref`, `name01`, `name02`, `kana01`, `kana02`, `company_name`, `zip01`, `zip02`, `zipcode`, `addr01`, `addr02`, `tel01`, `tel02`, `tel03`, `fax01`, `fax02`, `fax03`, `create_date`, `update_date`, `del_flg`) VALUES (1, 1, NULL, 13, 'テスト', 'テスト', 'テスト', 'テスト', NULL, '160', '0022', '1600022', '新宿区新宿', 'テスト', '0', '0', '0', NULL, NULL, NULL, '2018-08-09 10:31:18', '2018-08-09 10:31:18', 0);");

        $this->addSql("UPDATE dtb_product SET status_shop1 = 1, status_shop2 = 1,status_shop3 = 1;");
        $this->addSql("UPDATE dtb_product_class SET staff_price = price02, price_shop1 = price02, price_shop2 = price02, price_shop3 = price02;");

        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\ProductClass', 'staff_price', NULL, '社員価格', '241', '1', '2018-08-17 17:09:21', '2018-08-17 17:09:21');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\ProductClass', 'price_shop1', NULL, 'サーパスショップ（契約者）販売価格', '242', '1', '2018-08-17 17:09:21', '2018-08-17 17:09:21');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\ProductClass', 'price_shop2', NULL, 'サーパスショップ（入居者）販売価格', '243', '1', '2018-08-17 17:09:21', '2018-08-17 17:09:21');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\ProductClass', 'price_shop3', NULL, 'くらしスクエアショップ販売価格', '244', '1', '2018-08-17 17:09:21', '2018-08-17 17:09:21');");

        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'StatusShop1', 'id', 'サーパスショップ（契約者）公開ステータス(ID)', '32', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'StatusShop1', 'name', 'サーパスショップ（契約者）公開ステータス(名称)', '33', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'StatusShop2', 'id', 'サーパスショップ（入居者）公開ステータス(ID)', '34', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'StatusShop2', 'name', 'サーパスショップ（入居者）公開ステータス(名称)', '35', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'StatusShop3', 'id', 'くらしスクエアショップ公開ステータス(ID)', '36', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");
        $this->addSql("INSERT INTO `dtb_csv` (`csv_id`, `csv_type`, `creator_id`, `entity_name`, `field_name`, `reference_field_name`, `disp_name`, `rank`, `enable_flg`, `create_date`, `update_date`) VALUES (NULL, '1', '1', 'Eccube\\\\Entity\\\\Product', 'StatusShop3', 'name', 'くらしスクエアショップ公開ステータス(名称)', '37', '1', '2018-08-17 19:01:53', '2018-08-17 19:01:53');");

        $this->addSql("INSERT INTO `dtb_page_layout` (`page_id`, `device_type_id`, `shop_id`, `page_name`, `url`, `file_name`, `edit_flg`, `author`, `description`, `keyword`, `update_url`, `create_date`, `update_date`, `meta_robots`, `meta_tags`) VALUES
                        (NULL, 10, 2, 'TOPページ', 'homepage', 'index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '商品一覧ページ', 'product_list', 'Product/list', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '商品詳細ページ', 'product_detail', 'Product/detail', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, 'MYページ', 'mypage', 'Mypage/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/会員登録内容変更(入力ページ)', 'mypage_change', 'Mypage/change', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/会員登録内容変更(完了ページ)', 'mypage_change_complete', 'Mypage/change_complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/お届け先一覧', 'mypage_delivery', 'Mypage/delivery', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/お届け先追加', 'mypage_delivery_new', 'Mypage/delivery_edit', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/お気に入り一覧', 'mypage_favorite', 'Mypage/favorite', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/購入履歴詳細', 'mypage_history', 'Mypage/history', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/ログイン', 'mypage_login', 'Mypage/login', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/退会手続き(入力ページ)', 'mypage_withdraw', 'Mypage/withdraw', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/退会手続き(完了ページ)', 'mypage_withdraw_complete', 'Mypage/withdraw_complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, '当サイトについて', 'help_about', 'Help/about', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '現在のカゴの中', 'cart', 'Cart/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'お問い合わせ(入力ページ)', 'contact', 'Contact/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, 'お問い合わせ(完了ページ)', 'contact_complete', 'Contact/complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '会員登録(入力ページ)', 'entry', 'Entry/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, 'ご利用規約', 'help_agreement', 'Help/agreement', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '会員登録(完了ページ)', 'entry_complete', 'Entry/complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '特定商取引に関する法律に基づく表記', 'help_tradelaw', 'Help/tradelaw', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '本会員登録(完了ページ)', 'entry_activate', 'Entry/activate', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '商品購入', 'shopping', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/お届け先の指定', 'shopping_shipping', 'Shopping/shipping', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/お届け先の複数指定', 'shopping_shipping_multiple', 'Shopping/shipping_multiple', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/ご注文完了', 'shopping_complete', 'Shopping/complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 2, 'プライバシーポリシー', 'help_privacy', 'Help/privacy', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '商品購入ログイン', 'shopping_login', 'Shopping/login', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '非会員購入情報入力', 'shopping_nonmember', 'Shopping/nonmember', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 2, '商品購入/お届け先の追加', 'shopping_shipping_edit', 'Shopping/shipping_edit', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/お届け先の複数指定(お届け先の追加)', 'shopping_shipping_multiple_edit', 'Shopping/shipping_multiple_edit', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/購入エラー', 'shopping_error', 'Shopping/shopping_error', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, 'ご利用ガイド', 'help_guide', 'Help/guide', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', NULL, NULL),
                        (NULL, 10, 2, 'パスワード再発行(入力ページ)', 'forgot', 'Forgot/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', NULL, NULL),
                        (NULL, 10, 2, 'パスワード再発行(完了ページ)', 'forgot_complete', 'Forgot/complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, 'パスワード変更(完了ページ)', 'forgot_reset', 'Forgot/reset', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:46', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/配送方法選択', 'shopping_delivery', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/支払方法選択', 'shopping_payment', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/お届け先変更', 'shopping_shipping_change', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/お届け先変更', 'shopping_shipping_edit_change', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/お届け先の複数指定', 'shopping_shipping_multiple_change', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 2, 'MYページ/お届け先編集', 'mypage_delivery_edit', 'Mypage/delivery_edit', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:46', '2018-08-22 11:14:46', 'noindex', NULL),
                        (NULL, 10, 2, '商品購入/確認', 'shopping_confirm', 'Shopping/confirm', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:47', '2018-08-22 11:14:47', 'noindex', NULL);
                        ");

        $this->addSql("INSERT INTO `dtb_page_layout` (`page_id`, `device_type_id`, `shop_id`, `page_name`, `url`, `file_name`, `edit_flg`, `author`, `description`, `keyword`, `update_url`, `create_date`, `update_date`, `meta_robots`, `meta_tags`) VALUES
                        (NULL, 10, 3, 'TOPページ', 'homepage', 'index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '商品一覧ページ', 'product_list', 'Product/list', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '商品詳細ページ', 'product_detail', 'Product/detail', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, 'MYページ', 'mypage', 'Mypage/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/会員登録内容変更(入力ページ)', 'mypage_change', 'Mypage/change', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/会員登録内容変更(完了ページ)', 'mypage_change_complete', 'Mypage/change_complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/お届け先一覧', 'mypage_delivery', 'Mypage/delivery', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/お届け先追加', 'mypage_delivery_new', 'Mypage/delivery_edit', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/お気に入り一覧', 'mypage_favorite', 'Mypage/favorite', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/購入履歴詳細', 'mypage_history', 'Mypage/history', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/ログイン', 'mypage_login', 'Mypage/login', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/退会手続き(入力ページ)', 'mypage_withdraw', 'Mypage/withdraw', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/退会手続き(完了ページ)', 'mypage_withdraw_complete', 'Mypage/withdraw_complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, '当サイトについて', 'help_about', 'Help/about', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '現在のカゴの中', 'cart', 'Cart/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'お問い合わせ(入力ページ)', 'contact', 'Contact/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, 'お問い合わせ(完了ページ)', 'contact_complete', 'Contact/complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '会員登録(入力ページ)', 'entry', 'Entry/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, 'ご利用規約', 'help_agreement', 'Help/agreement', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '会員登録(完了ページ)', 'entry_complete', 'Entry/complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '特定商取引に関する法律に基づく表記', 'help_tradelaw', 'Help/tradelaw', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '本会員登録(完了ページ)', 'entry_activate', 'Entry/activate', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '商品購入', 'shopping', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/お届け先の指定', 'shopping_shipping', 'Shopping/shipping', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/お届け先の複数指定', 'shopping_shipping_multiple', 'Shopping/shipping_multiple', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/ご注文完了', 'shopping_complete', 'Shopping/complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', 'noindex', NULL),
                        (NULL, 10, 3, 'プライバシーポリシー', 'help_privacy', 'Help/privacy', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '商品購入ログイン', 'shopping_login', 'Shopping/login', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '非会員購入情報入力', 'shopping_nonmember', 'Shopping/nonmember', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, '商品購入/お届け先の追加', 'shopping_shipping_edit', 'Shopping/shipping_edit', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/お届け先の複数指定(お届け先の追加)', 'shopping_shipping_multiple_edit', 'Shopping/shipping_multiple_edit', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/購入エラー', 'shopping_error', 'Shopping/shopping_error', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, 'ご利用ガイド', 'help_guide', 'Help/guide', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', NULL, NULL),
                        (NULL, 10, 3, 'パスワード再発行(入力ページ)', 'forgot', 'Forgot/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', NULL, NULL),
                        (NULL, 10, 3, 'パスワード再発行(完了ページ)', 'forgot_complete', 'Forgot/complete', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, 'パスワード変更(完了ページ)', 'forgot_reset', 'Forgot/reset', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:46', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/配送方法選択', 'shopping_delivery', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/支払方法選択', 'shopping_payment', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/お届け先変更', 'shopping_shipping_change', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/お届け先変更', 'shopping_shipping_edit_change', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/お届け先の複数指定', 'shopping_shipping_multiple_change', 'Shopping/index', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:45', '2018-08-22 11:14:45', 'noindex', NULL),
                        (NULL, 10, 3, 'MYページ/お届け先編集', 'mypage_delivery_edit', 'Mypage/delivery_edit', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:46', '2018-08-22 11:14:46', 'noindex', NULL),
                        (NULL, 10, 3, '商品購入/確認', 'shopping_confirm', 'Shopping/confirm', 2, NULL, NULL, NULL, NULL, '2018-08-22 11:14:47', '2018-08-22 11:14:47', 'noindex', NULL),
                        (NULL, 10, 2, 'プレビューデータ', 'preview', NULL, 1, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL),
                        (NULL, 10, 3, 'プレビューデータ', 'preview', NULL, 1, NULL, NULL, NULL, NULL, '2018-08-22 11:14:42', '2018-08-22 11:14:42', NULL, NULL);");

        $this->addSql("INSERT INTO `mtb_banner_position` (`id`, `name`, `rank`) VALUES
                (1, 'トップスライダー', 0),
                (2, '特集バナー', 1),
                (3, 'キャンペーンバナー', 2),
                (4, '左ナビバナー', 3);");
        $this->addSql("INSERT INTO `dtb_banner` (`banner_id`, `shop_id`, `banner_position`, `name`, `caption`, `image_path`, `link`, `start_date`, `end_date`, `create_date`, `update_date`, `link_method`, `del_flg`) VALUES
                (NULL, 1, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-10-31 00:00:00', '0000-00-00 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 1, 3, 'テスト', 'テストテストテスト', '1012155209/1012155209_5bc044998fa0a.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:09', 0, 0),
                (NULL, 1, 3, 'テスト', 'テストテストテスト', '1012155219/1012155219_5bc044a37193b.jpeg', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:19', 0, 0),
                (NULL, 1, 2, 'テスト', NULL, '1029130151/1029130151_5bd6862fa267c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-29 13:01:51', 0, 0),
                (NULL, 1, 3, 'テスト', 'テストテストテスト', '1012155239/1012155239_5bc044b75683f.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:39', 0, 0),
                (NULL, 1, 3, 'テスト', 'テストテストテスト', '1012155249/1012155249_5bc044c1e85dd.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:49', 0, 0),
                (NULL, 1, 3, 'テスト', 'テストテストテスト', '1012155300/1012155300_5bc044cc76e5c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:53:00', 0, 0),
                (NULL, 1, 1, 'テスト', 'テストテストテスト', '1012160734/1012160734_5bc04836ef754.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:07:34', 0, 0),
                (NULL, 1, 1, 'テスト', 'テストテストテスト', '1012160745/1012160745_5bc04841e1e30.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:07:45', 0, 0),
                (NULL, 1, 1, 'テスト', 'テストテストテスト', '1012160826/1012160826_5bc0486a97ea3.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:26', 0, 0),
                (NULL, 1, 1, 'テスト', 'テストテストテスト', '1012160836/1012160836_5bc04874c6ec1.jpeg', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:36', 0, 0),
                (NULL, 1, 1, 'テスト', 'テストテストテスト', '1012160844/1012160844_5bc0487cd713c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:44', 0, 0),
                (NULL, 1, 1, 'テスト', 'テストテストテスト', '1012160853/1012160853_5bc0488547328.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:53', 0, 0),
                (NULL, 1, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 1, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 1, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 1, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0);
                ");
        $this->addSql("INSERT INTO `dtb_banner` (`banner_id`, `shop_id`, `banner_position`, `name`, `caption`, `image_path`, `link`, `start_date`, `end_date`, `create_date`, `update_date`, `link_method`, `del_flg`) VALUES
                (NULL, 2, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-10-31 00:00:00', '0000-00-00 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 2, 3, 'テスト', 'テストテストテスト', '1012155209/1012155209_5bc044998fa0a.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:09', 0, 0),
                (NULL, 2, 3, 'テスト', 'テストテストテスト', '1012155219/1012155219_5bc044a37193b.jpeg', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:19', 0, 0),
                (NULL, 2, 2, 'テスト', NULL, '1029130151/1029130151_5bd6862fa267c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-29 13:01:51', 0, 0),
                (NULL, 2, 3, 'テスト', 'テストテストテスト', '1012155239/1012155239_5bc044b75683f.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:39', 0, 0),
                (NULL, 2, 3, 'テスト', 'テストテストテスト', '1012155249/1012155249_5bc044c1e85dd.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:49', 0, 0),
                (NULL, 2, 3, 'テスト', 'テストテストテスト', '1012155300/1012155300_5bc044cc76e5c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:53:00', 0, 0),
                (NULL, 2, 1, 'テスト', 'テストテストテスト', '1012160734/1012160734_5bc04836ef754.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:07:34', 0, 0),
                (NULL, 2, 1, 'テスト', 'テストテストテスト', '1012160745/1012160745_5bc04841e1e30.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:07:45', 0, 0),
                (NULL, 2, 1, 'テスト', 'テストテストテスト', '1012160826/1012160826_5bc0486a97ea3.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:26', 0, 0),
                (NULL, 2, 1, 'テスト', 'テストテストテスト', '1012160836/1012160836_5bc04874c6ec1.jpeg', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:36', 0, 0),
                (NULL, 2, 1, 'テスト', 'テストテストテスト', '1012160844/1012160844_5bc0487cd713c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:44', 0, 0),
                (NULL, 2, 1, 'テスト', 'テストテストテスト', '1012160853/1012160853_5bc0488547328.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:53', 0, 0),
                (NULL, 2, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 2, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 2, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 2, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0);
                ");

        $this->addSql("INSERT INTO `dtb_banner` (`banner_id`, `shop_id`, `banner_position`, `name`, `caption`, `image_path`, `link`, `start_date`, `end_date`, `create_date`, `update_date`, `link_method`, `del_flg`) VALUES
                (NULL, 3, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-10-31 00:00:00', '0000-00-00 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 3, 3, 'テスト', 'テストテストテスト', '1012155209/1012155209_5bc044998fa0a.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:09', 0, 0),
                (NULL, 3, 3, 'テスト', 'テストテストテスト', '1012155219/1012155219_5bc044a37193b.jpeg', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:19', 0, 0),
                (NULL, 3, 2, 'テスト', NULL, '1029130151/1029130151_5bd6862fa267c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-29 13:01:51', 0, 0),
                (NULL, 3, 3, 'テスト', 'テストテストテスト', '1012155239/1012155239_5bc044b75683f.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:39', 0, 0),
                (NULL, 3, 3, 'テスト', 'テストテストテスト', '1012155249/1012155249_5bc044c1e85dd.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:52:49', 0, 0),
                (NULL, 3, 3, 'テスト', 'テストテストテスト', '1012155300/1012155300_5bc044cc76e5c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 15:53:00', 0, 0),
                (NULL, 3, 1, 'テスト', 'テストテストテスト', '1012160734/1012160734_5bc04836ef754.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:07:34', 0, 0),
                (NULL, 3, 1, 'テスト', 'テストテストテスト', '1012160745/1012160745_5bc04841e1e30.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:07:45', 0, 0),
                (NULL, 3, 1, 'テスト', 'テストテストテスト', '1012160826/1012160826_5bc0486a97ea3.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:26', 0, 0),
                (NULL, 3, 1, 'テスト', 'テストテストテスト', '1012160836/1012160836_5bc04874c6ec1.jpeg', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:36', 0, 0),
                (NULL, 3, 1, 'テスト', 'テストテストテスト', '1012160844/1012160844_5bc0487cd713c.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:44', 0, 0),
                (NULL, 3, 1, 'テスト', 'テストテストテスト', '1012160853/1012160853_5bc0488547328.png', 'http://google.com', '2018-04-26 00:00:00', '2019-10-26 00:00:00', '2018-10-12 00:00:00', '2018-10-12 16:08:53', 0, 0),
                (NULL, 3, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 3, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 3, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0),
                (NULL, 3, 2, 'テスト', NULL, '1029130142/1029130142_5bd6862642772.png', 'http://google.com', '2018-04-26 00:00:00', '2018-12-28 00:00:00', '2018-10-29 00:00:00', '2018-10-29 13:02:29', 0, 0);
                ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
