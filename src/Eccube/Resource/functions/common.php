<?php

use Eccube\Entity\BaseInfo;
use Eccube\Repository\CustomerRepository;
use Eccube\Service\CacheService;
use Eccube\Service\PointService;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

if (!function_exists('app')) {
    function app($input = null)
    {
        $app = \Eccube\Application::getInstance();
        if ($input)
            return $app[$input] ?? null;
        return $app;
    }
}
if (!function_exists('config')) {
    function config($key)
    {
        $config = app('config');
        return $config[$key] ?? null;
    }
}

if (!function_exists('dd')) {
    function dd($input = null)
    {
        dump($input);
        exit;
    }
}
if (!function_exists('cache')) {
    function cache($key = null)
    {
        if ($key) return cache()->get($key);
        return new CacheService();
    }
}

if (!function_exists('is_admin')) {
    function is_admin()
    {
        $app = \Eccube\Application::getInstance();
        return $app['is_admin'];
    }
}

if (!function_exists('is_staff')) {
    function is_staff()
    {
        $app = \Eccube\Application::getInstance();
        if ($app->user() !== "anon." && $app->user()->getStaffFlg()) {
            return true;
        }
        return false;
    }
}
if (!function_exists('is_escort_user')) {
    function is_escort_user()
    {
        $app = \Eccube\Application::getInstance();
        /** @var \Eccube\Entity\Customer $user */
        $user = $app->user();
        if ($user !== "anon." && $user->getEscortFlg()) {
            return true;
        }
        return false;
    }
}


if (!function_exists('front_current_shop')) {
    /**
     * @return mixed
     */
    function front_current_shop(): BaseInfo
    {
        $app = \Eccube\Application::getInstance();
        return $app['front_current_shop'];
    }
}

if (!function_exists('admin_current_shop')) {
    /**
     * @return mixed
     */
    function admin_current_shop(): BaseInfo
    {
        $app = \Eccube\Application::getInstance();
        return $app['admin_current_shop'];
    }
}

if (!function_exists('admin_shop_id')) {
    /**
     * @return mixed
     */
    function admin_shop_id()
    {
        $app = \Eccube\Application::getInstance();
        return $app['admin_shop_id'];
    }
}
if (!function_exists('front_shop_id')) {
    /**
     * @return mixed
     */
    function front_shop_id()
    {
        $app = \Eccube\Application::getInstance();
        return $app['front_shop_id'];
    }
}
if (!function_exists('loginById')) {
    /**
     * @param \Eccube\Application $app
     * @param $id
     * @return bool
     */
    function loginById(Eccube\Application $app, $id)
    {
        $User = $app['orm.em']->getRepository(\Eccube\Entity\Customer::class)->find($id);
        if (!$User)
            return false;
        $firewall = 'customer';
        $role = array('ROLE_USER');
        $token = new UsernamePasswordToken($User, null, $firewall, $role);
        $app['security.token_storage']->setToken($token);
        $app['session']->set('_security_' . $firewall, serialize($token));
        $app['session']->save();
        return true;
    }
}

if (!function_exists('generate_banner_url')) {
    function generate_banner_path($image_path)
    {
        $app = \Eccube\Application::getInstance();
        return $app['config']['image_save_realdir'] . '/' . $image_path;
    }
}

if (!function_exists('point')) {
    function point()
    {
        $app = \Eccube\Application::getInstance();
        /** @var PointService $point_service */
        $point_service = $app['point_service'];
        return $point_service;
    }
}


function api_string_parser($input)
{
    $output = [];
    foreach (explode('!', trim($input)) as $item) {
        $data = explode('^', $item);
        $output[$data[0]] = $data[1] ?? null;
    }
    return $output;
}

function base_url()
{
    $app = app();
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];
    $uri = $request->server->get('HTTP_HOST');
    if ($request->server->get('HTTPS') == 'on') {
        $url = 'https://' . $uri;
    } else {
        $url = 'http://' . $uri;
    }
    return $url;
}

function access_ability($app)
{
    //['HTTP_HOST'] . $app['request']['REQUEST_URI']
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];
    /** @var \Symfony\Component\HttpFoundation\Session\Session $session */
    $session = $app['session'];
    $uri = $request->server->get('HTTP_HOST') . $request->server->get('REQUEST_URI');
    if ($request->server->get('HTTPS') == 'on') {
        $url = 'https://' . $uri;
    } else {
        $url = 'http://' . $uri;
    }
    $session->set('next_url', $url);

    // HTTP_REFERER


    if ($app->user() == 'anon.') {
        switch (front_shop_id()) {
            case 1:
                //メンテナンスモード
                if (config('shop_1_maintenance_mode') == true
                    && !(is_array(config('shop_1_maintenance_allow_ip_address')) && in_array(@$_SERVER['REMOTE_ADDR'], config('shop_1_maintenance_allow_ip_address')))) {
                    header('location: ' . $app->url('maintenance'));
                    exit;
                }

                // さーぱショップの場合セッション認証する
                if (empty($app['session']->get('SURPASS_SHOP_AUTH'))) {
                    header('location: ' . $app->url('surpass_login'));
                    exit;
                }
                break;
            case 3:
                if (!isset($_GET['uid'])) {
                    header("HTTP/1.0 404 Not Found", true, 404);
                    exit;
                }
                break;
            case 2:
//                if (!$request->server->get('HTTP_REFERER'))
//                    throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
                header('location: ' . $app->url('mypage_login'));
                exit;
                break;
        }
    } else {
        /** @var \Eccube\Entity\Customer $Customer */
        $Customer = $app->user();
        //メンテナンスモード
        switch (front_shop_id()) {
            case 2:
                if (config('shop_2_maintenance_mode') === true
                    && !(is_array(config('shop_2_maintenance_allow_user_id')) && in_array($Customer->getId(), config('shop_2_maintenance_allow_user_id')))) {
                    header('location: ' . $app->url('maintenance'));
                    exit;
                }
                /** @var CustomerRepository $CustomerRepository */
                $CustomerRepository = $app['orm.em']->getRepository('Eccube\Entity\Customer');
                $CustomerRepository->clearExpiredPoint($app, $Customer);
                break;
            case 3:
                if (config('shop_3_maintenance_mode') === true
                    && !(is_array(config('shop_3_maintenance_allow_user_id')) && in_array($Customer->getId(), config('shop_3_maintenance_allow_user_id')))) {
                    header('location: ' . $app->url('maintenance'));
                    exit;
                }
                /** @var CustomerRepository $CustomerRepository */
                $CustomerRepository = $app['orm.em']->getRepository('Eccube\Entity\Customer');
                $CustomerRepository->clearExpiredPoint($app, $Customer);

                break;
        }

    }


}

function db_select($col, $table, $where = '', $sqlVal = [])
{
    $db = app('db');
    if (empty($where)) {
        $where = '1=1';
    }
    $sql = "SELECT $col FROM  $table WHERE $where ";

    return $db->fetchAll($sql, $sqlVal);
}

function db_pagination($col, $table, $where = '', $sqlVal = [], $page = 1, $display = 3)
{
    $db = app('db');
    if (empty($where)) {
        $where = '1=1';
    }
    $display = (int)$display;
    $page = (int)$page >= 1 ? (int)$page : 1;
    $offset = ($page - 1) * $display;

    $sql = "SELECT $col FROM  $table WHERE $where " . ' LIMIT ' . $offset . ',' . $display;
    $countSQL = "SELECT count(*) count FROM $table WHERE $where";

    $total = ($db->fetchAll($countSQL, $sqlVal))[0]['count'] ?? 0;
    $data = $db->fetchAll($sql, $sqlVal);
    return [
        'totalPage' => (int)ceil($total / $display),
        'display' => count($data),
        'page' => $page,
        'total' => $total,
        'data' => $data
    ];

}

function isEscort($rank_order)
{
    //※「ランクオーダ」項目の値が、8～15、24～31、40～47の値の場合:1
    //その他の場合:0
    if (
        ($rank_order >= 8 && $rank_order <= 15) ||
        ($rank_order >= 24 && $rank_order <= 31) ||
        ($rank_order >= 40 && $rank_order <= 47)) {
        return 1;
    }
    return 0;
}

