<?php
/**
 * Created by PhpStorm.
 * User: duong
 * Date: 2018/09/03
 * Time: 13:36
 */

namespace Eccube\Service;


use Eccube\Application;
use Eccube\Entity\CartItem;
use Eccube\Entity\Customer;
use Eccube\Entity\Order;
use Eccube\Entity\PointTransaction;
use Eccube\Entity\ProductClass;

class PointService
{
    const POINT_ROUND_FLOOR = 0;
    const POINT_ROUND_CEIL = 1;
    const POINT_ROUND_ROUND = 2;
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getData(Order $Order = null)
    {
        $app = $this->app;
        $current = null;
        $is_available = false;
        $balance = null;
        $available_point = null;
        $using_amount = null;
        $max_can_use = null;

        if (is_null($Order)) {
            /** @var Order $Order */
            $Order = $app['eccube.service.shopping']->getOrder($app['config']['order_processing']);
        }
        if ($Order) {
            $using_amount = $Order->getPointUse();
            $max_can_use = (float)$Order->getTotal() + (float)$Order->getPointUse();
        }
        if ($app->isGranted('IS_AUTHENTICATED_FULLY')) {
            $is_available = true;
            /** @var Customer $Customer */
            $Customer = $app->user();
            $available_point = ($Customer->getPoint() - $Order->getPointUse());
            if ($available_point <= 0)
                $available_point = 0;
            $balance = $Customer->getPoint();
        }

        return [
            'is_available' => $is_available,// ポイント
            'conversion_rate' => config('point_conversion_rate'),
            'balance' => (float)$balance,
            'using_amount' => (float)$using_amount,
            'available' => (float)$available_point,
            'cart_total' => (float)$this->getTotalPointInCart(),
            'max_can_use' => (float)$max_can_use,
            'subtotal' => $Order->getSubtotal(),
            'delivery_fee' => $Order->getDeliveryFeeTotal()
        ];
    }

    public function registerWithOrder(Order $Order)
    {
        $app = $this->app;
        $PointTransaction = $app['eccube.repository.point_transaction']->findOneBy([
            'Order' => $Order->getId()
        ]);
        if (!$PointTransaction) {
            $Customer = $Order->getCustomer();
            if ($Customer) {
                $PointTransaction = new PointTransaction();
                $PointTransaction->setCustomer($Customer);
                $PointTransaction->setOrder($Order);
                if ($Order->getPointUse()) {
                    $PointSubtract = clone $PointTransaction;
                    $PointSubtract->setCommitDate(new \DateTime());
                    $PointSubtract->setPoint((int)$Order->getPointUse());
                    $PointSubtract->setStatus(PointTransaction::CONFIRMED);
                    $PointSubtract->setType(PointTransaction::TRANSACTION_TYPE_SUBTRACT);
                    $PointSubtract->setDescription("ポイント利用");
                    $PointSubtract->setExtendFlg(PointTransaction::EXTEND_FLAG_ON);
                    $this->app['orm.em']->persist($PointSubtract);
                }
                if ($Order->getPointAdd()) {
                    $PointAdd = clone $PointTransaction;
                    $PointAdd->setPoint((int)$Order->getPointAdd());
                    $PointAdd->setStatus(PointTransaction::NOT_CONFIRMED);
                    $PointAdd->setType(PointTransaction::TRANSACTION_TYPE_ADD);
                    $PointAdd->setDescription("お買い物");
                    $PointAdd->setExtendFlg(PointTransaction::EXTEND_FLAG_ON);
                    $this->app['orm.em']->persist($PointAdd);
                }
                $this->app['orm.em']->flush();
                $app['eccube.repository.customer']->updatePoint($Order->getCustomer());
            }

        }
    }

    public function registerPoint(Customer $Customer, $point, $type, $description, $extend_flg = 0)
    {
        if (!$Customer->getId() || $point <= 0) {
            return false;
        }
        $PointTransaction = new PointTransaction();
        $PointTransaction->setCustomer($Customer);
        $PointTransaction->setPoint((int)$point);
        $PointTransaction->setType($type);
        $PointTransaction->setDescription($description);
        $PointTransaction->setCommitDate(new \DateTime());
        $PointTransaction->setStatus(PointTransaction::CONFIRMED);
        $PointTransaction->setExtendFlg($extend_flg);

        $this->app['orm.em']->persist($PointTransaction);
        $this->app['orm.em']->flush();
        return $PointTransaction;
    }

    public function confirmPointWithOrder(Order $Order)
    {


    }

    public function getTotalPointInCart()
    {
        $Cart = $this->app['eccube.service.cart']->getCart();
        $total_point = 0;
        /** @var CartItem $CartItem */
        foreach ($Cart->getCartItems() as $CartItem) {
            if ($CartItem->getObject() instanceof ProductClass)
                $total_point += $CartItem->getObject()->getPoint() * $CartItem->getQuantity();
        }
        return $total_point;
    }

    public function calculatePointAmountFromPointRate($rate, $price)
    {
        return $this->round($rate * $price / 100);
    }

    private function round($value)
    {
        $roundType = config('point_round_type');
        // 切り上げ
        if ($roundType == self::POINT_ROUND_CEIL)
            return ceil($value);
        // 四捨五入
        if ($roundType == self::POINT_ROUND_ROUND)
            return round($value, 0);
        return floor($value);
    }
}