<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller;

use Eccube\Application;
use Eccube\Entity\BlocPosition;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SurpassController
{

    public function login(Application $app, Request $request)
    {
        if (front_shop_id() != 1) {
            throw new AccessDeniedHttpException();
        }
        if ($app['session']->has('SURPASS_SHOP_AUTH')) {
            return $app->redirect($app->url('homepage'));
        }
        /** @var \Symfony\Component\HttpFoundation\Session\Session $session */
        $session = $app['session'];

        $error = false;
        if ($request->getMethod() == 'POST') {
            if ($request->get('login_id') == config('surpass_login_id') && $request->get('login_pass') == config('surpass_login_password')) {
                $app['session']->set('SURPASS_SHOP_AUTH', sha1($request->get('login_id') . '_' . $request->get('login_pass')));
//                return $app->redirect($app->url('homepage'));
                return $app->redirect($session->get('next_url', $app->url('homepage')));
            } else {
                $error = true;
            }
        }

        return $app->render('surpass_login.twig', [
            'error' => $error
        ]);
    }

    public function logout(Application $app)
    {
        if (front_shop_id() != 1) {
            throw new AccessDeniedHttpException();
        }
        $app['session']->remove('SURPASS_SHOP_AUTH');
        return $app->redirect($app->url('surpass_login'));
    }
}
