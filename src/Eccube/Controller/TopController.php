<?php namespace Eccube\Controller;

use Eccube\Application;
use Eccube\Entity\Customer;

class TopController extends AbstractController
{
    public function index(Application $app)
    {
        $ProductRanking = $app['eccube.repository.product_ranking']->getList();

        //DEBUG ONLYリリース持削除してください。
        if (isset($_GET['uid']) && front_shop_id() == 3) {
            /** @var Customer $Customer */
            $Customer = $app['eccube.repository.customer']->findOneBy([
                'id' => $_GET['uid'],
                'Shop' => front_current_shop()
            ]);
            if ($Customer) {
                loginById($app, $Customer->getId());
                return 'ログインしました、<a href="/">トップへ戻る</a>';
            } else {
                return "ユーザー情報が見つかりません。";
            }
        }


        access_ability($app);
        return $app->render('index.twig', [
            'ProductRanking' => $ProductRanking,
        ]);
    }

    public function smtIndex(Application $app)
    {
        return $app->redirect($app->url('top'));
    }

    public function maintenance(Application $app)
    {
        switch (front_shop_id()) {
            case 1:
                if (config('shop_1_maintenance_mode') != true) {
                    return $app->redirect($app->url('top'));
                }
                break;
            case 2:
                if (config('shop_2_maintenance_mode') != true) {
                    return $app->redirect($app->url('top'));
                }
                break;
            case 3:
                if (config('shop_3_maintenance_mode') != true) {
                    return $app->redirect($app->url('top'));
                }
                break;
        }
        return $app->render('maintenance.twig');
    }

}
