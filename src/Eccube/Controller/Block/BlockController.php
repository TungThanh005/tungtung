<?php


namespace Eccube\Controller\Block;

use Eccube\Application;
use Eccube\Entity\Category;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpFoundation\Request;

class BlockController
{
    public function index(Application $app, Request $request)
    {
        /** @var $form \Symfony\Component\Form\Form */
        $builder = $app['form.factory']
            ->createNamedBuilder('', 'search_product_block')
            ->setMethod('GET');

        $event = new EventArgs(
            array(
                'builder' => $builder,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_BLOCK_SEARCH_PRODUCT_INDEX_INITIALIZE, $event);

        /** @var $request \Symfony\Component\HttpFoundation\Request */
        $request = $app['request_stack']->getMasterRequest();

        $form = $builder->getForm();
        $form->handleRequest($request);

        $Categories = $app['eccube.repository.category']->getList();

        return $app->render('Block/header.twig', array(
            'form' => $form->createView(),
            'Categories' => $Categories
        ));
    }

    public function topSlider(Application $app)
    {
        $Banners = $app['eccube.repository.banner']->getListByPosition(1);

        return $app->render('Block/top_slider.twig', array(
            'Banners' => $Banners
        ));
    }

    public function leftBanner(Application $app, Request $request)
    {
        $Banners = $app['eccube.repository.banner']->getListByPosition(4);
        if (isset($_GET['category_id']) && $categoryId = $_GET['category_id']) {
            $Category = $app['eccube.repository.category']->find($categoryId);
        }
        if (!isset($Category) || !$Category) {
            $Category = new Category();
        }

        return $app->render('Block/left_banner.twig', array(
            'Banners' => $Banners,
            'Category' => $Category
        ));
    }

    public function featureBanner(Application $app)
    {
        $Banners = $app['eccube.repository.banner']->getListByPosition(2);

        return $app->render('Block/feature_banner.twig', array(
            'Banners' => $Banners
        ));
    }

    public function campaignBanner(Application $app)
    {
        $Banners = $app['eccube.repository.banner']->getListByPosition(3);
        return $app->render('Block/campaign_banner.twig', array(
            'Banners' => $Banners
        ));
    }

    public function leftLogin(Application $app)
    {
        return $app->render('Block/left_login.twig', array());
    }


    public function leftNav(Application $app)
    {
        return $app->render('Block/left_nav.twig', array());
    }

    public function seasonProduct(Application $app)
    {
        $Products = $app['eccube.repository.product']
            ->getQueryBuilderBySearchData([
                'tags' => [config('season_product_tag_id')],
                'orderby' => $app['orm.em']->getRepository('Eccube\Entity\Master\ProductListOrderBy')->find($app['config']['product_order_newer'])
            ])
            ->setMaxResults(config('top_page_season_product_display'))
            ->getQuery()
            ->getResult();
        return $app->render('Block/season_product.twig', compact('Products'));
    }

    public function featureProduct(Application $app)
    {

        $Products = $app['eccube.repository.product']
            ->getQueryBuilderBySearchData([
                'tags' => [config('feature_product_tag_id')],
                'orderby' => $app['orm.em']->getRepository('Eccube\Entity\Master\ProductListOrderBy')->find($app['config']['product_order_newer'])
            ])
            ->setMaxResults($app['config']['top_page_feature_product_display'])
            ->getQuery()
            ->getResult();

        return $app->render('Block/feature_product.twig', compact('Products'));
    }

    public function rankingProduct(Application $app)
    {
        $ProductRanking = $app['eccube.repository.product_ranking']->getList();
        return $app->render('Block/product_ranking.twig', array(
            'ProductRanking' => $ProductRanking,
        ));
    }

    public function category(Application $app)
    {
        $Categories = $app['eccube.repository.category']->getList();

        return $app->render('Block/category.twig', array(
            'Categories' => $Categories
        ));
    }

    public function news(Application $app)
    {
        $qb = $app['orm.em']->getRepository('\Eccube\Entity\News')->getList();
        $pagination = $app['paginator']()->paginate(
            $qb,
            1,
            $app['config']['top_page_news_display']
        );
        return $app->render('Block/news.twig', array(
            'NewsList' => $pagination,
        ));
    }


    public function productSearch(Application $app, Request $request)
    {
        /** @var $form \Symfony\Component\Form\Form */
        $builder = $app['form.factory']
            ->createNamedBuilder('', 'search_product_block')
            ->setMethod('GET');

        $event = new EventArgs(
            array(
                'builder' => $builder,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_BLOCK_SEARCH_PRODUCT_INDEX_INITIALIZE, $event);

        /** @var $request \Symfony\Component\HttpFoundation\Request */
        $request = $app['request_stack']->getMasterRequest();

        $form = $builder->getForm();
        $form->handleRequest($request);

        return $app->render('Block/search_product.twig', array(
            'form' => $form->createView(),
        ));
    }


}
