<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller;

use Eccube\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NewsController extends AbstractController
{
    public function index(Application $app, Request $request)
    {
        access_ability($app);
        $qb = $app['orm.em']->getRepository('\Eccube\Entity\News')->getList();
        $pagination = $app['paginator']()->paginate(
            $qb,
            $request->get('pageno') ? $request->get('pageno') : 1,
            $app['config']['news_news_display']
        );

        return $app->render('news.twig', array(
            'NewsList' => $pagination,
        ));
    }

    public function detail(Application $app, $id)
    {
        access_ability($app);
        $News = $app['orm.em']->getRepository('\Eccube\Entity\News')->findOneBy([
            'Shop' => front_shop_id(),
            'id' => $id
        ]);
        if (!$News) {
            throw new NotFoundHttpException();
        }
        return $app->render('news_detail.twig', array(
            'News' => $News,
        ));
    }

}
