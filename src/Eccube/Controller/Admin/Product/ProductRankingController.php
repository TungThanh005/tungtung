<?php

namespace Eccube\Controller\Admin\Product;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Entity\ProductRanking;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductRankingController extends AbstractController
{
    public function index(Application $app, Request $request)
    {
        $pagination = $app['eccube.repository.product_ranking']->getListForAdmin();
        $searchProductModalForm = $app['form.factory']->createBuilder('admin_search_product')->getForm();

        return $app->render('Setting/Shop/product_ranking.twig', array(
            'pagination' => $pagination,
            'total_item_count' => count($pagination),
            'searchProductModalForm' => $searchProductModalForm->createView(),

        ));
    }

    public function add(Application $app, Request $request)
    {
        if ($request->request->has('product_id')) {
            $product_id = $request->request->get('product_id');
            $Product = $app['eccube.repository.product']->find($product_id);
            if (!$Product) {
                $app->addError("選択した商品が見つかりません。", "admin");
                return $app->redirect($app->url('admin_setting_ranking_list'));
            }
            $ProductRanking = $app['eccube.repository.product_ranking']->findOneBy([
                'Product' => $Product
            ]);

            if (!$ProductRanking) {
                $rank = $app['eccube.repository.product_ranking']->countRecommend();

                $ProductRanking = new ProductRanking();
                $ProductRanking->setProduct($Product);
                $ProductRanking->setRank($rank + 1);
                $ProductRanking->setDelFlg(0);
                $app['orm.em']->persist($ProductRanking);
                $app['orm.em']->flush();
                $app->addSuccess("商品追加しました", "admin");
            } else {
                $app->addError("既にリスト追加済み。", "admin");
            }
        }
        return $app->redirect($app->url('admin_setting_ranking_list'));
    }

    public function delete(Application $app, Request $request, $id)
    {
        $this->isTokenValid($app);
        if (!'POST' === $request->getMethod()) {
            log_error('Delete with bad method!');
            throw new BadRequestHttpException();
        }
        if (!$id) {
            $app->addError('admin.product.ranking.recommend_id.not_exists', 'admin');
            return $this->redirect($app);
        }
        $ProductRecommend = $app['eccube.repository.product_ranking']->find($id);
        if ($ProductRecommend) {
            $ProductRecommend->setDelFlg(Constant::ENABLED);
            $app->addSuccess('admin.product.ranking.delete.success', 'admin');
            $app['orm.em']->persist($ProductRecommend);
            $app['orm.em']->flush();
        }
        return $app->redirect($app->url('admin_setting_ranking_list'));
    }

    public function moveRank(Application $app, Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $arrRank = $request->request->all();
            $arrRankMoved = $app['eccube.repository.product_ranking']->moveRecommendRank($arrRank);
            log_info('Recommend move rank', $arrRankMoved);
        }
        return true;
    }


    public function searchProduct(Application $app, Request $request, $page_no = null)
    {
        if (!$request->isXmlHttpRequest()) {
            return null;
        }
        log_debug('Search product start.');
        $pageCount = $app['config']['default_page_count'];
        $session = $app['session'];
        if ('POST' === $request->getMethod()) {
            $page_no = 1;
            $searchData = array(
                'name' => trim($request->get('id')),
            );

            if ($categoryId = $request->get('category_id')) {
                $searchData['category_id'] = $categoryId;
            }

            $session->set('eccube.admin.product.recommend.product.search', $searchData);
            $session->set('eccube.admin.product.recommend.product.search.page_no', $page_no);
        } else {
            $searchData = (array)$session->get('eccube.admin.product.recommend.product.search');
            if (is_null($page_no)) {
                $page_no = intval($session->get('eccube.admin.product.recommend.product.search.page_no'));
            } else {
                $session->set('eccube.admin.product.recommend.product.search.page_no', $page_no);
            }
        }
        //set parameter
        $searchData['id'] = $searchData['name'];
        if (!empty($searchData['category_id'])) {
            $searchData['category_id'] = $app['eccube.repository.category']->find($searchData['category_id']);
        }
        $qb = $app['eccube.repository.product']->getQueryBuilderBySearchDataForAdmin($searchData);
        /** @var \Knp\Component\Pager\Pagination\SlidingPagination $pagination */
        $pagination = $app['paginator']()->paginate(
            $qb,
            $page_no,
            $pageCount,
            array('wrap-queries' => true)
        );
        /** @var ArrayCollection */
        $arrProduct = $pagination->getItems();

        log_debug('Search product finish.');
        if (count($arrProduct) == 0) {
            log_debug('Search product not found.');
        }
        return $app->render('Setting/Shop/product_ranking_search.twig', array(
            'pagination' => $pagination));
    }

}
