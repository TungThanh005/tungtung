<?php

namespace Eccube\Controller\Admin\Product;

use Eccube\Application;
use Eccube\Controller\AbstractController;
use Eccube\Entity\Master\Maker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MakerController extends AbstractController
{
    public function index(Application $app, Request $request, $id = null)
    {
        $makers = $app['eccube.repository.master.maker']->findAll();

        if ($request->getMethod() == 'POST') {
            if (!$request->request->get('name')) {
                $app->addError('メーカー名を入力してください。', 'admin');
                return $app->redirect($app->url('admin_product_maker'));
            }
            if ($request->request->get('id') && $request->request->get('mode') == 'edit') {
                $Maker = $app['eccube.repository.maker']->find($request->request->get('id'));
                $message = 'メーカー情報を変更しました。';
            } else {
                $Maker = new Maker();
                $Maker->setRank($app['eccube.repository.master.maker']->getLastRank());
                $message = 'メーカー情報を登録しました。';
            }
            $Maker->setName($request->request->get('name'));
            $app['orm.em']->persist($Maker);
            $app['orm.em']->flush();
            $app->addSuccess($message, 'admin');

            return $app->redirect($app->url('admin_product_maker'));
        }
        if ($id) {
            $Maker = $app['eccube.repository.maker']->find($id);
        } else {
            $Maker = new Maker();
        }

        return $app->render('Product/maker.twig', array(
            'makers' => $makers,
            'TargetMaker' => $Maker,
            'id' => $id
        ));
    }

    public function edit(Application $app, Request $request, $id = null)
    {
        if (is_null($id)) {
            $Maker = new Maker();
        } else {
            $Maker = $app['eccube.repository.maker']->findOneBy([
                'maker_id' => $id
            ]);
            if (!$Maker) {
                throw new NotFoundHttpException();
            }
        }

        $builder = $app['form.factory']
            ->createBuilder('maker', $Maker);

        $form = $builder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $app['orm.em']->persist($Maker);
            $app['orm.em']->flush();
            $app->addSuccess('メーカー情報を保存しました', 'admin');
            return $app->redirect($app->url('admin_product_maker'));
        }
        return $app->render('Product/maker_edit.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function delete(Application $app, Request $request, $id = null)
    {
        if (!is_null($id)) {
            /** @var Maker $Maker */
            $Maker = $app['eccube.repository.maker']->find($id);
            if ($Maker->getProducts()->count()) {
                $app->addError('商品がある為、メーカーを削除できませんでした。', 'admin');
            } else {
                $app['orm.em']->remove($Maker);
                $app['orm.em']->flush();
                $app->addSuccess('メーカーを削除しました。', 'admin');
            }
        }
        return $app->redirect($app->url('admin_product_maker'));
    }
}