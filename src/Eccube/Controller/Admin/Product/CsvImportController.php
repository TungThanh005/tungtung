<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller\Admin\Product;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Entity\Category;
use Eccube\Entity\Delivery;
use Eccube\Entity\DeliveryFee;
use Eccube\Entity\DeliveryTime;
use Eccube\Entity\Master\Pref;
use Eccube\Entity\Payment;
use Eccube\Entity\PaymentOption;
use Eccube\Entity\PointTransaction;
use Eccube\Entity\Product;
use Eccube\Entity\ProductCategory;
use Eccube\Entity\ProductClass;
use Eccube\Entity\ProductImage;
use Eccube\Entity\ProductStock;
use Eccube\Entity\ProductTag;
use Eccube\Entity\ProductUnsoldableOffice;
use Eccube\Exception\CsvImportException;
use Eccube\Service\CsvImportService;
use Eccube\Util\Str;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CsvImportController
{

    private $errors = array();

    private $fileName;

    private $em;

    private $productTwig = 'Product/csv_product.twig';

    private $categoryTwig = 'Product/csv_category.twig';

    private $deliveryTwig = 'Setting/Shop/csv_delivery.twig';

    private $pointTwig = 'Customer/csv_point.twig';


    /**
     * 商品登録CSVアップロード
     */
    public function csvProduct(Application $app, Request $request)
    {
        $form = $app['form.factory']->createBuilder('admin_csv_import')->getForm();

        $headers = $this->getProductCsvHeader();

        if ('POST' === $request->getMethod()) {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $formFile = $form['import_file']->getData();

                if (!empty($formFile)) {

                    log_info('商品CSV登録開始');

                    $data = $this->getImportData($app, $formFile);
                    if ($data === false) {
                        $this->addErrors('CSVのフォーマットが一致しません。');

                        return $this->render($app, $form, $headers, $this->productTwig);
                    }


                    $arrRequireHeader = array(
                        '公開ステータス(ID)',
                        'サーパスショップ（契約者）公開ステータス(ID)',
                        'サーパスショップ（入居者）公開ステータス(ID)',
                        'くらしスクエアショップ公開ステータス(ID)',
                        '商品名', '商品種別(ID)', '在庫数無制限フラグ', '販売価格');
                    $columnHeaders = $data->getColumnHeaders();
                    if (count(array_diff($arrRequireHeader, $columnHeaders)) > 0) {
                        $this->addErrors('CSVのフォーマットが一致しません。');

                        return $this->render($app, $form, $headers, $this->productTwig);
                    }

                    $size = count($data);
                    if ($size < 1) {
                        $this->addErrors('CSVデータが存在しません。');

                        return $this->render($app, $form, $headers, $this->productTwig);
                    }

                    $this->em = $app['orm.em'];
                    $this->em->getConfiguration()->setSQLLogger(null);

                    $this->em->getConnection()->beginTransaction();

                    $BaseInfo = $app['eccube.repository.base_info']->get();

                    // CSVファイルの登録処理
                    foreach ($data as $row) {

                        if (isset($row['商品ID']) && Str::isNotBlank($row['商品ID'])) {
                            if (preg_match('/^\d+$/', $row['商品ID'])) {
                                $Product = $app['eccube.repository.product']->find($row['商品ID']);
                                if (!$Product) {
                                    $this->addErrors(($data->key() + 1) . '行目の商品IDが存在しません。');

                                    return $this->render($app, $form, $headers, $this->productTwig);
                                }
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目の商品IDが存在しません。');

                                return $this->render($app, $form, $headers, $this->productTwig);
                            }
                        } else {
                            $Product = new Product();
                            $this->em->persist($Product);
                        }

                        if (Str::isBlank($row['公開ステータス(ID)'])) {
                            $this->addErrors(($data->key() + 1) . '行目の公開ステータス(ID)が設定されていません。');
                        } else {
                            if (preg_match('/^\d+$/', $row['公開ステータス(ID)'])) {
                                $Disp = $app['eccube.repository.master.disp']->find($row['公開ステータス(ID)']);
                                if (!$Disp) {
                                    $this->addErrors(($data->key() + 1) . '行目の公開ステータス(ID)が存在しません。');
                                } else {
                                    $Product->setStatus($Disp);
                                }
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目の公開ステータス(ID)が存在しません。');
                            }
                        }

                        if (Str::isBlank($row['サーパスショップ（契約者）公開ステータス(ID)'])) {
                            $this->addErrors(($data->key() + 1) . '行目のサーパスショップ（契約者）公開ステータス(ID)が設定されていません。');
                        } else {
                            if (preg_match('/^\d+$/', $row['サーパスショップ（契約者）公開ステータス(ID)'])) {
                                $Disp = $app['eccube.repository.master.disp']->find($row['サーパスショップ（契約者）公開ステータス(ID)']);
                                if (!$Disp) {
                                    $this->addErrors(($data->key() + 1) . '行目のサーパスショップ（契約者）公開ステータス(ID)が存在しません。');
                                } else {
                                    $Product->setStatusShop1($Disp);
                                }
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目のサーパスショップ（契約者）公開ステータス(ID)が存在しません。');
                            }
                        }

                        if (Str::isBlank($row['サーパスショップ（入居者）公開ステータス(ID)'])) {
                            $this->addErrors(($data->key() + 1) . '行目のサーパスショップ（入居者）公開ステータス(ID)が設定されていません。');
                        } else {
                            if (preg_match('/^\d+$/', $row['サーパスショップ（入居者）公開ステータス(ID)'])) {
                                $Disp = $app['eccube.repository.master.disp']->find($row['サーパスショップ（入居者）公開ステータス(ID)']);
                                if (!$Disp) {
                                    $this->addErrors(($data->key() + 1) . '行目のサーパスショップ（入居者）公開ステータス(ID)が存在しません。');
                                } else {
                                    $Product->setStatusShop2($Disp);
                                }
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目のサーパスショップ（入居者）公開ステータス(ID)が存在しません。');
                            }
                        }

                        if (Str::isBlank($row['くらしスクエアショップ公開ステータス(ID)'])) {
                            $this->addErrors(($data->key() + 1) . '行目のくらしスクエアショップ公開ステータス(ID)が設定されていません。');
                        } else {
                            if (preg_match('/^\d+$/', $row['くらしスクエアショップ公開ステータス(ID)'])) {
                                $Disp = $app['eccube.repository.master.disp']->find($row['くらしスクエアショップ公開ステータス(ID)']);
                                if (!$Disp) {
                                    $this->addErrors(($data->key() + 1) . '行目のくらしスクエアショップ公開ステータス(ID)が存在しません。');
                                } else {
                                    $Product->setStatusShop3($Disp);
                                }
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目のくらしスクエアショップ公開ステータス(ID)が存在しません。');
                            }
                        }

                        if (Str::isBlank($row['商品名'])) {
                            $this->addErrors(($data->key() + 1) . '行目の商品名が設定されていません。');

                            return $this->render($app, $form, $headers, $this->productTwig);
                        } else {
                            $Product->setName(Str::trimAll($row['商品名']));
                        }
                        if (!Str::isBlank($row['提携ショップ'])) {
                            $Product->setAgency(Str::trimAll($row['提携ショップ']));
                        }

                        if (isset($row['ショップ用メモ欄']) && Str::isNotBlank($row['ショップ用メモ欄'])) {
                            $Product->setNote(Str::trimAll($row['ショップ用メモ欄']));
                        } else {
                            $Product->setNote(null);
                        }

                        if (isset($row['商品説明(一覧)']) && Str::isNotBlank($row['商品説明(一覧)'])) {
                            $Product->setDescriptionList(Str::trimAll($row['商品説明(一覧)']));
                        } else {
                            $Product->setDescriptionList(null);
                        }

                        if (isset($row['商品説明(詳細)']) && Str::isNotBlank($row['商品説明(詳細)'])) {
                            $Product->setDescriptionDetail(Str::trimAll($row['商品説明(詳細)']));
                        } else {
                            $Product->setDescriptionDetail(null);
                        }

                        if (isset($row['検索ワード']) && Str::isNotBlank($row['検索ワード'])) {
                            $Product->setSearchWord(Str::trimAll($row['検索ワード']));
                        } else {
                            $Product->setSearchWord(null);
                        }

                        if (isset($row['フリーエリア']) && Str::isNotBlank($row['フリーエリア'])) {
                            $Product->setFreeArea(Str::trimAll($row['フリーエリア']));
                        } else {
                            $Product->setFreeArea(null);
                        }

                        if (isset($row['商品削除フラグ']) && Str::isNotBlank($row['商品削除フラグ'])) {
                            if ($row['商品削除フラグ'] == (string)Constant::DISABLED || $row['商品削除フラグ'] == (string)Constant::ENABLED) {
                                $Product->setDelFlg($row['商品削除フラグ']);
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目の商品削除フラグが設定されていません。');

                                return $this->render($app, $form, $headers, $this->productTwig);
                            }
                        } else {
                            $Product->setDelFlg(Constant::DISABLED);
                        }

                        if (isset($row['ファインパスポート対象']) && Str::isNotBlank($row['ファインパスポート対象'])) {
                            if ($row['ファインパスポート対象'] == (string)Constant::DISABLED || $row['ファインパスポート対象'] == (string)Constant::ENABLED) {
                                $Product->setFinePassportFlg($row['ファインパスポート対象']);
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目のファインパスポート対象が設定されていません。');
                                return $this->render($app, $form, $headers, $this->productTwig);
                            }
                        } else {
                            $Product->setFinePassportFlg(Constant::DISABLED);
                        }

                        if (isset($row['エスコート対象']) && Str::isNotBlank($row['エスコート対象'])) {
                            if ($row['エスコート対象'] == (string)Constant::DISABLED || $row['エスコート対象'] == (string)Constant::ENABLED) {
                                $Product->setEscortFlg($row['エスコート対象']);
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目のエスコート対象が設定されていません。');
                                return $this->render($app, $form, $headers, $this->productTwig);
                            }
                        } else {
                            $Product->setEscortFlg(Constant::DISABLED);
                        }

                        if (isset($row['販売不可事業所(ID)']) && Str::isNotBlank($row['販売不可事業所(ID)'])) {
                            foreach ($Product->getProductUnsoldableOffices() as $productUnsoldableOffice) {
                                $app['orm.em']->remove($productUnsoldableOffice);
                            }
                            foreach (explode(",", $row['販売不可事業所(ID)']) as $office_no) {
                                if (empty($office_no)) continue;
                                $Office = $app['eccube.repository.office']->findOneBy([
                                    'office_no' => $office_no
                                ]);
                                if (!$Office) {
                                    $this->addErrors(($data->key() + 1) . '行目の「' . $office_no . '」事業所を見つかりません"。');
                                } else {
                                    $ProductUnsoldableOffice = new ProductUnsoldableOffice();
                                    $ProductUnsoldableOffice->setOffice($Office);
                                    $ProductUnsoldableOffice->setProduct($Product);
                                    $app['orm.em']->persist($ProductUnsoldableOffice);
                                }
                            }
                        } else {
                            foreach ($Product->getProductUnsoldableOffices() as $productUnsoldableOffice) {
                                $app['orm.em']->remove($productUnsoldableOffice);
                            }
                        }


                        // 商品画像登録
                        $this->createProductImage($row, $Product, $data);

                        $this->em->flush($Product);

                        // 商品カテゴリ登録
                        $this->createProductCategory($row, $Product, $app, $data);

                        //タグ登録
                        $this->createProductTag($row, $Product, $app, $data);

                        // 商品規格が存在しなければ新規登録
                        $ProductClasses = $Product->getProductClasses();
                        if ($ProductClasses->count() < 1) {
                            // 規格分類1(ID)がセットされていると規格なし商品、規格あり商品を作成
                            $ProductClassOrg = $this->createProductClass($row, $Product, $app, $data);
                            if ($BaseInfo->getOptionProductDeliveryFee() == Constant::ENABLED) {
                                if (isset($row['送料']) && Str::isNotBlank($row['送料'])) {
                                    $deliveryFee = str_replace(',', '', $row['送料']);
                                    if (preg_match('/^\d+$/', $deliveryFee) && $deliveryFee >= 0) {
                                        $ProductClassOrg->setDeliveryFee($deliveryFee);
                                    } else {
                                        $this->addErrors(($data->key() + 1) . '行目の送料は0以上の数値を設定してください。');
                                    }
                                }
                            }

                            if (isset($row['規格分類1(ID)']) && Str::isNotBlank($row['規格分類1(ID)'])) {

                                if (isset($row['規格分類2(ID)']) && $row['規格分類1(ID)'] == $row['規格分類2(ID)']) {
                                    $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)と規格分類2(ID)には同じ値を使用できません。');
                                } else {
                                    // 商品規格あり
                                    // 規格分類あり商品を作成
                                    $ProductClass = clone $ProductClassOrg;
                                    $ProductStock = clone $ProductClassOrg->getProductStock();

                                    // 規格分類1、規格分類2がnullであるデータの削除フラグを1にセット
                                    $ProductClassOrg->setDelFlg(Constant::ENABLED);

                                    // 規格分類1、2をそれぞれセットし作成
                                    $ClassCategory1 = null;
                                    if (preg_match('/^\d+$/', $row['規格分類1(ID)'])) {
                                        $ClassCategory1 = $app['eccube.repository.class_category']->find($row['規格分類1(ID)']);
                                        if (!$ClassCategory1) {
                                            $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)が存在しません。');
                                        } else {
                                            $ProductClass->setClassCategory1($ClassCategory1);
                                        }
                                    } else {
                                        $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)が存在しません。');
                                    }

                                    if (isset($row['規格分類2(ID)']) && Str::isNotBlank($row['規格分類2(ID)'])) {
                                        if (preg_match('/^\d+$/', $row['規格分類2(ID)'])) {
                                            $ClassCategory2 = $app['eccube.repository.class_category']->find($row['規格分類2(ID)']);
                                            if (!$ClassCategory2) {
                                                $this->addErrors(($data->key() + 1) . '行目の規格分類2(ID)が存在しません。');
                                            } else {
                                                if ($ClassCategory1 &&
                                                    ($ClassCategory1->getClassName()->getId() == $ClassCategory2->getClassName()->getId())
                                                ) {
                                                    $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)と規格分類2(ID)の規格名が同じです。');
                                                } else {
                                                    $ProductClass->setClassCategory2($ClassCategory2);
                                                }
                                            }
                                        } else {
                                            $this->addErrors(($data->key() + 1) . '行目の規格分類2(ID)が存在しません。');
                                        }
                                    }
                                    $ProductClass->setProductStock($ProductStock);
                                    $ProductStock->setProductClass($ProductClass);

                                    $this->em->persist($ProductClass);
                                    $this->em->persist($ProductStock);
                                }

                            } else {
                                if (isset($row['規格分類2(ID)']) && Str::isNotBlank($row['規格分類2(ID)'])) {
                                    $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)が存在しません。');
                                }
                            }

                        } else {
                            // 商品規格の更新

                            $flag = false;
                            $classCategoryId1 = Str::isBlank($row['規格分類1(ID)']) ? null : $row['規格分類1(ID)'];
                            $classCategoryId2 = Str::isBlank($row['規格分類2(ID)']) ? null : $row['規格分類2(ID)'];

                            foreach ($ProductClasses as $pc) {

                                $classCategory1 = is_null($pc->getClassCategory1()) ? null : $pc->getClassCategory1()->getId();
                                $classCategory2 = is_null($pc->getClassCategory2()) ? null : $pc->getClassCategory2()->getId();

                                // 登録されている商品規格を更新
                                if ($classCategory1 == $classCategoryId1 &&
                                    $classCategory2 == $classCategoryId2
                                ) {
                                    $this->updateProductClass($row, $Product, $pc, $app, $data);

                                    if ($BaseInfo->getOptionProductDeliveryFee() == Constant::ENABLED) {
                                        if (isset($row['送料']) && Str::isNotBlank($row['送料'])) {
                                            $deliveryFee = str_replace(',', '', $row['送料']);
                                            if (preg_match('/^\d+$/', $deliveryFee) && $deliveryFee >= 0) {
                                                $pc->setDeliveryFee($deliveryFee);
                                            } else {
                                                $this->addErrors(($data->key() + 1) . '行目の送料は0以上の数値を設定してください。');
                                            }
                                        }
                                    }

                                    $flag = true;
                                    break;
                                }
                            }

                            // 商品規格を登録
                            if (!$flag) {
                                $pc = $ProductClasses[0];
                                if ($pc->getClassCategory1() == null &&
                                    $pc->getClassCategory2() == null
                                ) {

                                    // 規格分類1、規格分類2がnullであるデータの削除フラグを1にセット
                                    $pc->setDelFlg(Constant::ENABLED);
                                }

                                if (isset($row['規格分類1(ID)']) && isset($row['規格分類2(ID)']) && $row['規格分類1(ID)'] == $row['規格分類2(ID)']) {
                                    $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)と規格分類2(ID)には同じ値を使用できません。');
                                } else {

                                    // 必ず規格分類1がセットされている
                                    // 規格分類1、2をそれぞれセットし作成
                                    $ClassCategory1 = null;
                                    if (preg_match('/^\d+$/', $classCategoryId1)) {
                                        $ClassCategory1 = $app['eccube.repository.class_category']->find($classCategoryId1);
                                        if (!$ClassCategory1) {
                                            $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)が存在しません。');
                                        }
                                    } else {
                                        $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)が存在しません。');
                                    }

                                    $ClassCategory2 = null;
                                    if (isset($row['規格分類2(ID)']) && Str::isNotBlank($row['規格分類2(ID)'])) {
                                        if ($pc->getClassCategory1() != null && $pc->getClassCategory2() == null) {
                                            $this->addErrors(($data->key() + 1) . '行目の規格分類2(ID)は設定できません。');
                                        } else {
                                            if (preg_match('/^\d+$/', $classCategoryId2)) {
                                                $ClassCategory2 = $app['eccube.repository.class_category']->find($classCategoryId2);
                                                if (!$ClassCategory2) {
                                                    $this->addErrors(($data->key() + 1) . '行目の規格分類2(ID)が存在しません。');
                                                } else {
                                                    if ($ClassCategory1 &&
                                                        ($ClassCategory1->getClassName()->getId() == $ClassCategory2->getClassName()->getId())
                                                    ) {
                                                        $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)と規格分類2(ID)の規格名が同じです。');
                                                    }
                                                }
                                            } else {
                                                $this->addErrors(($data->key() + 1) . '行目の規格分類2(ID)が存在しません。');
                                            }

                                        }
                                    } else {
                                        if ($pc->getClassCategory1() != null && $pc->getClassCategory2() != null) {
                                            $this->addErrors(($data->key() + 1) . '行目の規格分類2(ID)に値を設定してください。');
                                        }
                                    }
                                    $ProductClass = $this->createProductClass($row, $Product, $app, $data, $ClassCategory1, $ClassCategory2);

                                    if ($BaseInfo->getOptionProductDeliveryFee() == Constant::ENABLED) {
                                        if (isset($row['送料']) && Str::isNotBlank($row['送料'])) {
                                            $deliveryFee = str_replace(',', '', $row['送料']);
                                            if (preg_match('/^\d+$/', $deliveryFee) && $deliveryFee >= 0) {
                                                $ProductClass->setDeliveryFee($deliveryFee);
                                            } else {
                                                $this->addErrors(($data->key() + 1) . '行目の送料は0以上の数値を設定してください。');
                                            }
                                        }
                                    }

                                    $Product->addProductClass($ProductClass);
                                }

                            }

                        }


                        if ($this->hasErrors()) {
                            return $this->render($app, $form, $headers, $this->productTwig);
                        }

                        $this->em->persist($Product);

                    }

                    $this->em->flush();
                    $this->em->getConnection()->commit();

                    log_info('商品CSV登録完了');

                    $app->addSuccess('admin.product.csv_import.save.complete', 'admin');
                }

            }
        }

        return $this->render($app, $form, $headers, $this->productTwig);
    }

    /**
     * カテゴリ登録CSVアップロード
     */
    public function csvCategory(Application $app, Request $request)
    {
        $form = $app['form.factory']->createBuilder('admin_csv_import')->getForm();

        $headers = $this->getCategoryCsvHeader();

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $formFile = $form['import_file']->getData();

                if (!empty($formFile)) {
                    log_info('カテゴリCSV登録開始');

                    $data = $this->getImportData($app, $formFile);
                    if ($data === false) {
                        $this->addErrors('CSVのフォーマットが一致しません。');

                        return $this->render($app, $form, $headers, $this->categoryTwig);
                    }

                    /**
                     * Checking the header for the data column flexible.
                     */
                    $requireHeader = array('カテゴリ名');
                    $columnHeaders = $data->getColumnHeaders();
                    if (count(array_diff($requireHeader, $columnHeaders)) > 0) {
                        $this->addErrors('CSVのフォーマットが一致しません。');

                        return $this->render($app, $form, $headers, $this->categoryTwig);
                    }

                    $size = count($data);
                    if ($size < 1) {
                        $this->addErrors('CSVデータが存在しません。');

                        return $this->render($app, $form, $headers, $this->categoryTwig);
                    }

                    $this->em = $app['orm.em'];
                    $this->em->getConfiguration()->setSQLLogger(null);

                    $this->em->getConnection()->beginTransaction();

                    // CSVファイルの登録処理
                    foreach ($data as $row) {
                        /** @var $Category Category */
                        $Category = new Category();
                        if (isset($row['カテゴリID']) && strlen($row['カテゴリID']) > 0) {
                            if (!preg_match('/^\d+$/', $row['カテゴリID'])) {
                                $this->addErrors(($data->key() + 1) . '行目のカテゴリIDが存在しません。');

                                return $this->render($app, $form, $headers, $this->categoryTwig);
                            }
                            $Category = $app['eccube.repository.category']->find($row['カテゴリID']);
                            if (!$Category) {
                                $this->addErrors(($data->key() + 1) . '行目のカテゴリIDが存在しません。');

                                return $this->render($app, $form, $headers, $this->categoryTwig);
                            }
                            if ($row['カテゴリID'] == $row['親カテゴリID']) {
                                $this->addErrors(($data->key() + 1) . '行目のカテゴリIDと親カテゴリIDが同じです。');

                                return $this->render($app, $form, $headers, $this->categoryTwig);
                            }
                            if ($Category->getShopId() != admin_shop_id()) {
                                $this->addErrors(($data->key() + 1) . '行目のカテゴリIDが存在しません。');
                                return $this->render($app, $form, $headers, $this->categoryTwig);
                            }
                        }

                        if (!isset($row['カテゴリ名']) || Str::isBlank($row['カテゴリ名'])) {
                            $this->addErrors(($data->key() + 1) . '行目のカテゴリ名が設定されていません。');

                            return $this->render($app, $form, $headers, $this->categoryTwig);
                        } else {
                            $Category->setName(Str::trimAll($row['カテゴリ名']));
                        }
                        $Category->setShop(admin_current_shop());
                        $ParentCategory = null;
                        if (isset($row['親カテゴリID']) && Str::isNotBlank($row['親カテゴリID'])) {
                            if (!preg_match('/^\d+$/', $row['親カテゴリID'])) {
                                $this->addErrors(($data->key() + 1) . '行目の親カテゴリIDが存在しません。');

                                return $this->render($app, $form, $headers, $this->categoryTwig);
                            }

                            /** @var $ParentCategory Category */
                            $ParentCategory = $app['eccube.repository.category']->find($row['親カテゴリID']);
                            if (!$ParentCategory) {
                                $this->addErrors(($data->key() + 1) . '行目の親カテゴリIDが存在しません。');

                                return $this->render($app, $form, $headers, $this->categoryTwig);
                            }
                        }
                        $Category->setParent($ParentCategory);

                        // Level
                        if (isset($row['階層']) && Str::isNotBlank($row['階層'])) {
                            if ($ParentCategory == null && $row['階層'] != 1) {
                                $this->addErrors(($data->key() + 1) . '行目の親カテゴリIDが存在しません。');

                                return $this->render($app, $form, $headers, $this->categoryTwig);
                            }
                            $level = Str::trimAll($row['階層']);
                        } else {
                            $level = 1;
                            if ($ParentCategory) {
                                $level = $ParentCategory->getLevel() + 1;
                            }
                        }
                        $Category->setLevel($level);

                        if ($app['config']['category_nest_level'] < $Category->getLevel()) {
                            $this->addErrors(($data->key() + 1) . '行目のカテゴリが最大レベルを超えているため設定できません。');

                            return $this->render($app, $form, $headers, $this->categoryTwig);
                        }

                        // カテゴリ削除フラグ対応
                        if (isset($row['カテゴリ削除フラグ']) && Str::isNotBlank($row['カテゴリ削除フラグ'])) {
                            $Category->setDelFlg($row['カテゴリ削除フラグ']);
                            $status = true;
                            switch ($row['カテゴリ削除フラグ']) {
                                case (string)Constant::DISABLED:
                                    break;

                                case (string)Constant::ENABLED:
                                    $status = $app['eccube.repository.category']->delete($Category);
                                    break;

                                default:
                                    $this->addErrors(($data->key() + 1) . '行目のカテゴリ削除フラグが設定されていません。');

                                    return $this->render($app, $form, $headers, $this->categoryTwig);
                                    break;
                            }

                            if (!$status) {
                                $this->addErrors(($data->key() + 1) . '行目のカテゴリが、子カテゴリまたは商品が紐付いているため削除できません。');

                                return $this->render($app, $form, $headers, $this->categoryTwig);
                            }
                        } else {
                            $Category->setDelFlg(Constant::DISABLED);
                        }

                        if ($this->hasErrors()) {
                            return $this->render($app, $form, $headers, $this->categoryTwig);
                        }

                        $app['eccube.repository.category']->save($Category);
                    }

                    $this->em->getConnection()->commit();

                    log_info('カテゴリCSV登録完了');

                    $app->addSuccess('admin.category.csv_import.save.complete', 'admin');
                }
            }
        }

        return $this->render($app, $form, $headers, $this->categoryTwig);
    }


    /**
     * アップロード用CSV雛形ファイルダウンロード
     */
    public function csvTemplate(Application $app, Request $request, $type)
    {
        set_time_limit(0);

        $response = new StreamedResponse();

        if ($type == 'product') {
            $headers = $this->getProductCsvHeader();
            $filename = 'product.csv';
        } else if ($type == 'category') {
            $headers = $this->getCategoryCsvHeader();
            $filename = 'category.csv';
        } else {
            throw new NotFoundHttpException();
        }

        $response->setCallback(function () use ($app, $request, $headers) {

            // ヘッダ行の出力
            $row = array();
            foreach ($headers as $key => $value) {
                $row[] = mb_convert_encoding($key, $app['config']['csv_export_encoding'], 'UTF-8');
            }

            $fp = fopen('php://output', 'w');
            fputcsv($fp, $row, $app['config']['csv_export_separator']);
            fclose($fp);

        });

        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $filename);
        $response->send();

        return $response;
    }


    /**
     * 登録、更新時のエラー画面表示
     *
     */
    protected function render($app, $form, $headers, $twig)
    {

        if ($this->hasErrors()) {
            if ($this->em) {
                $this->em->getConnection()->rollback();
            }
        }

        if (!empty($this->fileName)) {
            try {
                $fs = new Filesystem();
                $fs->remove($app['config']['csv_temp_realdir'] . '/' . $this->fileName);
            } catch (\Exception $e) {
                // エラーが発生しても無視する
            }
        }

        return $app->render($twig, array(
            'form' => $form->createView(),
            'headers' => $headers,
            'errors' => $this->errors,
        ));
    }


    /**
     * アップロードされたCSVファイルの行ごとの処理
     *
     * @param $formFile
     * @return CsvImportService
     */
    protected function getImportData($app, $formFile)
    {
        // アップロードされたCSVファイルを一時ディレクトリに保存
        $this->fileName = 'upload_' . Str::random() . '.' . $formFile->getClientOriginalExtension();
        $formFile->move($app['config']['csv_temp_realdir'], $this->fileName);

        $file = file_get_contents($app['config']['csv_temp_realdir'] . '/' . $this->fileName);

        if ('\\' === DIRECTORY_SEPARATOR && PHP_VERSION_ID >= 70000) {
            // Windows 環境の PHP7 の場合はファイルエンコーディングを CP932 に合わせる
            // see https://github.com/EC-CUBE/ec-cube/issues/1780
            setlocale(LC_ALL, ''); // 既定のロケールに設定
            if (mb_detect_encoding($file) === 'UTF-8') { // UTF-8 を検出したら SJIS-win に変換
                $file = mb_convert_encoding($file, 'SJIS-win', 'UTF-8');
            }
        } else {
            // アップロードされたファイルがUTF-8以外は文字コード変換を行う
            $encode = Str::characterEncoding($file);
            if (!empty($encode) && $encode != 'UTF-8') {
                $file = mb_convert_encoding($file, 'UTF-8', $encode);
            }
        }
        $file = Str::convertLineFeed($file);

        $tmp = tmpfile();
        fwrite($tmp, $file);
        rewind($tmp);
        $meta = stream_get_meta_data($tmp);
        $file = new \SplFileObject($meta['uri']);

        set_time_limit(0);

        // アップロードされたCSVファイルを行ごとに取得
        $data = new CsvImportService($file, $app['config']['csv_import_delimiter'], $app['config']['csv_import_enclosure']);

        $ret = $data->setHeaderRowNumber(0);

        return ($ret !== false) ? $data : false;
    }


    /**
     * 商品画像の削除、登録
     */
    protected function createProductImage($row, Product $Product, $data)
    {
        if (isset($row['商品画像']) && Str::isNotBlank($row['商品画像'])) {

            // 画像の削除
            $ProductImages = $Product->getProductImage();
            foreach ($ProductImages as $ProductImage) {
                $Product->removeProductImage($ProductImage);
                $this->em->remove($ProductImage);
            }

            // 画像の登録
            $images = explode(',', $row['商品画像']);
            $rank = 1;

            $pattern = "/\\$|^.*.\.\\\.*|\/$|^.*.\.\/\.*/";
            foreach ($images as $image) {

                $fileName = Str::trimAll($image);

                // 商品画像名のフォーマットチェック
                if (strlen($fileName) > 0 && preg_match($pattern, $fileName)) {
                    $this->addErrors(($data->key() + 1) . '行目の商品画像には末尾に"/"や"../"を使用できません。');
                } else {
                    // 空文字は登録対象外
                    if (!empty($fileName)) {
                        $ProductImage = new ProductImage();
                        $ProductImage->setFileName($fileName);
                        $ProductImage->setProduct($Product);
                        $ProductImage->setRank($rank);

                        $Product->addProductImage($ProductImage);
                        $rank++;
                        $this->em->persist($ProductImage);
                    }
                }
            }
        }
    }


    /**
     * 商品カテゴリの削除、登録
     */
    protected function createProductCategory($row, Product $Product, $app, $data)
    {
        // カテゴリの削除
        $ProductCategories = $Product->getProductCategories();
        foreach ($ProductCategories as $ProductCategory) {
            $Product->removeProductCategory($ProductCategory);
            $this->em->remove($ProductCategory);
            $this->em->flush($ProductCategory);
        }

        if (isset($row['商品カテゴリ(ID)']) && Str::isNotBlank($row['商品カテゴリ(ID)'])) {
            // カテゴリの登録
            $categories = explode(',', $row['商品カテゴリ(ID)']);
            $rank = 1;
            $categoriesIdList = array();
            foreach ($categories as $category) {

                if (preg_match('/^\d+$/', $category)) {
                    $Category = $app['eccube.repository.category']->find($category);
                    if (!$Category) {
                        $this->addErrors(($data->key() + 1) . '行目の商品カテゴリ(ID)「' . $category . '」が存在しません。');
                    } else {
                        foreach ($Category->getPath() as $ParentCategory) {
                            if (!isset($categoriesIdList[$ParentCategory->getId()])) {
                                $ProductCategory = $this->makeProductCategory($Product, $ParentCategory, $rank);
                                $app['orm.em']->persist($ProductCategory);
                                $rank++;
                                $Product->addProductCategory($ProductCategory);
                                $categoriesIdList[$ParentCategory->getId()] = true;
                            }
                        }
                        if (!isset($categoriesIdList[$Category->getId()])) {
                            $ProductCategory = $this->makeProductCategory($Product, $Category, $rank);
                            $rank++;
                            $this->em->persist($ProductCategory);
                            $Product->addProductCategory($ProductCategory);
                            $categoriesIdList[$Category->getId()] = true;
                        }
                    }
                } else {
                    $this->addErrors(($data->key() + 1) . '行目の商品カテゴリ(ID)「' . $category . '」が存在しません。');
                }
            }
        }

    }


    /**
     * タグの登録
     *
     * @param array $row
     * @param Product $Product
     * @param Application $app
     * @param CsvImportService $data
     */
    protected function createProductTag($row, Product $Product, $app, $data)
    {
        // タグの削除
        $ProductTags = $Product->getProductTag();
        foreach ($ProductTags as $ProductTags) {
            $Product->removeProductTag($ProductTags);
            $this->em->remove($ProductTags);
        }

        if (isset($row['メーカー(ID)']) && Str::isNotBlank($row['メーカー(ID)'])) {
            $maker_id = $row['メーカー(ID)'];
            $Maker = $app['eccube.repository.master.maker']->find($maker_id);
            if ($Maker) {
                $Product->setMaker($Maker);
            }
        }
        if (isset($row['タグ(ID)']) && Str::isNotBlank($row['タグ(ID)'])) {
            // タグの登録
            $tags = explode(',', $row['タグ(ID)']);
            foreach ($tags as $tag_id) {
                $Tag = null;
                if (preg_match('/^\d+$/', $tag_id)) {
                    $Tag = $app['eccube.repository.master.tag']->find($tag_id);
                    if ($Tag) {
                        $ProductTags = new ProductTag();
                        $ProductTags
                            ->setProduct($Product)
                            ->setTag($Tag);

                        $Product->addProductTag($ProductTags);

                        $this->em->persist($ProductTags);
                    }
                }
                if (!$Tag) {
                    $this->addErrors(($data->key() + 1) . '行目のタグ(ID)「' . $tag_id . '」が存在しません。');
                }
            }
        }
    }


    /**
     * 商品規格分類1、商品規格分類2がnullとなる商品規格情報を作成
     */
    protected function createProductClass($row, Product $Product, $app, $data, $ClassCategory1 = null, $ClassCategory2 = null)
    {
        // 規格分類1、規格分類2がnullとなる商品を作成

        $ProductClass = new ProductClass();
        $ProductClass->setProduct($Product);


        if (isset($row['商品種別(ID)']) && Str::isNotBlank($row['商品種別(ID)'])) {
            if (preg_match('/^\d+$/', $row['商品種別(ID)'])) {
                $ProductType = $app['eccube.repository.master.product_type']->find($row['商品種別(ID)']);
                if (!$ProductType) {
                    $this->addErrors(($data->key() + 1) . '行目の商品種別(ID)が存在しません。');
                } else {
                    $ProductClass->setProductType($ProductType);
                }
            } else {
                $this->addErrors(($data->key() + 1) . '行目の商品種別(ID)が存在しません。');
            }
        } else {
            $this->addErrors(($data->key() + 1) . '行目の商品種別(ID)が設定されていません。');
        }

        $ProductClass->setClassCategory1($ClassCategory1);
        $ProductClass->setClassCategory2($ClassCategory2);

        if (isset($row['発送日目安(ID)']) && Str::isNotBlank($row['発送日目安(ID)'])) {
            if (preg_match('/^\d+$/', $row['発送日目安(ID)'])) {
                $DeliveryDate = $app['eccube.repository.delivery_date']->find($row['発送日目安(ID)']);
                if (!$DeliveryDate) {
                    $this->addErrors(($data->key() + 1) . '行目の発送日目安(ID)が存在しません。');
                } else {
                    $ProductClass->setDeliveryDate($DeliveryDate);
                }
            } else {
                $this->addErrors(($data->key() + 1) . '行目の発送日目安(ID)が存在しません。');
            }
        }

        if (isset($row['商品コード']) && Str::isNotBlank($row['商品コード'])) {
            $ProductClass->setCode(Str::trimAll($row['商品コード']));
        } else {
            $ProductClass->setCode(null);
        }

        if (Str::isBlank($row['在庫数無制限フラグ'])) {
            $this->addErrors(($data->key() + 1) . '行目の在庫数無制限フラグが設定されていません。');
        } else {
            if ($row['在庫数無制限フラグ'] == (string)Constant::DISABLED) {
                $ProductClass->setStockUnlimited(Constant::DISABLED);
                // 在庫数が設定されていなければエラー
                if (isset($row['在庫数']) && Str::isNotBlank($row['在庫数'])) {
                    $stock = str_replace(',', '', $row['在庫数']);
                    if (preg_match('/^\d+$/', $stock) && $stock >= 0) {
                        $ProductClass->setStock($stock);
                    } else {
                        $this->addErrors(($data->key() + 1) . '行目の在庫数は0以上の数値を設定してください。');
                    }
                } else {
                    $this->addErrors(($data->key() + 1) . '行目の在庫数が設定されていません。');
                }

            } else if ($row['在庫数無制限フラグ'] == (string)Constant::ENABLED) {
                $ProductClass->setStockUnlimited(Constant::ENABLED);
                $ProductClass->setStock(null);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の在庫数無制限フラグが設定されていません。');
            }
        }

        if (isset($row['販売制限数']) && Str::isNotBlank($row['販売制限数'])) {
            $saleLimit = str_replace(',', '', $row['販売制限数']);
            if (preg_match('/^\d+$/', $saleLimit) && $saleLimit >= 0) {
                $ProductClass->setSaleLimit($saleLimit);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の販売制限数は0以上の数値を設定してください。');
            }
        }

        if (isset($row['通常価格']) && Str::isNotBlank($row['通常価格'])) {
            $price01 = str_replace(',', '', $row['通常価格']);
            if (preg_match('/^\d+$/', $price01) && $price01 >= 0) {
                $ProductClass->setPrice01($price01);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の通常価格は0以上の数値を設定してください。');
            }
        }

        if (isset($row['販売価格']) && Str::isNotBlank($row['販売価格'])) {
            $price02 = str_replace(',', '', $row['販売価格']);
            if (preg_match('/^\d+$/', $price02) && $price02 >= 0) {
                $ProductClass->setPrice02($price02);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の販売価格は0以上の数値を設定してください。');
            }
        } else {
            $this->addErrors(($data->key() + 1) . '行目の販売価格が設定されていません。');
        }

        if (isset($row['サーパスショップ（契約者・入居者）販売価格']) && Str::isNotBlank($row['サーパスショップ（契約者・入居者）販売価格'])) {
            $price_shop1 = str_replace(',', '', $row['サーパスショップ（契約者・入居者）販売価格']);
            if (preg_match('/^\d+$/', $price_shop1) && $price_shop1 >= 0) {
                $ProductClass->setPriceShop1($price_shop1);
                $ProductClass->setPriceShop2($price_shop1);

            } else {
                $this->addErrors(($data->key() + 1) . '行目のサーパスショップ（契約者・入居者）販売価格は0以上の数値を設定してください。');
            }
        }
        if (isset($row['くらしスクエアショップ販売価格']) && Str::isNotBlank($row['くらしスクエアショップ販売価格'])) {
            $price_shop3 = str_replace(',', '', $row['くらしスクエアショップ販売価格']);
            if (preg_match('/^\d+$/', $price_shop3) && $price_shop3 >= 0) {
                $ProductClass->setPriceShop3($price_shop3);
            } else {
                $this->addErrors(($data->key() + 1) . '行目のくらしスクエアショップ販売価格は0以上の数値を設定してください。');
            }
        }

        if (isset($row['社員価格']) && Str::isNotBlank($row['社員価格'])) {
            $staff_price = str_replace(',', '', $row['社員価格']);
            if (preg_match('/^\d+$/', $staff_price) && $staff_price >= 0) {
                $ProductClass->setStaffPrice($staff_price);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の社員価格は0以上の数値を設定してください。');
            }
        }


        if (isset($row['送料']) && Str::isNotBlank($row['送料'])) {
            $delivery_fee = str_replace(',', '', $row['送料']);
            if (preg_match('/^\d+$/', $delivery_fee) && $delivery_fee >= 0) {
                $ProductClass->setDeliveryFee($delivery_fee);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の送料は0以上の数値を設定してください。');
            }
        }

        if (isset($row['商品規格削除フラグ']) && Str::isNotBlank($row['商品規格削除フラグ'])) {
            if ($row['商品規格削除フラグ'] == (string)Constant::DISABLED || $row['商品規格削除フラグ'] == (string)Constant::ENABLED) {
                $ProductClass->setDelFlg($row['商品規格削除フラグ']);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の商品規格削除フラグが設定されていません。');
            }
        } else {
            $ProductClass->setDelFlg(Constant::DISABLED);
        }

        $Product->addProductClass($ProductClass);
        $ProductStock = new ProductStock();
        $ProductClass->setProductStock($ProductStock);
        $ProductStock->setProductClass($ProductClass);

        if (!$ProductClass->getStockUnlimited()) {
            $ProductStock->setStock($ProductClass->getStock());
        } else {
            // 在庫無制限時はnullを設定
            $ProductStock->setStock(null);
        }

        $this->em->persist($ProductClass);
        $this->em->persist($ProductStock);

        return $ProductClass;

    }


    /**
     * 商品規格情報を更新
     */
    protected function updateProductClass($row, Product $Product, ProductClass $ProductClass, $app, $data)
    {

        $ProductClass->setProduct($Product);

        if ($row['商品種別(ID)'] == '') {
            $this->addErrors(($data->key() + 1) . '行目の商品種別(ID)が設定されていません。');
        } else {
            if (preg_match('/^\d+$/', $row['商品種別(ID)'])) {
                $ProductType = $app['eccube.repository.master.product_type']->find($row['商品種別(ID)']);
                if (!$ProductType) {
                    $this->addErrors(($data->key() + 1) . '行目の商品種別(ID)が存在しません。');
                } else {
                    $ProductClass->setProductType($ProductType);
                }
            } else {
                $this->addErrors(($data->key() + 1) . '行目の商品種別(ID)が存在しません。');
            }
        }

        // 規格分類1、2をそれぞれセットし作成
        if ($row['規格分類1(ID)'] != '') {
            if (preg_match('/^\d+$/', $row['規格分類1(ID)'])) {
                $ClassCategory = $app['eccube.repository.class_category']->find($row['規格分類1(ID)']);
                if (!$ClassCategory) {
                    $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)が存在しません。');
                } else {
                    $ProductClass->setClassCategory1($ClassCategory);
                }
            } else {
                $this->addErrors(($data->key() + 1) . '行目の規格分類1(ID)が存在しません。');
            }
        }

        if ($row['規格分類2(ID)'] != '') {
            if (preg_match('/^\d+$/', $row['規格分類2(ID)'])) {
                $ClassCategory = $app['eccube.repository.class_category']->find($row['規格分類2(ID)']);
                if (!$ClassCategory) {
                    $this->addErrors(($data->key() + 1) . '行目の規格分類2(ID)が存在しません。');
                } else {
                    $ProductClass->setClassCategory2($ClassCategory);
                }
            } else {
                $this->addErrors(($data->key() + 1) . '行目の規格分類2(ID)が存在しません。');
            }
        }

        if ($row['発送日目安(ID)'] != '') {
            if (preg_match('/^\d+$/', $row['発送日目安(ID)'])) {
                $DeliveryDate = $app['eccube.repository.delivery_date']->find($row['発送日目安(ID)']);
                if (!$DeliveryDate) {
                    $this->addErrors(($data->key() + 1) . '行目の発送日目安(ID)が存在しません。');
                } else {
                    $ProductClass->setDeliveryDate($DeliveryDate);
                }
            } else {
                $this->addErrors(($data->key() + 1) . '行目の発送日目安(ID)が存在しません。');
            }
        }

        if (Str::isNotBlank($row['商品コード'])) {
            $ProductClass->setCode(Str::trimAll($row['商品コード']));
        } else {
            $ProductClass->setCode(null);
        }

        if ($row['在庫数無制限フラグ'] == '') {
            $this->addErrors(($data->key() + 1) . '行目の在庫数無制限フラグが設定されていません。');
        } else {
            if ($row['在庫数無制限フラグ'] == (string)Constant::DISABLED) {
                $ProductClass->setStockUnlimited(Constant::DISABLED);
                // 在庫数が設定されていなければエラー
                if ($row['在庫数'] == '') {
                    $this->addErrors(($data->key() + 1) . '行目の在庫数が設定されていません。');
                } else {
                    $stock = str_replace(',', '', $row['在庫数']);
                    if (preg_match('/^\d+$/', $stock) && $stock >= 0) {
                        $ProductClass->setStock($row['在庫数']);
                    } else {
                        $this->addErrors(($data->key() + 1) . '行目の在庫数は0以上の数値を設定してください。');
                    }
                }

            } else if ($row['在庫数無制限フラグ'] == (string)Constant::ENABLED) {
                $ProductClass->setStockUnlimited(Constant::ENABLED);
                $ProductClass->setStock(null);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の在庫数無制限フラグが設定されていません。');
            }
        }

        if ($row['販売制限数'] != '') {
            $saleLimit = str_replace(',', '', $row['販売制限数']);
            if (preg_match('/^\d+$/', $saleLimit) && $saleLimit >= 0) {
                $ProductClass->setSaleLimit($saleLimit);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の販売制限数は0以上の数値を設定してください。');
            }
        }

        if ($row['通常価格'] != '') {
            $price01 = str_replace(',', '', $row['通常価格']);
            if (preg_match('/^\d+$/', $price01) && $price01 >= 0) {
                $ProductClass->setPrice01($price01);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の通常価格は0以上の数値を設定してください。');
            }
        }

        if ($row['販売価格'] == '') {
            $this->addErrors(($data->key() + 1) . '行目の販売価格が設定されていません。');
        } else {
            $price02 = str_replace(',', '', $row['販売価格']);
            if (preg_match('/^\d+$/', $price02) && $price02 >= 0) {
                $ProductClass->setPrice02($price02);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の販売価格は0以上の数値を設定してください。');
            }
        }

        if (isset($row['サーパスショップ（契約者・入居者）販売価格']) && Str::isNotBlank($row['サーパスショップ（契約者・入居者）販売価格'])) {
            $price_shop1 = str_replace(',', '', $row['サーパスショップ（契約者・入居者）販売価格']);
            if (preg_match('/^\d+$/', $price_shop1) && $price_shop1 >= 0) {
                $ProductClass->setPriceShop1($price_shop1);
                $ProductClass->setPriceShop2($price_shop1);
            } else {
                $this->addErrors(($data->key() + 1) . '行目のサーパスショップ（契約者・入居者）販売価格は0以上の数値を設定してください。');
            }
        }
        if (isset($row['くらしスクエアショップ販売価格']) && Str::isNotBlank($row['くらしスクエアショップ販売価格'])) {
            $price_shop3 = str_replace(',', '', $row['くらしスクエアショップ販売価格']);
            if (preg_match('/^\d+$/', $price_shop3) && $price_shop3 >= 0) {
                $ProductClass->setPriceShop3($price_shop3);
            } else {
                $this->addErrors(($data->key() + 1) . '行目のくらしスクエアショップ販売価格は0以上の数値を設定してください。');
            }
        }

        if (isset($row['社員価格']) && Str::isNotBlank($row['社員価格'])) {
            $staff_price = str_replace(',', '', $row['社員価格']);
            if (preg_match('/^\d+$/', $staff_price) && $staff_price >= 0) {
                $ProductClass->setStaffPrice($staff_price);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の社員価格は0以上の数値を設定してください。');
            }
        }


        if ($row['商品規格削除フラグ'] == '') {
            $ProductClass->setDelFlg(Constant::DISABLED);
        } else {
            if ($row['商品規格削除フラグ'] == (string)Constant::DISABLED || $row['商品規格削除フラグ'] == (string)Constant::ENABLED) {
                $ProductClass->setDelFlg($row['商品規格削除フラグ']);
            } else {
                $this->addErrors(($data->key() + 1) . '行目の商品規格削除フラグが設定されていません。');
            }
        }

        $ProductStock = $ProductClass->getProductStock();

        if (!$ProductClass->getStockUnlimited()) {
            $ProductStock->setStock($ProductClass->getStock());
        } else {
            // 在庫無制限時はnullを設定
            $ProductStock->setStock(null);
        }

        return $ProductClass;
    }

    /**
     * 登録、更新時のエラー画面表示
     *
     */
    protected function addErrors($message)
    {
        $e = new CsvImportException($message);
        $this->errors[] = $e;
    }

    /**
     * @return array
     */
    protected function getErrors()
    {
        return $this->errors;
    }

    /**
     *
     * @return boolean
     */
    protected function hasErrors()
    {
        return count($this->getErrors()) > 0;
    }

    /**
     * 商品登録CSVヘッダー定義
     */
    private function getProductCsvHeader()
    {
        return array(
            '商品ID' => 'id',
            '公開ステータス(ID)' => 'status',
            'サーパスショップ（契約者）公開ステータス(ID)' => 'status_shop1',
            'サーパスショップ（入居者）公開ステータス(ID)' => 'status_shop2',
            'くらしスクエアショップ公開ステータス(ID)' => 'status_shop3',
            '商品名' => 'name',
            '提携ショップ' => 'agency',
            'ファインパスポート対象' => 'fine_passport_flg',
            'エスコート対象' => 'escort_flg',
            'メーカー(ID)' => 'maker',
            'ショップ用メモ欄' => 'note',
            '商品説明(一覧)' => 'description_list',
            '商品説明(詳細)' => 'description_detail',
            '検索ワード' => 'search_word',
            'フリーエリア' => 'free_area',
            '商品削除フラグ' => 'product_del_flg',
            '商品画像' => 'product_image',
            '商品カテゴリ(ID)' => 'product_category',
            'タグ(ID)' => 'product_tag',
            '商品種別(ID)' => 'product_type',
            '規格分類1(ID)' => 'class_category1',
            '規格分類2(ID)' => 'class_category2',
            '発送日目安(ID)' => 'deliveryFee',
            '商品コード' => 'product_code',
            '在庫数' => 'stock',
            '在庫数無制限フラグ' => 'stock_unlimited',
            '販売制限数' => 'sale_limit',
            '通常価格' => 'price01',
            '販売価格' => 'price02',
            'サーパスショップ（契約者・入居者）販売価格' => 'price_shop1',
            'くらしスクエアショップ販売価格' => 'price_shop3',
            '社員価格' => 'staff_price',
            '送料' => 'delivery_fee',
            '販売不可事業所(ID)' => 'ng_office',
            '商品規格削除フラグ' => 'product_class_del_flg',
        );
    }


    /**
     * カテゴリCSVヘッダー定義
     */
    private function getCategoryCsvHeader()
    {
        return array(
            'カテゴリID' => 'id',
            'カテゴリ名' => 'category_name',
            '親カテゴリID' => 'parent_category_id',
            'カテゴリ削除フラグ' => 'category_del_flg',
        );
    }

    /**
     * ProductCategory作成
     *
     * @param \Eccube\Entity\Product $Product
     * @param \Eccube\Entity\Category $Category
     * @return ProductCategory
     */
    private function makeProductCategory($Product, $Category, $rank)
    {
        $ProductCategory = new ProductCategory();
        $ProductCategory->setProduct($Product);
        $ProductCategory->setProductId($Product->getId());
        $ProductCategory->setCategory($Category);
        $ProductCategory->setCategoryId($Category->getId());
        $ProductCategory->setRank($rank);

        return $ProductCategory;
    }


    /**
     * カテゴリCSVヘッダー定義
     */
    private function getDeliveryCsvHeader()
    {
        return array(
            "配送業者ID" => "id",
            "配送業者名" => "name",
            "名称" => "service_name",
//            "伝票No.URL" => "confirm_url",
            "商品種別ID" => "product_type_id",
            "ショップ備考欄" => "description",
            "支払方法設定ID" => "payment_id",
            "お届け時間設定" => "delivery_time",
            "北海道" => "fee_1",
            "青森県" => "fee_2",
            "岩手県" => "fee_3",
            "宮城県" => "fee_4",
            "秋田県" => "fee_5",
            "山形県" => "fee_6",
            "福島県" => "fee_7",
            "茨城県" => "fee_8",
            "栃木県" => "fee_9",
            "群馬県" => "fee_10",
            "埼玉県" => "fee_11",
            "千葉県" => "fee_12",
            "東京都" => "fee_13",
            "神奈川県" => "fee_14",
            "新潟県" => "fee_15",
            "富山県" => "fee_16",
            "石川県" => "fee_17",
            "福井県" => "fee_18",
            "山梨県" => "fee_19",
            "長野県" => "fee_20",
            "岐阜県" => "fee_21",
            "静岡県" => "fee_22",
            "愛知県" => "fee_23",
            "三重県" => "fee_24",
            "滋賀県" => "fee_25",
            "京都府" => "fee_26",
            "大阪府" => "fee_27",
            "兵庫県" => "fee_28",
            "奈良県" => "fee_29",
            "和歌山県" => "fee_30",
            "鳥取県" => "fee_31",
            "島根県" => "fee_32",
            "岡山県" => "fee_33",
            "広島県" => "fee_34",
            "山口県" => "fee_35",
            "徳島県" => "fee_36",
            "香川県" => "fee_37",
            "愛媛県" => "fee_38",
            "高知県" => "fee_39",
            "福岡県" => "fee_40",
            "佐賀県" => "fee_41",
            "長崎県" => "fee_42",
            "熊本県" => "fee_43",
            "大分県" => "fee_44",
            "宮崎県" => "fee_45",
            "鹿児島県" => "fee_46",
            "沖縄県" => "fee_47",
//            "配送方法削除フラグ" => "del_flg"
        );
    }


    /**
     * 配送業者登録CSVアップロード
     * @param Application $app
     * @param Request $request
     * @return mixed
     */
    public function csvDelivery(Application $app, Request $request)
    {
        $form = $app['form.factory']->createBuilder('admin_csv_import')->getForm();

        $headers = $this->getDeliveryCsvHeader();

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $formFile = $form['import_file']->getData();

                if (!empty($formFile)) {
                    log_info('配送業者CSV登録開始');

                    $data = $this->getImportData($app, $formFile);
                    if ($data === false) {
                        $this->addErrors('CSVのフォーマットが一致しません。');
                        return $this->render($app, $form, $headers, $this->deliveryTwig);
                    }
                    /**
                     * Checking the header for the data column flexible.
                     */
                    $requireHeader = array('配送業者ID', '配送業者名', '名称', '商品種別ID', 'ショップ備考欄', '支払方法設定ID', 'お届け時間設定', '北海道', '青森県', '岩手県', '宮城県', '秋田県', '山形県', '福島県', '茨城県', '栃木県', '群馬県', '埼玉県', '千葉県', '東京都', '神奈川県', '新潟県', '富山県', '石川県', '福井県', '山梨県', '長野県', '岐阜県', '静岡県', '愛知県', '三重県', '滋賀県', '京都府', '大阪府', '兵庫県', '奈良県', '和歌山県', '鳥取県', '島根県', '岡山県', '広島県', '山口県', '徳島県', '香川県', '愛媛県', '高知県', /*'福岡県', '佐賀県', '長崎県', '熊本県', '大分県', '宮崎県', '鹿児島県', '沖縄県'*/);
                    $columnHeaders = $data->getColumnHeaders();

                    if (count(array_diff($requireHeader, $columnHeaders)) > 0) {
                        $this->addErrors('CSVのフォーマットが一致しません。');
                        return $this->render($app, $form, $headers, $this->deliveryTwig);
                    }


                    $size = count($data);
                    if ($size < 1) {
                        $this->addErrors('CSVデータが存在しません。');
                        return $this->render($app, $form, $headers, $this->deliveryTwig);
                    }


                    $this->em = $app['orm.em'];
                    $this->em->getConfiguration()->setSQLLogger(null);

                    $this->em->getConnection()->beginTransaction();
                    $Prefectures = $app['eccube.repository.master.pref']->findAll();
                    $payment_ids = array_map(function ($val) {
                        return $val['payment_id'];
                    }, $app['db']->fetchAll("SELECT payment_id from dtb_payment"));

                    // CSVファイルの登録処理

                    foreach ($data as $row) {
                        /** @var $Delivery Delivery */
                        $Delivery = new Delivery();
                        if (isset($row['配送方法削除フラグ']) && Str::isNotBlank($row['配送方法削除フラグ'])) {
                            if ($row['配送方法削除フラグ'] == (string)Constant::DISABLED || $row['配送方法削除フラグ'] == (string)Constant::ENABLED) {
                                $Delivery->setDelFlg($row['配送方法削除フラグ']);
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目の配送方法削除フラグが設定されていません。');
                                return $this->render($app, $form, $headers, $this->productTwig);
                            }
                        } else {
                            $Delivery->setDelFlg(Constant::DISABLED);
                        }


                        if (isset($row['配送業者ID']) && strlen($row['配送業者ID']) > 0) {
                            if (!preg_match('/^\d+$/', $row['配送業者ID'])) {
                                $this->addErrors(($data->key() + 1) . '行目の配送業者IDが存在しません。');
                                return $this->render($app, $form, $headers, $this->deliveryTwig);
                            }
                            $Delivery = $app['eccube.repository.delivery']->find($row['配送業者ID']);
                            if (!$Delivery) {
                                $this->addErrors(($data->key() + 1) . '行目の配送業者IDが存在しません。');
                                return $this->render($app, $form, $headers, $this->deliveryTwig);
                            }
                        }

                        if (!isset($row['配送業者名']) || Str::isBlank($row['配送業者名'])) {
                            $this->addErrors(($data->key() + 1) . '行目の配送業者名が設定されていません。');
                            return $this->render($app, $form, $headers, $this->deliveryTwig);
                        } else {
                            $Delivery->setName(Str::trimAll($row['配送業者名']));
                        }
                        // 名称
                        if (!isset($row['名称']) || Str::isBlank($row['名称'])) {
                            $this->addErrors(($data->key() + 1) . '行目の名称が設定されていません。');
                            return $this->render($app, $form, $headers, $this->deliveryTwig);
                        } else {
                            $Delivery->setServiceName(Str::trimAll($row['名称']));
                        }
                        // 伝票No.URL
//                        if (!isset($row['伝票No.URL']) || Str::isBlank($row['伝票No.URL'])) {
////                            $this->addErrors(($data->key() + 1) . '行目の伝票No.URLが設定されていません。');
////                            return $this->render($app, $form, $headers, $this->deliveryTwig);
//                        } else {
//                            $Delivery->setConfirmUrl(Str::trimAll($row['伝票No.URL']));
//                        }

                        // 商品種別
                        if (!isset($row['商品種別ID']) || Str::isBlank($row['商品種別ID'])) {
                            $this->addErrors(($data->key() + 1) . '行目の商品種別IDが設定されていません。');
                            return $this->render($app, $form, $headers, $this->deliveryTwig);
                        } else {
                            if (!preg_match('/^\d+$/', $row['商品種別ID'])) {
                                $this->addErrors(($data->key() + 1) . '行目の商品種別IDが存在しません。');
                                return $this->render($app, $form, $headers, $this->deliveryTwig);
                            }

                            $ProductType = $app['eccube.repository.master.product_type']->find($row['商品種別ID']);
                            if (!$ProductType) {
                                $this->addErrors(($data->key() + 1) . '行目の商品種別IDが存在しません。');
                                return $this->render($app, $form, $headers, $this->deliveryTwig);
                            }
                            $Delivery->setProductType($ProductType);
                        }
                        // ショップ備考欄
                        if (!isset($row['ショップ備考欄']) || Str::isBlank($row['ショップ備考欄'])) {
//                            $this->addErrors(($data->key() + 1) . '行目のショップ備考欄が設定されていません。');
//                            return $this->render($app, $form, $headers, $this->deliveryTwig);
                        } else {
                            $Delivery->setDescription(Str::trimAll($row['ショップ備考欄']));
                        }
                        $this->em->persist($Delivery);
                        $this->em->flush();


                        if (!isset($row['支払方法設定ID']) || Str::isBlank($row['支払方法設定ID'])) {
//                            $this->addErrors(($data->key() + 1) . '行目の支払方法設定IDが設定されていません。');
//                            return $this->render($app, $form, $headers, $this->deliveryTwig);
                        } else {
                            $app['db']->query("DELETE FROM dtb_payment_option WHERE delivery_id = " . $Delivery->getId());
                            foreach (explode(",", Str::trimAll($row['支払方法設定ID'])) as $payment_id) {
                                $payment_id = (int)$payment_id;
                                if (!in_array($payment_id, $payment_ids)) {
                                    $this->addErrors(($data->key() + 1) . '行目の支払方法(ID: ' . $payment_id . ')が存在しません。');
                                    return $this->render($app, $form, $headers, $this->deliveryTwig);
                                }
                                $app['db']->query("INSERT INTO dtb_payment_option(delivery_id,payment_id) VALUES('" . $Delivery->getId() . "','" . $payment_id . "') ");

                            }
                        }

                        /** @var DeliveryTime $DeliveryTime */
                        $app['db']->update("dtb_delivery_time", ['delivery_time' => ''], ['delivery_id' => $Delivery->getId()]);
                        if (!isset($row['お届け時間設定']) || Str::isBlank($row['お届け時間設定'])) {
//                            $this->addErrors(($data->key() + 1) . '行目のショップ備考欄が設定されていません。');
//                            return $this->render($app, $form, $headers, $this->deliveryTwig);
                        } else {
                            $DeliveryTimes = $app['db']->fetchAll("SELECT * from dtb_delivery_time WHERE delivery_id = " . $Delivery->getId());
                            $Delivery->getDeliveryTimes()->count();
                            foreach (explode(",", Str::trimAll($row['お届け時間設定'])) as $index => $Time) {
                                if (!isset($DeliveryTimes[$index])) {
                                    $DeliveryTime = new DeliveryTime();
                                    $DeliveryTime->setDelivery($Delivery);
                                    $DeliveryTime->setDeliveryTime($Time);
                                    $this->em->persist($DeliveryTime);
                                } else {
                                    $DeliveryTime = $DeliveryTimes[$index];
                                    $app['db']->update("dtb_delivery_time", ['delivery_time' => $Time], ['time_id' => $DeliveryTime['time_id']]);
                                }
                            }
                        }
                        $Fees = $app['db']->fetchAll('SELECT * FROM dtb_delivery_fee WHERE delivery_id = ' . $Delivery->getId());
                        /** @var Pref $Pref */
                        foreach ($Prefectures as $Pref) {
                            if (!isset($row[$Pref->getName()]) || Str::isBlank($Pref->getName()) || !preg_match('/^\d+$/', $row[$Pref->getName()])) {
                                $this->addErrors(($data->key() + 1) . '行目の' . $Pref->getName() . 'が設定されていません。');
                                return $this->render($app, $form, $headers, $this->deliveryTwig);
                            }
                            $update = false;
                            foreach ($Fees as $fee) {
                                if ($fee['pref'] == $Pref->getId()) {
                                    $update = true;
                                    $app['db']->update("dtb_delivery_fee", ['fee' => (int)$row[$Pref->getName()]], ['fee_id' => $fee['fee_id']]);
                                }
                            }
                            if (!$update) {
                                $app['db']->insert('dtb_delivery_fee', [
                                    'delivery_id' => $Delivery->getId(),
                                    'pref' => $Pref->getId(),
                                    'fee' => $row[$Pref->getName()]
                                ]);
                            }
                        }

                        if ($this->hasErrors()) {
                            return $this->render($app, $form, $headers, $this->deliveryTwig);
                        }
                        if (!$Delivery->getId()) {
                            $Delivery->setDelFlg(0);
                        }
                        $this->em->persist($Delivery);
                        $this->em->flush();
                    }

                    $this->em->getConnection()->commit();

                    log_info('配送業者CSV登録完了');

                    $app->addSuccess('配送業者CSV登録完了しました。', 'admin');
                    return $app->redirect($app->url('admin_setting_shop_delivery_csv_upload'));
                }
            }
        }

        return $this->render($app, $form, $headers, $this->deliveryTwig);
    }


    /**
     * カテゴリCSVヘッダー定義
     */
    private function getPointCsvHeader()
    {
        return array(
            'ポイント情報ID' => 'point_id',
            '会員ID' => 'customer_id',
            '利用日/獲得日' => 'create_date',
            '内容' => 'description',
            '注文番号' => 'order_id',
            '確定日' => 'commit_date',
            'ポイント増減' => 'point',
            '有効期限延長フラグ' => 'extend_flg'
        );
    }

    public function csvPoint(Application $app, Request $request)
    {
        $form = $app['form.factory']->createBuilder('admin_csv_import')->getForm();

        $headers = $this->getPointCsvHeader();

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $formFile = $form['import_file']->getData();

                if (!empty($formFile)) {
                    log_info('ポイントCSV登録開始');
                    $data = $this->getImportData($app, $formFile);

                    if ($data === false) {
                        $this->addErrors('CSVのフォーマットが一致しません。');
                        return $this->render($app, $form, $headers, $this->pointTwig);
                    }
                    /**
                     * Checking the header for the data column flexible.
                     */
                    $requireHeader = array('会員ID', '内容', 'ポイント増減');
                    $columnHeaders = $data->getColumnHeaders();

                    if (count(array_diff($requireHeader, $columnHeaders)) > 0) {
                        $this->addErrors('CSVのフォーマットが一致しません。');
                        return $this->render($app, $form, $headers, $this->pointTwig);
                    }


                    $size = count($data);
                    if ($size < 1) {
                        $this->addErrors('CSVデータが存在しません。');
                        return $this->render($app, $form, $headers, $this->pointTwig);
                    }


                    $this->em = $app['orm.em'];
                    $this->em->getConfiguration()->setSQLLogger(null);

                    $this->em->getConnection()->beginTransaction();

                    // CSVファイルの登録処理
                    foreach ($data as $row) {

                        /** @var $PointTransaction PointTransaction */
                        $PointTransaction = [];
                        $customer_id = null;
                        $point_transaction_id = null;
                        if (isset($row['会員ID']) && strlen($row['会員ID']) > 0) {
                            if (!preg_match('/^\d+$/', $row['会員ID'])) {
                                $this->addErrors(($data->key() + 1) . '行目の会員IDが存在しません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                            $customer_id = $row['会員ID'];
                        } else {
                            $this->addErrors(($data->key() + 1) . '行目の会員IDが存在しません。');
                            return $this->render($app, $form, $headers, $this->pointTwig);
                        }


                        if (isset($row['ポイント情報ID']) && strlen($row['ポイント情報ID']) > 0) {
                            if (!preg_match('/^\d+$/', $row['ポイント情報ID'])) {
                                $this->addErrors(($data->key() + 1) . '行目のポイント情報IDが存在しません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }

                            $PointTransactions = $app['db']->fetchAll("SELECT * FROM dtb_point_transaction WHERE point_transaction_id = ? AND customer_id = ?", [$row['ポイント情報ID'], $customer_id]);

                            if (!isset($PointTransactions[0])) {
                                $this->addErrors(($data->key() + 1) . '行目のポイント情報IDが存在しません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                            $point_transaction_id = $row['ポイント情報ID'];
                            $PointTransaction = $PointTransactions[0];
                        }

                        if (!isset($PointTransaction['customer_id'])) {
                            $PointTransaction['customer_id'] = $customer_id;
                        }

                        if (!isset($row['利用日/獲得日']) || Str::isBlank($row['利用日/獲得日'])) {
                            $this->addErrors(($data->key() + 1) . '行目の利用日/獲得日が設定されていません。');
                            return $this->render($app, $form, $headers, $this->pointTwig);

                        } else {
                            if (!strtotime($row['利用日/獲得日'])) {
                                $this->addErrors(($data->key() + 1) . '行目の利用日/獲得日が取り込みできません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                            $PointTransaction['create_date'] = (new \DateTime(Str::trimAll($row['利用日/獲得日'])))->format("Y-m-d H:i:s");
                            $PointTransaction['update_date'] = (new \DateTime())->format("Y-m-d H:i:s");
                        }


                        if (!isset($row['内容']) || Str::isBlank($row['内容'])) {
                            $this->addErrors(($data->key() + 1) . '行目の内容が設定されていません。');
                            return $this->render($app, $form, $headers, $this->pointTwig);
                        } else {
                            $PointTransaction['description'] = Str::trimAll($row['内容']);
                        }

                        if (!isset($row['有効期限延長フラグ']) || Str::isBlank($row['有効期限延長フラグ'])) {
                            $this->addErrors(($data->key() + 1) . '行目の有効期限延長フラグが設定されていません。');
                            return $this->render($app, $form, $headers, $this->pointTwig);
                        } else {
                            if ($row['有効期限延長フラグ'] == (string)Constant::DISABLED || $row['有効期限延長フラグ'] == (string)Constant::ENABLED) {
                                $PointTransaction['extend_flg'] = Str::trimAll($row['有効期限延長フラグ']);
                            } else {
                                $this->addErrors(($data->key() + 1) . '行目の有効期限延長フラグが設定されていません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                        }


                        if (!isset($row['注文番号']) || Str::isBlank($row['注文番号'])) {
                            if (!isset($PointTransaction['order_id'])) {
                                $PointTransaction['order_id'] = null;
                            }
                        } else {
                            $PointTransaction['order_id'] = Str::trimAll($row['注文番号']);
                            $OrderCount = $app['db']->fetchAll("SELECT count(*) rowCount FROM dtb_order WHERE order_id = ? AND customer_id = ?", [$row['注文番号'], $customer_id]);
                            if (!isset($OrderCount[0]['rowCount']) || $OrderCount[0]['rowCount'] == 0) {
                                $this->addErrors(($data->key() + 1) . '行目の注文番号が存在しません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                        }


                        if (!isset($row['確定日']) || Str::isBlank($row['確定日'])) {
                            if (!isset($PointTransaction['commit_date']) && !is_null($PointTransaction['commit_date'])) {
//                                $PointTransaction['commit_date'] = (new \DateTime())->format("Y-m-d H:i:s");
                                $PointTransaction['commit_date'] = null;
                            }
                        } else {
                            if (!strtotime($row['確定日'])) {
                                $this->addErrors(($data->key() + 1) . '行目の確定日が取り込みできません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                            $PointTransaction['commit_date'] = (new \DateTime(Str::trimAll($row['確定日'])))->format("Y-m-d H:i:s");
                        }
                        // 名称
                        if (!isset($row['ポイント増減']) || Str::isBlank($row['ポイント増減'])) {
                            $this->addErrors(($data->key() + 1) . '行目のポイント増減が設定されていません。');
                            return $this->render($app, $form, $headers, $this->pointTwig);
                        } else {
                            if (!preg_match('/^(\-|\+|)\d+$/', $row['ポイント増減'])) {
                                $this->addErrors(($data->key() + 1) . '行目のポイント増減が有効な値ではありません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                            $point = (int)(Str::trimAll($row['ポイント増減']));
                            if ($point_transaction_id) {
                                $PointTotalArray = $app['db']->fetchAll("SELECT sum(if(`type` = 2,-1*point,point)) total FROM dtb_point_transaction WHERE customer_id = ? AND status = 1 AND point_transaction_id <> ?", [$customer_id, $point_transaction_id]);
                            } else {
                                $PointTotalArray = $app['db']->fetchAll("SELECT sum(if(`type` = 2,-1*point,point)) total FROM dtb_point_transaction WHERE customer_id = ? AND status = 1", [$customer_id]);
                            }

                            if (!isset($PointTotalArray[0]['total']) && !is_null($PointTotalArray[0]['total'])) {
                                $this->addErrors(($data->key() + 1) . '行目のポイントが有効な値ではありません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                            $PointTotal = (int)$PointTotalArray[0]['total'];
                            if ($PointTotal + $point < 0) {
                                $this->addErrors(($data->key() + 1) . '行目のポイント合計（' . ($PointTotal + $point) . 'pt）が有効な値ではありません。');
                                return $this->render($app, $form, $headers, $this->pointTwig);
                            }
                            $PointTransaction['point'] = abs($point);
                            if ($point >= 0) {
                                $PointTransaction['type'] = 1;
                            } else {
                                $PointTransaction['type'] = 2;
                            }
                            if (!isset($PointTransaction['status'])) {
                                $PointTransaction['status'] = 1;
                            }
                            if (isset($PointTransaction['commit_date']) && strtotime($PointTransaction['commit_date'])) {
                                $PointTransaction['status'] = 1;
                            }
                        }

                        if (isset($PointTransaction['point_transaction_id']) && $PointTransaction['point_transaction_id']) {
                            $app['db']->update("dtb_point_transaction", $PointTransaction, ['point_transaction_id' => $PointTransaction['point_transaction_id']]);
                        } else {
                            $app['db']->insert('dtb_point_transaction', $PointTransaction);
                        }

                        if ($this->hasErrors()) {
                            return $this->render($app, $form, $headers, $this->pointTwig);
                        }
                    }

                    $this->em->getConnection()->commit();

                    log_info('ポイントCSV登録完了');

                    $app->addSuccess('ポイントCSV登録完了しました。', 'admin');
                    return $app->redirect($app->url('admin_customer_point_csv_import'));
                }
            }
        }

        return $this->render($app, $form, $headers, $this->pointTwig);

    }
}
