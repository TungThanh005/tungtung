<?php

namespace Eccube\Controller\Admin\Setting\Shop;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Entity\Banner;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

class BannerController extends AbstractController
{
    public function index(Application $app, Request $request)
    {
        $BannerPositions = $app['eccube.repository.master.banner_position']->findAll();
        $Banners = $app['eccube.repository.banner']->findBy([
            'Shop' => admin_shop_id()
        ], [
            'create_date' => 'DESC'
        ]);

        return $app->render('Setting/Shop/banner.twig', array(
            'BannerPositions' => $BannerPositions,
            'Banners' => $Banners
        ));

    }


    public function edit(Application $app, Request $request, $id = null)
    {
        if (is_null($id)) {
            $Banner = new Banner();
            $Banner->setShop(admin_current_shop());
            $Banner->setLinkMethod((bool)Constant::DISABLED);

        } else {
            /** @var Banner $Banner */
            $Banner = $app['eccube.repository.banner']->find($id);

            if ($Banner->getShopId() != admin_shop_id()) {
                return $app->redirect($app->url('admin_setting_banner'));
            }
            if (!$Banner) {
                throw new NotFoundHttpException();
            }
            $Banner->setLinkMethod((bool)$Banner->getLinkMethod());

        }

        /** @var Form $form */
        $form = $app['form.factory']
            ->createBuilder('admin_banner', $Banner)->getForm();
        // Set Campaign
        $form['Campaign']->setData($Banner->getCampaign());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Set Campaign
            $Banner->setCampaign($form->get('Campaign')->getData());
            $images = $request->files->get('admin_banner');
            $filename = null;
            if (isset($images['image_path'])) {
                $image = $images['image_path'];
                //ファイルフォーマット検証
                $mimeType = $image->getMimeType();
                if (0 !== strpos($mimeType, 'image')) {
                    throw new UnsupportedMediaTypeHttpException();
                }

                $extension = $image->guessExtension();
                $filename = date('mdHis') . uniqid('_') . '.' . $extension;

                if ($image->move($app['config']['image_save_realdir'] . '/banners/' . date('Ym'), $filename)) {
                    if ($Banner->getImagePath()) {
                        // 削除
                        $fs = new Filesystem();
                        $fs->remove($app['config']['image_save_realdir'] . '/' . $Banner->getImagePath());
                    }
                    $Banner->setImagePath('banners/' . date('Ym') . '/' . $filename);
                }

            }
            $app['orm.em']->persist($Banner);
            $app['orm.em']->flush();
            $app->addSuccess('バナー情報を保存しました', 'admin');
            return $app->redirect($app->url('admin_setting_banner_edit', ['id' => $Banner->getId()]));
        }
        return $app->render('Setting/Shop/banner_edit.twig', array(
            'form' => $form->createView(),
            'Banner' => $Banner
        ));
    }

    public function delete(Application $app, Request $request, $id = null)
    {
        if (!is_null($id)) {
            /** @var Banner $Banner */
            $Banner = $app['eccube.repository.banner']->find($id);
            $Banner->setDelFlg(Constant::ENABLED);
            $app['orm.em']->persist($Banner);
            $app['orm.em']->flush();
        }

        $app->addSuccess('バナーを削除しました。', 'admin');
        return $app->redirect($app->url('admin_setting_banner'));
    }
}