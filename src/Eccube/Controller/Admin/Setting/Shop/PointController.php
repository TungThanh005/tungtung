<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Controller\Admin\Setting\Shop;

use Eccube\Application;
use Eccube\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Yaml\Yaml;

class PointController extends AbstractController
{
    public function index(Application $app)
    {
        $file = $app['config']['root_dir'] . '/app/config/eccube/config.yml';
        if (!file_exists($file)) {
            log_error('File Not found: ' . $file);
            throw new NotFoundHttpException();
        }
        $config = Yaml::parse(file_get_contents($file));

        return $app->render('Setting/Shop/point.twig', array(
            'config' => $config,
        ));
    }

    public function edit(Application $app, Request $request)
    {
        if (is_numeric($request->request->get('point_basic_rate')) &&
            is_numeric($request->request->get('point_conversion_rate')) &&
            is_numeric($request->request->get('point_round_type'))
        ) {
            $file = $app['config']['root_dir'] . '/app/config/eccube/config.yml';
            $config = Yaml::parse(file_get_contents($file));
            $config['point_basic_rate'] = (int)$request->request->get('point_basic_rate');
            $config['point_conversion_rate'] = (int)$request->request->get('point_conversion_rate');
            $config['point_round_type'] = (int)$request->request->get('point_round_type');

            file_put_contents($file, Yaml::dump($config));

            $app->addSuccess('設定更新しました。', 'admin');
        } else {
            $app->addError('設定失敗しました。', 'admin');
        }

        return $app->redirect($app->url('admin_setting_shop_point'));
    }

}
