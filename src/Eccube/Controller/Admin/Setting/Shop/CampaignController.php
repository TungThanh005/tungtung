<?php

namespace Eccube\Controller\Admin\Setting\Shop;

use Eccube\Application;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Entity\Banner;
use Eccube\Entity\Campaign;
use Eccube\Entity\CampaignTargetOffice;
use Eccube\Entity\CampaignTargetProduct;
use Eccube\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CampaignController extends AbstractController
{
    public function index(Application $app, Request $request)
    {
        $condition = [
            'campaign_name' => $request->get('campaign_name'),
            'start_date' => $request->get('start_date'),
            'end_date' => $request->get('end_date'),
            'campaign_end_flg' => (bool)$request->get('campaign_end_flg'),
        ];
        $Campaigns = $app['eccube.repository.campaign']->search($condition);

        return $app->render('Setting/Shop/campaign.twig', array(
            'Campaigns' => $Campaigns
        ));
    }

    public function productRegister(Application $app, Request $request)
    {
        $Campaign = $app['eccube.repository.campaign']->find($request->request->get('campaign_id'));
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }
        $Product = $app['eccube.repository.product']->find($request->request->get('product_id'));
        if (!$Product) {
            throw new NotFoundHttpException();
        }
        $CampaignTargetProduct = $app['eccube.repository.campaign_target_product']->findOneBy([
            'Campaign' => $Campaign,
            'Product' => $Product,
        ]);
        if (!$CampaignTargetProduct) {
            $data = $app['eccube.repository.campaign']->checkValidProducts([$Product->getId()], $Campaign);
            if (isset($data[$Product->getId()])) {
                $check = $data[$Product->getId()];
                $app->addError('「' . $check['campaign_name'] . '」使用中為登録できませんでした。', 'admin');
            } else {
                $CampaignTargetProduct = new CampaignTargetProduct();
                $CampaignTargetProduct->setCampaign($Campaign);
                $CampaignTargetProduct->setProduct($Product);
                $app['orm.em']->persist($CampaignTargetProduct);
                $app['orm.em']->flush();
                $app->addSuccess('商品を登録しました', 'admin');
            }
        } else {
            $app->addError('既に商品を登録しました', 'admin');
        }
        return $app->redirect($app->url('admin_setting_campaign_product_select', ['id' => $Campaign->getId()]));
    }

    public function productSelectAll(Application $app, Request $request, $campaign_id)
    {
        $Campaign = $app['eccube.repository.campaign']->find($campaign_id);
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }
        $query = $request->get('admin_search_product');
        $searchData = array(
            'id' => trim($query['id'] ?? null),
        );
        if (!empty($query['category_id'])) {
            $searchData['category_id'] = $app['eccube.repository.category']->find($query['category_id']);
        }

        $qb = $app['eccube.repository.product']->getQueryBuilderBySearchDataForAdmin($searchData);

        $pagination = $qb->getQuery()->getResult();

        $ids = [];
        foreach ($pagination as $item) {
            $ids[] = $item->getId();
        }
        $data = $app['eccube.repository.campaign']->checkValidProducts($ids, $Campaign);

        /** @var Product $Product */
        foreach ($pagination as $Product) {
            if (!isset($data[$Product->getId()])) {
                $CampaignTargetProduct = $app['eccube.repository.campaign_target_product']->findOneBy([
                    'Campaign' => $Campaign,
                    'Product' => $Product,
                ]);
                if (!$CampaignTargetProduct) {
                    $CampaignTargetProduct = new CampaignTargetProduct();
                    $CampaignTargetProduct->setProduct($Product);
                    $CampaignTargetProduct->setCampaign($Campaign);
                    $app['orm.em']->persist($CampaignTargetProduct);

                }
            }
        }
        $app['orm.em']->flush();
        $app->addSuccess('商品を登録しました', 'admin');
        return $app->redirect($app->url('admin_setting_campaign_product_select', ['id' => $Campaign->getId()]));
    }

    /**
     * @param Application $app
     * @param Request $request
     * @param null $campaign_id
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function productDelete(Application $app, Request $request, $campaign_id = null, $id = null)
    {
        $Campaign = $app['eccube.repository.campaign']->find($campaign_id);
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }
        $Product = $app['eccube.repository.product']->find($id);
        if (!$Product) {
            throw new NotFoundHttpException();
        }

        /** @var CampaignTargetProduct $CampaignTargetProduct */
        $CampaignTargetProduct = $app['eccube.repository.campaign_target_product']->findOneBy([
            'Campaign' => $Campaign,
            'Product' => $Product,
        ]);

        $app['orm.em']->remove($CampaignTargetProduct);
        $app['orm.em']->flush();
        return $app->redirect($app->url('admin_setting_campaign_product_select', ['id' => $Campaign->getId()]));
    }


    public function productClearList(Application $app, Request $request, $campaign_id = null)
    {
        $Campaign = $app['eccube.repository.campaign']->find($campaign_id);
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }
        $CampaignTargetProducts = $app['eccube.repository.campaign_target_product']->findBy([
            'Campaign' => $Campaign,
        ]);
        foreach ($CampaignTargetProducts as $CampaignTargetProduct) {
            $app['orm.em']->remove($CampaignTargetProduct);
        }
        $app['orm.em']->flush();
        return $app->redirect($app->url('admin_setting_campaign_product_select', ['id' => $Campaign->getId()]));
    }

    public function productSelect(Application $app, Request $request, $id = null)
    {
        if (is_null($id)) {
            throw new NotFoundHttpException();
        }
        /** @var Campaign $Campaign */
        $Campaign = $app['eccube.repository.campaign']->find($id);
        if (!$Campaign->getProductSelectFlg()) {
            $app->addError('対象商品指定できません', 'admin');
            return $app->redirect($app->url('admin_setting_campaign_edit', ['id' => $Campaign->getId()]));
        }
        $CampaignTargetProducts = $app['eccube.repository.campaign_target_product']->findBy([
            'Campaign' => $Campaign
        ]);
        $searchProductModalForm = $app['form.factory']->createBuilder('admin_search_product')->getForm();

        $pagination = $app['paginator']()->paginate(
            $CampaignTargetProducts,
            $request->get('page') ? $request->get('page') : 1,
            20
        );


        return $app->render('Setting/Shop/campaign_product.twig', array(
            'Campaign' => $Campaign,
            'pagination' => $pagination,
            'searchProductModalForm' => $searchProductModalForm->createView(),
        ));
    }

    public function productSearch(Application $app, Request $request, $campaign_id)
    {
        $Campaign = $app['eccube.repository.campaign']->find($campaign_id);
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }

        $query = $request->get('admin_search_product');
        $searchData = array(
            'id' => trim($query['id'] ?? null),
        );
        if (!empty($query['category_id'])) {
            $searchData['category_id'] = $app['eccube.repository.category']->find($query['category_id']);
        }

        $qb = $app['eccube.repository.product']->getQueryBuilderBySearchDataForAdmin($searchData);
        $pageCount = $app['config']['default_page_count'];

        /** @var \Knp\Component\Pager\Pagination\SlidingPagination $pagination */
        $pagination = $app['paginator']()->paginate(
            $qb,
            $request->get('page'),
            $pageCount
        );
        $ids = [];
        foreach ($pagination as $item) {
            $ids[] = $item->getId();
        }
        $data = $app['eccube.repository.campaign']->checkValidProducts($ids, $Campaign);
        foreach ($pagination as $item) {
            $item->check = false;
            if (isset($data[$item->getId()])) {
                $item->check = $data[$item->getId()];
            }
        }

        return $app->render('Setting/Shop/campaign_product_search.twig', array(
            'pagination' => $pagination,
            'Campaign' => $Campaign
        ));

    }


    public function officeSelect(Application $app, Request $request, $id = null)
    {
        if (is_null($id)) {
            throw new NotFoundHttpException();
        }
        /** @var Campaign $Campaign */
        $Campaign = $app['eccube.repository.campaign']->find($id);
        if (!$Campaign->getOfficeSelectFlg()) {
            $app->addError('対象事業所指定できません', 'admin');
            return $app->redirect($app->url('admin_setting_campaign_edit', ['id' => $Campaign->getId()]));
        }

        $Offices = $app['eccube.repository.campaign']->getOffices($id, $request->get('q'), $request->get('page'));

        return $app->render('Setting/Shop/campaign_office.twig', array(
            'Campaign' => $Campaign,
            'Offices' => $Offices
        ));

    }

    public function realEstateSelectAll(Application $app, Request $request, $campaign_id)
    {
        $Campaign = $app['eccube.repository.campaign']->find($campaign_id);
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }

        $RealEstates = $app['eccube.repository.real_estate']->search([
            'office_no' => $request->get('office_no'),
            'real_estate_no' => $request->get('real_estate_no'),
            'office_name' => $request->get('office_name'),
            'real_estate_name' => $request->get('real_estate_name'),
            'jnet_code' => $request->get('jnet_code'),
        ], false);
        foreach ($RealEstates as $realEstate) {
            $CampaignTargetOffice = $app['eccube.repository.campaign_target_office']->findOneBy([
                'Campaign' => $Campaign,
                'office_no' => $realEstate['office_no'],
                'real_estate_no' => $realEstate['real_estate_no']
            ]);
            if (!$CampaignTargetOffice) {
                $CampaignTargetOffice = new CampaignTargetOffice();
                $CampaignTargetOffice->setCampaign($Campaign);
                $CampaignTargetOffice->setOfficeNo($realEstate['office_no']);
                $CampaignTargetOffice->setRealEstateNo($realEstate['real_estate_no']);
                $app['orm.em']->persist($CampaignTargetOffice);
            }
        }
        $app['orm.em']->flush();
        $app->addSuccess('対象事業所・物件を登録しました', 'admin');
        return $app->redirect($app->url('admin_setting_campaign_office_select', ['id' => $Campaign->getId()]));
    }

    public function realEstateSearch(Application $app, Request $request)
    {
        $RealEstates = $app['eccube.repository.real_estate']->search([
            'office_no' => $request->get('office_no'),
            'real_estate_no' => $request->get('real_estate_no'),
            'office_name' => $request->get('office_name'),
            'real_estate_name' => $request->get('real_estate_name'),
            'jnet_code' => $request->get('jnet_code'),
        ], true, $request->get('page'));

        return $app->render('Setting/Shop/campaign_office_search.twig', array(
            'RealEstates' => $RealEstates
        ));
    }

    public function realEstateDelete(Application $app, Request $request, $campaign_id = null, $id = null)
    {
        $Campaign = $app['eccube.repository.campaign']->find($campaign_id);
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }
        $CampaignTargetOffice = $app['eccube.repository.campaign_target_office']->findOneBy([
            'Campaign' => $Campaign,
            'id' => $id,
        ]);

        $app['orm.em']->remove($CampaignTargetOffice);
        $app['orm.em']->flush();
        return $app->redirect($app->url('admin_setting_campaign_office_select', ['id' => $Campaign->getId()]));
    }

    public function realEstateClearList(Application $app, Request $request, $campaign_id = null)
    {
        $Campaign = $app['eccube.repository.campaign']->find($campaign_id);
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }
        $CampaignTargetOffices = $app['eccube.repository.campaign_target_office']->findBy([
            'Campaign' => $Campaign,
        ]);
        foreach ($CampaignTargetOffices as $campaignTargetOffice) {
            $app['orm.em']->remove($campaignTargetOffice);
        }
        $app['orm.em']->flush();
        return $app->redirect($app->url('admin_setting_campaign_office_select', ['id' => $Campaign->getId()]));
    }

    public function realEstateRegister(Application $app, Request $request)
    {
        $Campaign = $app['eccube.repository.campaign']->find($request->request->get('campaign_id'));
        if (!$Campaign) {
            throw new NotFoundHttpException();
        }
        $CampaignTargetOffice = $app['eccube.repository.campaign_target_office']->findOneBy([
            'Campaign' => $Campaign,
            'office_no' => $request->request->get('office_no'),
            'real_estate_no' => $request->request->get('real_estate_no')
        ]);
        if (!$CampaignTargetOffice) {

            $CampaignTargetOffice = new CampaignTargetOffice();
            $CampaignTargetOffice->setCampaign($Campaign);
            $CampaignTargetOffice->setOfficeNo($request->request->get('office_no'));
            $CampaignTargetOffice->setRealEstateNo($request->request->get('real_estate_no'));

            $app['orm.em']->persist($CampaignTargetOffice);
            $app['orm.em']->flush();
            $app->addSuccess('対象事業所・物件を登録しました', 'admin');
        } else {
            $app->addError('既に対象事業所・物件を登録しました', 'admin');
        }
        return $app->redirect($app->url('admin_setting_campaign_office_select', ['id' => $Campaign->getId()]));
    }

    public function edit(Application $app, Request $request, $id = null)
    {
        if (is_null($id)) {
            $Campaign = new Campaign();
            $Campaign->setShop1Flg((bool)Constant::DISABLED);
            $Campaign->setShop2Flg((bool)Constant::DISABLED);
            $Campaign->setShop3Flg((bool)Constant::DISABLED);
            $Campaign->setFreeShippingFlag((bool)Constant::DISABLED);
            $Campaign->setProductSelectFlg((bool)Constant::DISABLED);
            $Campaign->setOfficeSelectFlg((bool)Constant::DISABLED);
        } else {
            /** @var Campaign $Campaign */
            $Campaign = $app['eccube.repository.campaign']->find($id);
            if (!$Campaign) {
                throw new NotFoundHttpException();
            }
            $Campaign->setShop1Flg((bool)$Campaign->getShop1Flg());
            $Campaign->setShop2Flg((bool)$Campaign->getShop2Flg());
            $Campaign->setShop3Flg((bool)$Campaign->getShop3Flg());
            $Campaign->setFreeShippingFlag((bool)$Campaign->getFreeShippingFlag());
            $Campaign->setProductSelectFlg((bool)$Campaign->getProductSelectFlg());
            $Campaign->setOfficeSelectFlg((bool)$Campaign->getOfficeSelectFlg());
        }

        $builder = $app['form.factory']
            ->createBuilder('admin_campaign', $Campaign);

        $form = $builder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $app['orm.em']->persist($Campaign);
            $app['orm.em']->flush();
            $app->addSuccess('キャンペーン情報を保存しました', 'admin');
            return $app->redirect($app->url('admin_setting_campaign_edit', ['id' => $Campaign->getId()]));
        }


        return $app->render('Setting/Shop/campaign_edit.twig', array(
            'form' => $form->createView(),
            'Campaign' => $Campaign,
            'real_estate_count' => $app['eccube.repository.campaign']->countRealEstate($id),
            'product_count' => $app['eccube.repository.campaign']->countProduct($id),
        ));
    }

    public function delete(Application $app, Request $request, $id = null)
    {
        if (!is_null($id)) {
            /** @var campaign $Campaign */
            $Campaign = $app['eccube.repository.campaign']->find($id);
            /** @var Banner $Banner */
            $Banner = $app['eccube.repository.banner']->findOneBy([
                'Campaign' => $Campaign
            ]);
            if ($Banner) {
                $Banner->setCampaign(null);
                $app['orm.em']->persist($Banner);

            }

            $Campaign->setDelFlg(Constant::ENABLED);
            $app['orm.em']->persist($Campaign);
            $app['orm.em']->flush();
        }

        $app->addSuccess('キャンペーンを削除しました。', 'admin');
        return $app->redirect($app->url('admin_setting_campaign'));
    }
}