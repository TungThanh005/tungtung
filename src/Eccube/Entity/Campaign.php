<?php

namespace Eccube\Entity;
/**
 * Campaign
 */
class Campaign
{
    const CAMPAIGN_TYPE_POINT_RATE = 1;//ポイント率
    const CAMPAIGN_TYPE_POINT_AMOUNT = 2;//ポイント額
    const CAMPAIGN_TYPE_DISCOUNT_AMOUNT = 3;//割引額
    const CAMPAIGN_TYPE_FREE_SHIP = 4;//送料無料
    const CAMPAIGN_TYPE_NONE = 0;// なし

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $campaign_name;

    /**
     * @var integer
     */
    private $campaign_type = '0';

    /**
     * @var integer
     */
    private $shop1_flg = '0';

    /**
     * @var integer
     */
    private $shop2_flg = '0';

    /**
     * @var integer
     */
    private $shop3_flg = '0';

    /**
     * @var \DateTime
     */
    private $start_date;

    /**
     * @var \DateTime
     */
    private $end_date;

    /**
     * @var integer
     */
    private $product_select_flg = '0';

    /**
     * @var integer
     */
    private $office_select_flg = '0';

    /**
     * @var string
     */
    private $point_rate;

    /**
     * @var string
     */
    private $point_amount;

    /**
     * @var string
     */
    private $discount_rate;

    /**
     * @var string
     */
    private $discount_amount;

    /**
     * @var integer
     */
    private $free_shipping_flag = '0';

    /**
     * @var \DateTime
     */
    private $create_date;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var integer
     */
    private $del_flg = '0';

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CampaignTargetProducts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CampaignTargetOffices;

    /**
     * @var \Eccube\Entity\Member
     */
    private $Creator;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->CampaignTargetProducts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->CampaignTargetOffices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set campaign_name
     *
     * @param string $campaignName
     * @return Campaign
     */
    public function setCampaignName($campaignName)
    {
        $this->campaign_name = $campaignName;

        return $this;
    }

    /**
     * Get campaign_name
     *
     * @return string
     */
    public function getCampaignName()
    {
        return $this->campaign_name;
    }

    /**
     * Set campaign_type
     *
     * @param integer $campaignType
     * @return Campaign
     */
    public function setCampaignType($campaignType)
    {
        $this->campaign_type = $campaignType;

        return $this;
    }

    /**
     * Get campaign_type
     *
     * @return integer
     */
    public function getCampaignType()
    {
        return $this->campaign_type;
    }

    /**
     * Set shop1_flg
     *
     * @param integer $shop1Flg
     * @return Campaign
     */
    public function setShop1Flg($shop1Flg)
    {
        $this->shop1_flg = $shop1Flg;

        return $this;
    }

    /**
     * Get shop1_flg
     *
     * @return integer
     */
    public function getShop1Flg()
    {
        return $this->shop1_flg;
    }

    /**
     * Set shop2_flg
     *
     * @param integer $shop2Flg
     * @return Campaign
     */
    public function setShop2Flg($shop2Flg)
    {
        $this->shop2_flg = $shop2Flg;

        return $this;
    }

    /**
     * Get shop2_flg
     *
     * @return integer
     */
    public function getShop2Flg()
    {
        return $this->shop2_flg;
    }

    /**
     * Set shop3_flg
     *
     * @param integer $shop3Flg
     * @return Campaign
     */
    public function setShop3Flg($shop3Flg)
    {
        $this->shop3_flg = $shop3Flg;

        return $this;
    }

    /**
     * Get shop3_flg
     *
     * @return integer
     */
    public function getShop3Flg()
    {
        return $this->shop3_flg;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return Campaign
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get start_date
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return Campaign
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }

    /**
     * Get end_date
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * Set product_select_flg
     *
     * @param integer $productSelectFlg
     * @return Campaign
     */
    public function setProductSelectFlg($productSelectFlg)
    {
        $this->product_select_flg = $productSelectFlg;

        return $this;
    }

    /**
     * Get product_select_flg
     *
     * @return integer
     */
    public function getProductSelectFlg()
    {
        return $this->product_select_flg;
    }

    /**
     * Set office_select_flg
     *
     * @param integer $officeSelectFlg
     * @return Campaign
     */
    public function setOfficeSelectFlg($officeSelectFlg)
    {
        $this->office_select_flg = $officeSelectFlg;

        return $this;
    }

    /**
     * Get office_select_flg
     *
     * @return integer
     */
    public function getOfficeSelectFlg()
    {
        return $this->office_select_flg;
    }

    /**
     * Set point_rate
     *
     * @param string $pointRate
     * @return Campaign
     */
    public function setPointRate($pointRate)
    {
        $this->point_rate = $pointRate;

        return $this;
    }

    /**
     * Get point_rate
     *
     * @return string
     */
    public function getPointRate()
    {
        return $this->point_rate;
    }

    /**
     * Set point_amount
     *
     * @param string $pointAmount
     * @return Campaign
     */
    public function setPointAmount($pointAmount)
    {
        $this->point_amount = $pointAmount;

        return $this;
    }

    /**
     * Get point_amount
     *
     * @return string
     */
    public function getPointAmount()
    {
        return $this->point_amount;
    }

    /**
     * Set discount_rate
     *
     * @param string $discountRate
     * @return Campaign
     */
    public function setDiscountRate($discountRate)
    {
        $this->discount_rate = $discountRate;

        return $this;
    }

    /**
     * Get discount_rate
     *
     * @return string
     */
    public function getDiscountRate()
    {
        return $this->discount_rate;
    }

    /**
     * Set discount_amount
     *
     * @param string $discountAmount
     * @return Campaign
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discount_amount = $discountAmount;

        return $this;
    }

    /**
     * Get discount_amount
     *
     * @return string
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * Set free_shipping_flag
     *
     * @param integer $freeShippingFlag
     * @return Campaign
     */
    public function setFreeShippingFlag($freeShippingFlag)
    {
        $this->free_shipping_flag = $freeShippingFlag;

        return $this;
    }

    /**
     * Get free_shipping_flag
     *
     * @return integer
     */
    public function getFreeShippingFlag()
    {
        return $this->free_shipping_flag;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Campaign
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set update_date
     *
     * @param \DateTime $updateDate
     * @return Campaign
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Set del_flg
     *
     * @param integer $delFlg
     * @return Campaign
     */
    public function setDelFlg($delFlg)
    {
        $this->del_flg = $delFlg;

        return $this;
    }

    /**
     * Get del_flg
     *
     * @return integer
     */
    public function getDelFlg()
    {
        return $this->del_flg;
    }

    /**
     * Add CampaignTargetProducts
     *
     * @param \Eccube\Entity\CampaignTargetProduct $campaignTargetProducts
     * @return Campaign
     */
    public function addCampaignTargetProduct(\Eccube\Entity\CampaignTargetProduct $campaignTargetProducts)
    {
        $this->CampaignTargetProducts[] = $campaignTargetProducts;

        return $this;
    }

    /**
     * Remove CampaignTargetProducts
     *
     * @param \Eccube\Entity\CampaignTargetProduct $campaignTargetProducts
     */
    public function removeCampaignTargetProduct(\Eccube\Entity\CampaignTargetProduct $campaignTargetProducts)
    {
        $this->CampaignTargetProducts->removeElement($campaignTargetProducts);
    }

    /**
     * Get CampaignTargetProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignTargetProducts()
    {
        return $this->CampaignTargetProducts;
    }

    /**
     * Add CampaignTargetOffices
     *
     * @param \Eccube\Entity\CampaignTargetOffice $campaignTargetOffices
     * @return Campaign
     */
    public function addCampaignTargetOffice(\Eccube\Entity\CampaignTargetOffice $campaignTargetOffices)
    {
        $this->CampaignTargetOffices[] = $campaignTargetOffices;

        return $this;
    }

    /**
     * Remove CampaignTargetOffices
     *
     * @param \Eccube\Entity\CampaignTargetOffice $campaignTargetOffices
     */
    public function removeCampaignTargetOffice(\Eccube\Entity\CampaignTargetOffice $campaignTargetOffices)
    {
        $this->CampaignTargetOffices->removeElement($campaignTargetOffices);
    }

    /**
     * Get CampaignTargetOffices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignTargetOffices()
    {
        return $this->CampaignTargetOffices;
    }

    /**
     * Set Creator
     *
     * @param \Eccube\Entity\Member $creator
     * @return Campaign
     */
    public function setCreator(\Eccube\Entity\Member $creator)
    {
        $this->Creator = $creator;

        return $this;
    }

    /**
     * Get Creator
     *
     * @return \Eccube\Entity\Member
     */
    public function getCreator()
    {
        return $this->Creator;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $OrderDetails;


    /**
     * Add OrderDetails
     *
     * @param \Eccube\Entity\OrderDetail $orderDetails
     * @return Campaign
     */
    public function addOrderDetail(\Eccube\Entity\OrderDetail $orderDetails)
    {
        $this->OrderDetails[] = $orderDetails;

        return $this;
    }

    /**
     * Remove OrderDetails
     *
     * @param \Eccube\Entity\OrderDetail $orderDetails
     */
    public function removeOrderDetail(\Eccube\Entity\OrderDetail $orderDetails)
    {
        $this->OrderDetails->removeElement($orderDetails);
    }

    /**
     * Get OrderDetails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderDetails()
    {
        return $this->OrderDetails;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CampaignBanners;


    /**
     * Add CampaignBanners
     *
     * @param \Eccube\Entity\Banner $campaignBanners
     * @return Campaign
     */
    public function addCampaignBanner(\Eccube\Entity\Banner $campaignBanners)
    {
        $this->CampaignBanners[] = $campaignBanners;

        return $this;
    }

    /**
     * Remove CampaignBanners
     *
     * @param \Eccube\Entity\Banner $campaignBanners
     */
    public function removeCampaignBanner(\Eccube\Entity\Banner $campaignBanners)
    {
        $this->CampaignBanners->removeElement($campaignBanners);
    }

    /**
     * Get CampaignBanners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaignBanners()
    {
        return $this->CampaignBanners;
    }
}
