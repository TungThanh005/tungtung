<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Banner
 */
class Banner
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $caption;

    /**
     * @var string
     */
    private $image_path;

    /**
     * @var \DateTime
     */
    private $create_date;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var integer
     */
    private $del_flg = '0';

    /**
     * @var integer
     */
    private $shop_id = '1';

    /**
     * @var \Eccube\Entity\BaseInfo
     */
    private $Shop;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Banner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set caption
     *
     * @param string $caption
     * @return Banner
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set image_path
     *
     * @param string $imagePath
     * @return Banner
     */
    public function setImagePath($imagePath)
    {
        $this->image_path = $imagePath;

        return $this;
    }

    /**
     * Get image_path
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->image_path;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Banner
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set update_date
     *
     * @param \DateTime $updateDate
     * @return Banner
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Set del_flg
     *
     * @param integer $delFlg
     * @return Banner
     */
    public function setDelFlg($delFlg)
    {
        $this->del_flg = $delFlg;

        return $this;
    }

    /**
     * Get del_flg
     *
     * @return integer
     */
    public function getDelFlg()
    {
        return $this->del_flg;
    }

    /**
     * Set shop_id
     *
     * @param integer $shopId
     * @return Banner
     */
    public function setShopId($shopId)
    {
        $this->shop_id = $shopId;

        return $this;
    }

    /**
     * Get shop_id
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shop_id;
    }

    /**
     * Set Shop
     *
     * @param \Eccube\Entity\BaseInfo $shop
     * @return Banner
     */
    public function setShop(\Eccube\Entity\BaseInfo $shop = null)
    {
        $this->Shop = $shop;

        return $this;
    }

    /**
     * Get Shop
     *
     * @return \Eccube\Entity\BaseInfo
     */
    public function getShop()
    {
        return $this->Shop;
    }

    /**
     * @var \Eccube\Entity\Master\BannerPosition
     */
    private $BannerPosition;


    /**
     * Set BannerPosition
     *
     * @param \Eccube\Entity\Master\BannerPosition $bannerPosition
     * @return Banner
     */
    public function setBannerPosition(\Eccube\Entity\Master\BannerPosition $bannerPosition)
    {
        $this->BannerPosition = $bannerPosition;

        return $this;
    }

    /**
     * Get BannerPosition
     *
     * @return \Eccube\Entity\Master\BannerPosition
     */
    public function getBannerPosition()
    {
        return $this->BannerPosition;
    }

    /**
     * @var string
     */
    private $link;

    /**
     * @var \DateTime
     */
    private $start_date;

    /**
     * @var \DateTime
     */
    private $end_date;


    /**
     * Set link
     *
     * @param string $link
     * @return Banner
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return Banner
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get start_date
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        if ($this->getCampaign()) {
            return $this->getCampaign()->getStartDate();
        }
        return $this->start_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return Banner
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }

    /**
     * Get end_date
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        if ($this->getCampaign()) {
            return $this->getCampaign()->getEndDate();
        }
        return $this->end_date;
    }

    /**
     * @var integer
     */
    private $link_method = '0';


    /**
     * Set link_method
     *
     * @param integer $linkMethod
     * @return Banner
     */
    public function setLinkMethod($linkMethod)
    {
        $this->link_method = $linkMethod;

        return $this;
    }

    /**
     * Get link_method
     *
     * @return integer
     */
    public function getLinkMethod()
    {
        return $this->link_method;
    }

    /**
     * @var \Eccube\Entity\Campaign
     */
    private $Campaign;


    /**
     * Set Campaign
     *
     * @param \Eccube\Entity\Campaign $campaign
     * @return Banner
     */
    public function setCampaign(\Eccube\Entity\Campaign $campaign = null)
    {
        $this->Campaign = $campaign;

        return $this;
    }

    /**
     * Get Campaign
     *
     * @return \Eccube\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->Campaign;
    }
}
