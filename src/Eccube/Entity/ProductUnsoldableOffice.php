<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductUnsoldableOffice
 */
class ProductUnsoldableOffice extends AbstractEntity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Eccube\Entity\Product
     */
    private $Product;

    /**
     * @var \Eccube\Entity\Office
     */
    private $Office;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Product
     *
     * @param \Eccube\Entity\Product $product
     * @return ProductUnsoldableOffice
     */
    public function setProduct(\Eccube\Entity\Product $product = null)
    {
        $this->Product = $product;

        return $this;
    }

    /**
     * Get Product
     *
     * @return \Eccube\Entity\Product
     */
    public function getProduct()
    {
        return $this->Product;
    }

    /**
     * Set Office
     *
     * @param \Eccube\Entity\Office $office
     * @return ProductUnsoldableOffice
     */
    public function setOffice(\Eccube\Entity\Office $office = null)
    {
        $this->Office = $office;

        return $this;
    }

    /**
     * Get Office
     *
     * @return \Eccube\Entity\Office
     */
    public function getOffice()
    {
        return $this->Office;
    }
}
