<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignDetail
 */
class CampaignDetail
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Eccube\Entity\BaseInfo
     */
    private $Shop;

    /**
     * @var \Eccube\Entity\Campaign
     */
    private $Campaign;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Shop
     *
     * @param \Eccube\Entity\BaseInfo $shop
     * @return CampaignDetail
     */
    public function setShop(\Eccube\Entity\BaseInfo $shop = null)
    {
        $this->Shop = $shop;

        return $this;
    }

    /**
     * Get Shop
     *
     * @return \Eccube\Entity\BaseInfo 
     */
    public function getShop()
    {
        return $this->Shop;
    }

    /**
     * Set Campaign
     *
     * @param \Eccube\Entity\Campaign $campaign
     * @return CampaignDetail
     */
    public function setCampaign(\Eccube\Entity\Campaign $campaign = null)
    {
        $this->Campaign = $campaign;

        return $this;
    }

    /**
     * Get Campaign
     *
     * @return \Eccube\Entity\Campaign 
     */
    public function getCampaign()
    {
        return $this->Campaign;
    }
}
