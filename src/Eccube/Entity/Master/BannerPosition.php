<?php

namespace Eccube\Entity\Master;

use Doctrine\ORM\Mapping as ORM;
use Eccube\Entity\AbstractEntity;

/**
 * BannerPosition
 */
class BannerPosition extends AbstractEntity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $rank;


    /**
     * Set id
     *
     * @param integer $id
     * @return BannerPosition
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BannerPosition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return BannerPosition
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Banners;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Banners = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add Banners
     *
     * @param \Eccube\Entity\Banner $banners
     * @return BannerPosition
     */
    public function addBanner(\Eccube\Entity\Banner $banners)
    {
        $this->Banners[] = $banners;

        return $this;
    }

    /**
     * Remove Banners
     *
     * @param \Eccube\Entity\Banner $banners
     */
    public function removeBanner(\Eccube\Entity\Banner $banners)
    {
        $this->Banners->removeElement($banners);
    }

    /**
     * Get Banners
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBanners()
    {
        return $this->Banners;
    }
}
