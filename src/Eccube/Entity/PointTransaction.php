<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PointTransaction
 */
class PointTransaction
{
    const TRANSACTION_TYPE_ADD = 1;
    const TRANSACTION_TYPE_SUBTRACT = 2;

    const NOT_CONFIRMED = 0;
    const CONFIRMED = 1;

    const EXTEND_FLAG_ON = 1;
    const EXTEND_FLAG_OFF = 0;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $point = '0';

    /**
     * @var integer
     */
    private $order_id;

    /**
     * @var integer
     */
    private $type = '1';

    /**
     * @var integer
     */
    private $status = '0';

    /**
     * @var \DateTime
     */
    private $commit_date;

    /**
     * @var \DateTime
     */
    private $create_date;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var \Eccube\Entity\Customer
     */
    private $Customer;

    /**
     * @var \Eccube\Entity\Order
     */
    private $Order;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PointTransaction
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set point
     *
     * @param integer $point
     * @return PointTransaction
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set order_id
     *
     * @param integer $orderId
     * @return PointTransaction
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;

        return $this;
    }

    /**
     * Get order_id
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return PointTransaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PointTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set commit_date
     *
     * @param \DateTime $commitDate
     * @return PointTransaction
     */
    public function setCommitDate($commitDate)
    {
        $this->commit_date = $commitDate;

        return $this;
    }

    /**
     * Get commit_date
     *
     * @return \DateTime
     */
    public function getCommitDate()
    {
        return $this->commit_date;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return PointTransaction
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set update_date
     *
     * @param \DateTime $updateDate
     * @return PointTransaction
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Set Customer
     *
     * @param \Eccube\Entity\Customer $customer
     * @return PointTransaction
     */
    public function setCustomer(\Eccube\Entity\Customer $customer = null)
    {
        $this->Customer = $customer;

        return $this;
    }

    /**
     * Get Customer
     *
     * @return \Eccube\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->Customer;
    }

    /**
     * Set Order
     *
     * @param \Eccube\Entity\Order $order
     * @return PointTransaction
     */
    public function setOrder(\Eccube\Entity\Order $order = null)
    {
        $this->Order = $order;

        return $this;
    }

    /**
     * Get Order
     *
     * @return \Eccube\Entity\Order
     */
    public function getOrder()
    {
        return $this->Order;
    }

    /**
     * @var string
     */
    private $note;


    /**
     * Set note
     *
     * @param string $note
     * @return PointTransaction
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @var integer
     */
    private $extend_flg = '0';


    /**
     * Set extend_flg
     *
     * @param integer $extendFlg
     * @return PointTransaction
     */
    public function setExtendFlg($extendFlg)
    {
        $this->extend_flg = $extendFlg;

        return $this;
    }

    /**
     * Get extend_flg
     *
     * @return integer
     */
    public function getExtendFlg()
    {
        return $this->extend_flg;
    }
}
