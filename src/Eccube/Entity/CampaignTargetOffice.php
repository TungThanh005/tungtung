<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignTargetOffice
 */
class CampaignTargetOffice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $office_no;

    /**
     * @var string
     */
    private $real_estate_no;

    /**
     * @var \Eccube\Entity\Campaign
     */
    private $Campaign;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set office_no
     *
     * @param string $officeNo
     * @return CampaignTargetOffice
     */
    public function setOfficeNo($officeNo)
    {
        $this->office_no = $officeNo;

        return $this;
    }

    /**
     * Get office_no
     *
     * @return string 
     */
    public function getOfficeNo()
    {
        return $this->office_no;
    }

    /**
     * Set real_estate_no
     *
     * @param string $realEstateNo
     * @return CampaignTargetOffice
     */
    public function setRealEstateNo($realEstateNo)
    {
        $this->real_estate_no = $realEstateNo;

        return $this;
    }

    /**
     * Get real_estate_no
     *
     * @return string 
     */
    public function getRealEstateNo()
    {
        return $this->real_estate_no;
    }

    /**
     * Set Campaign
     *
     * @param \Eccube\Entity\Campaign $campaign
     * @return CampaignTargetOffice
     */
    public function setCampaign(\Eccube\Entity\Campaign $campaign = null)
    {
        $this->Campaign = $campaign;

        return $this;
    }

    /**
     * Get Campaign
     *
     * @return \Eccube\Entity\Campaign 
     */
    public function getCampaign()
    {
        return $this->Campaign;
    }
}
