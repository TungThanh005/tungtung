<?php /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */

namespace Eccube\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Eccube\Common\Constant;
use Eccube\Entity\Master\Disp;
use Eccube\Entity\Master\Maker;
use Eccube\Entity\Master\Tag;
use Eccube\Util\EntityUtil;

/**
 * Product
 */
class Product extends AbstractEntity
{
    private $_calc = false;
    private $stockFinds = array();
    private $stocks = array();
    private $stockUnlimiteds = array();
    private $price01 = array();
    private $price02 = array();
    private $price_shop1 = array();
    private $price_shop2 = array();
    private $price_shop3 = array();
    private $staff_price = array();
    private $price01IncTaxs = array();
    private $price02IncTaxs = array();
    private $priceShop1IncTaxs = array();
    private $priceShop2IncTaxs = array();
    private $priceShop3IncTaxs = array();
    private $staffPriceIncTaxs = array();
    private $codes = array();
    private $classCategories1 = array();
    private $classCategories2 = array();
    private $className1;
    private $className2;
    private $point;
    private $campaign;
    private $price_discount = array();

    private $price_discount_inc_tax = array();


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    public function _calc()
    {
        if (!$this->_calc) {
            $i = 0;
            foreach ($this->getProductClasses() as $ProductClass) {
                /* @var $ProductClass \Eccube\Entity\ProductClass */
                // del_flg
                if ($ProductClass->getDelFlg() === 1) {
                    continue;
                }

                // stock_find
                $this->stockFinds[] = $ProductClass->getStockFind();

                // stock
                $this->stocks[] = $ProductClass->getStock();

                // stock_unlimited
                $this->stockUnlimiteds[] = $ProductClass->getStockUnlimited();

                // price01
                if (!is_null($ProductClass->getPrice01())) {
                    $this->price01[] = $ProductClass->getPrice01();
                    // price01IncTax
                    $this->price01IncTaxs[] = $ProductClass->getPrice01IncTax();
                }

                // price02
                $this->point[] = $ProductClass->getPoint();

                $this->price_discount[] = $ProductClass->getCampaignDiscountPrice();

                $this->price_discount_inc_tax[] = $ProductClass->getCampaignDiscountPriceIncTax();
                // price02
                $this->price02[] = $ProductClass->getPrice02();

                // price02IncTax
                $this->price02IncTaxs[] = $ProductClass->getPrice02IncTax();

                // price_shop1
                $this->price_shop1[] = $ProductClass->getPriceShop1();

                // priceShop1IncTax
                $this->priceShop1IncTaxs[] = $ProductClass->getPriceShop1IncTax();

                // priceShop2IncTax
                $this->priceShop2IncTaxs[] = $ProductClass->getPriceShop2IncTax();

                // priceShop3IncTax
                $this->priceShop3IncTaxs[] = $ProductClass->getPriceShop3IncTax();

                // price_shop2
                $this->price_shop2[] = $ProductClass->getPriceShop2();

                // price_shop3
                $this->price_shop3[] = $ProductClass->getPriceShop3();

                // staff_price
                $this->staff_price[] = $ProductClass->getStaffPrice();

                // staffPriceIncTax
                $this->staffPriceIncTaxs[] = $ProductClass->getStaffPriceIncTax();

                // product_code
                $this->codes[] = $ProductClass->getCode();

                if ($i === 0) {
                    if ($ProductClass->getClassCategory1() && $ProductClass->getClassCategory1()->getId()) {
                        $this->className1 = $ProductClass->getClassCategory1()->getClassName()->getName();
                    }
                    if ($ProductClass->getClassCategory2() && $ProductClass->getClassCategory2()->getId()) {
                        $this->className2 = $ProductClass->getClassCategory2()->getClassName()->getName();
                    }
                }
                if ($ProductClass->getClassCategory1()) {
                    $classCategoryId1 = $ProductClass->getClassCategory1()->getId();
                    if (!empty($classCategoryId1)) {
                        $this->classCategories1[$ProductClass->getClassCategory1()->getId()] = $ProductClass->getClassCategory1()->getName();
                        if ($ProductClass->getClassCategory2()) {
                            $this->classCategories2[$ProductClass->getClassCategory1()->getId()][$ProductClass->getClassCategory2()->getId()] = $ProductClass->getClassCategory2()->getName();
                        }
                    }
                }
                $i++;
            }
            $this->_calc = true;
        }
    }

    /**
     * Is Enable
     *
     * @return bool
     */
    public function isEnable()
    {
        return $this->getStatus()->getId() === Disp::DISPLAY_SHOW ? true : false;
    }

    /**
     * Get ClassName1
     *
     * @return string
     */
    public function getClassName1()
    {
        $this->_calc();

        return $this->className1;
    }

    /**
     * Get ClassName2
     *
     * @return string
     */
    public function getClassName2()
    {
        $this->_calc();

        return $this->className2;
    }

    /**
     * Get getClassCategories1
     *
     * @return array
     */
    public function getClassCategories1()
    {
        $this->_calc();

        return $this->classCategories1;
    }

    /**
     * Get getClassCategories2
     *
     * @return array
     */
    public function getClassCategories2($class_category1)
    {
        $this->_calc();

        return isset($this->classCategories2[$class_category1]) ? $this->classCategories2[$class_category1] : array();
    }

    /**
     * Get StockFind
     *
     * @return bool
     */
    public function getStockFind()
    {
        $this->_calc();

        return max($this->stockFinds);
    }

    /**
     * Get Stock min
     *
     * @return integer
     */
    public function getStockMin()
    {
        $this->_calc();

        return min($this->stocks);
    }

    /**
     * Get Stock max
     *
     * @return integer
     */
    public function getStockMax()
    {
        $this->_calc();

        return max($this->stocks);
    }

    /**
     * Get StockUnlimited min
     *
     * @return integer
     */
    public function getStockUnlimitedMin()
    {
        $this->_calc();

        return min($this->stockUnlimiteds);
    }

    /**
     * Get StockUnlimited max
     *
     * @return integer
     */
    public function getStockUnlimitedMax()
    {
        $this->_calc();

        return max($this->stockUnlimiteds);
    }

    /**
     * Get Price01 min
     *
     * @return integer
     */
    public function getPrice01Min()
    {
        $this->_calc();

        if (count($this->price01) == 0) {
            return null;
        }

        return min($this->price01);
    }

    /**
     * Get Price01 max
     *
     * @return integer
     */
    public function getPrice01Max()
    {
        $this->_calc();

        if (count($this->price01) == 0) {
            return null;
        }

        return max($this->price01);
    }
    public function getDiscountPriceIncTaxMax()
    {
        $this->_calc();

        return max($this->price_discount_inc_tax);
    }

    public function getDiscountPriceMax()
    {
        $this->_calc();

        return max($this->price_discount);
    }

    public function getDiscountPriceIncTaxMin()
    {
        $this->_calc();

        return min($this->price_discount_inc_tax);
    }

    public function getDiscountPriceMin()
    {
        $this->_calc();

        return min($this->price_discount);
    }

    /**
     * Get Price02 min
     *
     * @return integer
     */
    public function getPrice02Min()
    {
        $this->_calc();

        return min($this->price02);
    }

    /**
     * Get Price02 max
     *
     * @return integer
     */
    public function getPrice02Max()
    {
        $this->_calc();

        return max($this->price02);
    }

    /**
     * Get Price02 min
     *
     * @return integer
     */
    public function getStaffPriceMin()
    {
        $this->_calc();

        return min($this->staff_price);
    }

    /**
     * Get Price02 max
     *
     * @return integer
     */
    public function getPriceShop1Max()
    {
        $this->_calc();

        return max($this->price_shop1);
    }

    /**
     * Get Price02 min
     *
     * @return integer
     */
    public function getPriceShop1Min()
    {
        $this->_calc();

        return min($this->price_shop1);
    }

    /**
     * Get Price02 max
     *
     * @return integer
     */
    public function getPriceShop2Max()
    {
        $this->_calc();

        return max($this->price_shop2);
    }

    /**
     * Get Price02 min
     *
     * @return integer
     */
    public function getPriceShop2Min()
    {
        $this->_calc();

        return min($this->price_shop2);
    }

    /**
     * Get Price02 max
     *
     * @return integer
     */
    public function getPriceShop3Max()
    {
        $this->_calc();

        return max($this->price_shop3);
    }

    /**
     * Get Price02 min
     *
     * @return integer
     */
    public function getPriceShop3Min()
    {
        $this->_calc();

        return min($this->price_shop3);
    }


    /**
     * Get Price02 max
     *
     * @return integer
     */
    public function getStaffPriceMax()
    {
        $this->_calc();

        return max($this->staff_price);
    }


    /**
     * Get Price01IncTax min
     *
     * @return integer
     */
    public function getPrice01IncTaxMin()
    {
        $this->_calc();

        return min($this->price01IncTaxs);
    }

    /**
     * Get Price01IncTax max
     *
     * @return integer
     */
    public function getPrice01IncTaxMax()
    {
        $this->_calc();

        return max($this->price01IncTaxs);
    }

    /**
     * Get Price02IncTax min
     *
     * @return integer
     */
    public function getPrice02IncTaxMin()
    {
        $this->_calc();

        return min($this->price02IncTaxs);
    }

    /**
     * Get Price02IncTax max
     *
     * @return integer
     */
    public function getPrice02IncTaxMax()
    {
        $this->_calc();

        return max($this->price02IncTaxs);
    }

    /**
     * Get Product_code min
     *
     * @return integer
     */
    public function getCodeMin()
    {
        $this->_calc();

        $codes = array();
        foreach ($this->codes as $code) {
            if (!is_null($code)) {
                $codes[] = $code;
            }
        }
        return count($codes) ? min($codes) : null;
    }

    /**
     * Get Product_code max
     *
     * @return integer
     */
    public function getCodeMax()
    {
        $this->_calc();

        $codes = array();
        foreach ($this->codes as $code) {
            if (!is_null($code)) {
                $codes[] = $code;
            }
        }
        return count($codes) ? max($codes) : null;
    }

    /**
     * Get ClassCategories
     *
     * @return array
     */
    public function getClassCategories()
    {
        $this->_calc();

        $class_categories = array(
            '__unselected' => array(
                '__unselected' => array(
                    'name' => '選択してください',
                    'product_class_id' => '',
                ),
            ),
        );
        foreach ($this->getProductClasses() as $ProductClass) {
            /* @var $ProductClass \Eccube\Entity\ProductClass */
            $ClassCategory1 = $ProductClass->getClassCategory1();
            $ClassCategory2 = $ProductClass->getClassCategory2();

            $class_category_id1 = $ClassCategory1 ? (string)$ClassCategory1->getId() : '__unselected2';
            $class_category_id2 = $ClassCategory2 ? (string)$ClassCategory2->getId() : '';
            $class_category_name1 = $ClassCategory1 ? $ClassCategory1->getName() . ($ProductClass->getStockFind() ? '' : ' (品切れ中)') : '';
            $class_category_name2 = $ClassCategory2 ? $ClassCategory2->getName() . ($ProductClass->getStockFind() ? '' : ' (品切れ中)') : '';

            $class_categories[$class_category_id1]['#'] = array(
                'classcategory_id2' => '',
                'name' => '選択してください',
                'product_class_id' => '',
            );
            $class_categories[$class_category_id1]['#' . $class_category_id2] = array(
                'classcategory_id2' => $class_category_id2,
                'name' => $class_category_name2,
                'stock_find' => $ProductClass->getStockFind(),
                'price01' => $ProductClass->getPrice01() === null ? '' : number_format($ProductClass->getPrice01IncTax()),
                'price02' => number_format($ProductClass->getPrice02IncTax()),
                'price01ExTax' => $ProductClass->getPrice01() === null ? '' : number_format($ProductClass->getPrice01()),
                'price02ExTax' => number_format($ProductClass->getPrice02()),
                'point' => $ProductClass->getPoint() === null ? '' : number_format($ProductClass->getPoint()),
                'product_class_id' => (string)$ProductClass->getId(),
                'product_code' => $ProductClass->getCode() === null ? '' : $ProductClass->getCode(),
                'product_type' => (string)$ProductClass->getProductType()->getId(),
            );
        }

        return $class_categories;
    }

    public function getMainListImage()
    {
        $ProductImages = $this->getProductImage();
        return empty($ProductImages) ? null : $ProductImages[0];
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $note;

    /**
     * @var string
     */
    private $description_list;

    /**
     * @var string
     */
    private $description_detail;

    /**
     * @var string
     */
    private $search_word;

    /**
     * @var string
     */
    private $free_area;

    /**
     * @var integer
     */
    private $del_flg;

    /**
     * @var \DateTime
     */
    private $create_date;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ProductCategories;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ProductClasses;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CustomerFavoriteProducts;

    /**
     * @var \Eccube\Entity\Member
     */
    private $Creator;

    /**
     * @var Disp
     */
    private $Status;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ProductCategories = new ArrayCollection();
        $this->ProductClasses = new ArrayCollection();
        $this->ProductStatuses = new ArrayCollection();
        $this->CustomerFavoriteProducts = new ArrayCollection();
        $this->ProductImage = new ArrayCollection();
        $this->ProductTag = new ArrayCollection();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function copy()
    {
        // コピー対象外
        $this->CustomerFavoriteProducts = new ArrayCollection();

        $Categories = $this->getProductCategories();
        $this->ProductCategories = new ArrayCollection();
        foreach ($Categories as $Category) {
            $CopyCategory = clone $Category;
            $this->addProductCategory($CopyCategory);
            $CopyCategory->setProduct($this);
        }

        $Classes = $this->getProductClasses();
        $this->ProductClasses = new ArrayCollection();
        foreach ($Classes as $Class) {
            $CopyClass = clone $Class;
            $this->addProductClass($CopyClass);
            $CopyClass->setProduct($this);
        }

        $Images = $this->getProductImage();
        $this->ProductImage = new ArrayCollection();
        foreach ($Images as $Image) {
            $CloneImage = clone $Image;
            $this->addProductImage($CloneImage);
            $CloneImage->setProduct($this);
        }

        $Tags = $this->getProductTag();
        $this->ProductTag = new ArrayCollection();
        foreach ($Tags as $Tag) {
            $CloneTag = clone $Tag;
            $this->addProductTag($CloneTag);
            $CloneTag->setProduct($this);
        }

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set note
     *
     * @param  string $note
     * @return Product
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set description_list
     *
     * @param string $descriptionList
     * @return Product
     */
    public function setDescriptionList($descriptionList)
    {
        $this->description_list = $descriptionList;

        return $this;
    }

    /**
     * Get description_list
     *
     * @return string
     */
    public function getDescriptionList()
    {
        return $this->description_list;
    }

    /**
     * Set description_detail
     *
     * @param string $descriptionDetail
     * @return Product
     */
    public function setDescriptionDetail($descriptionDetail)
    {
        $this->description_detail = $descriptionDetail;

        return $this;
    }

    /**
     * Get description_detail
     *
     * @return string
     */
    public function getDescriptionDetail()
    {
        return $this->description_detail;
    }

    /**
     * Set search_word
     *
     * @param string $searchWord
     * @return Product
     */
    public function setSearchWord($searchWord)
    {
        $this->search_word = $searchWord;

        return $this;
    }

    /**
     * Get search_word
     *
     * @return string
     */
    public function getSearchWord()
    {
        return $this->search_word;
    }

    /**
     * Set free_area
     *
     * @param string $freeArea
     * @return Product
     */
    public function setFreeArea($freeArea)
    {
        $this->free_area = $freeArea;

        return $this;
    }

    /**
     * Get free_area
     *
     * @return string
     */
    public function getFreeArea()
    {
        return $this->free_area;
    }


    /**
     * Set del_flg
     *
     * @param  integer $delFlg
     * @return Product
     */
    public function setDelFlg($delFlg)
    {
        $this->del_flg = $delFlg;

        return $this;
    }

    /**
     * Get del_flg
     *
     * @return integer
     */
    public function getDelFlg()
    {
        return $this->del_flg;
    }

    /**
     * Set create_date
     *
     * @param  \DateTime $createDate
     * @return Product
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set update_date
     *
     * @param  \DateTime $updateDate
     * @return Product
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Add ProductCategories
     *
     * @param  \Eccube\Entity\ProductCategory $productCategories
     * @return Product
     */
    public function addProductCategory(\Eccube\Entity\ProductCategory $productCategories)
    {
        $this->ProductCategories[] = $productCategories;

        return $this;
    }

    /**
     * Remove ProductCategories
     *
     * @param \Eccube\Entity\ProductCategory $productCategories
     */
    public function removeProductCategory(\Eccube\Entity\ProductCategory $productCategories)
    {
        $this->ProductCategories->removeElement($productCategories);
    }

    /**
     * Get ProductCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductCategories()
    {
        return $this->ProductCategories;
    }

    public function getCurrentShopProductCategories()
    {
        $Categories = new ArrayCollection();
        /** @var ProductCategory $productCategory */
        $parentIds = [];
        foreach ($this->getProductCategories() as $productCategory) {
            if ($productCategory->getCategory()->getShop()->getId() == front_shop_id()) {
                $Categories[] = $productCategory;
                if ($productCategory->getCategory()->getParent())
                    $parentIds[] = $productCategory->getCategory()->getParent()->getId();
            }
        }
        // カテゴリ親を外す
        $output = new ArrayCollection();
        /** @var ProductCategory $Category */
        foreach ($Categories as $Category) {
            if (!in_array($Category->getCategory()->getId(), $parentIds)) {
                $output[] = $Category;
            }
        }

        return $output;
    }

    /**
     * Add ProductClasses
     *
     * @param  \Eccube\Entity\ProductClass $productClasses
     * @return Product
     */
    public function addProductClass(\Eccube\Entity\ProductClass $productClasses)
    {
        $this->ProductClasses[] = $productClasses;

        return $this;
    }

    /**
     * Remove ProductClasses
     *
     * @param \Eccube\Entity\ProductClass $productClasses
     */
    public function removeProductClass(\Eccube\Entity\ProductClass $productClasses)
    {
        $this->ProductClasses->removeElement($productClasses);
    }

    /**
     * Get ProductClasses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductClasses()
    {
        return $this->ProductClasses;
    }

    public function hasProductClass()
    {
        foreach ($this->ProductClasses as $ProductClass) {
            if (!is_null($ProductClass->getClassCategory1()) && $ProductClass->getDelFlg() == Constant::DISABLED) {
                return true;
            }
        }
        return false;
    }


    /**
     * Add CustomerFavoriteProducts
     *
     * @param  \Eccube\Entity\CustomerFavoriteProduct $customerFavoriteProducts
     * @return Product
     */
    public function addCustomerFavoriteProduct(\Eccube\Entity\CustomerFavoriteProduct $customerFavoriteProducts)
    {
        $this->CustomerFavoriteProducts[] = $customerFavoriteProducts;

        return $this;
    }

    /**
     * Remove CustomerFavoriteProducts
     *
     * @param \Eccube\Entity\CustomerFavoriteProduct $customerFavoriteProducts
     */
    public function removeCustomerFavoriteProduct(\Eccube\Entity\CustomerFavoriteProduct $customerFavoriteProducts)
    {
        $this->CustomerFavoriteProducts->removeElement($customerFavoriteProducts);
    }

    /**
     * Get CustomerFavoriteProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerFavoriteProducts()
    {
        return $this->CustomerFavoriteProducts;
    }

    /**
     * Set Creator
     *
     * @param  \Eccube\Entity\Member $creator
     * @return Product
     */
    public function setCreator(\Eccube\Entity\Member $creator)
    {
        $this->Creator = $creator;

        return $this;
    }

    /**
     * Get Creator
     *
     * @return \Eccube\Entity\Member
     */
    public function getCreator()
    {
        if (EntityUtil::isEmpty($this->Creator)) {
            return null;
        }
        return $this->Creator;
    }

    /**
     * Set Status
     *
     * @param  Disp $status
     * @return Product
     */
    public function setStatus(Disp $status = null)
    {
        $this->Status = $status;

        return $this;
    }

    /**
     * Get Status
     *
     * @return Disp
     */
    public function getStatus()
    {
        return $this->Status;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ProductImage;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ProductTag;


    /**
     * Add ProductImage
     *
     * @param \Eccube\Entity\ProductImage $productImage
     * @return Product
     */
    public function addProductImage(\Eccube\Entity\ProductImage $productImage)
    {
        $this->ProductImage[] = $productImage;

        return $this;
    }

    /**
     * Remove ProductImage
     *
     * @param \Eccube\Entity\ProductImage $productImage
     */
    public function removeProductImage(\Eccube\Entity\ProductImage $productImage)
    {
        $this->ProductImage->removeElement($productImage);
    }

    /**
     * Get ProductImage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductImage()
    {
        return $this->ProductImage;
    }

    public function getMainFileName()
    {
        if (count($this->ProductImage) > 0) {
            return $this->ProductImage[0];
        } else {
            return null;
        }
    }

    /**
     * Add ProductTag
     *
     * @param \Eccube\Entity\ProductTag $productTag
     * @return Product
     */
    public function addProductTag(\Eccube\Entity\ProductTag $productTag)
    {
        $this->ProductTag[] = $productTag;

        return $this;
    }

    /**
     * Remove ProductTag
     *
     * @param \Eccube\Entity\ProductTag $productTag
     */
    public function removeProductTag(\Eccube\Entity\ProductTag $productTag)
    {
        $this->ProductTag->removeElement($productTag);
    }

    /**
     * Get ProductTag
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductTag()
    {
        return $this->ProductTag;
    }

    /**
     * @var Disp
     */
    private $StatusShop1;

    /**
     * @var Disp
     */
    private $StatusShop2;

    /**
     * @var Disp
     */
    private $StatusShop3;

    public function getPointMin()
    {
        return min($this->point);
    }

    public function getPointMax()
    {
        return max($this->point);
    }

    /**
     * Set StatusShop1
     *
     * @param Disp $statusShop1
     * @return Product
     */
    public function setStatusShop1(Disp $statusShop1 = null)
    {
        $this->StatusShop1 = $statusShop1;

        return $this;
    }

    /**
     * Get StatusShop1
     *
     * @return Disp
     */
    public function getStatusShop1()
    {
        return $this->StatusShop1;
    }

    /**
     * Set StatusShop2
     *
     * @param Disp $statusShop2
     * @return Product
     */
    public function setStatusShop2(Disp $statusShop2 = null)
    {
        $this->StatusShop2 = $statusShop2;

        return $this;
    }

    /**
     * Get StatusShop2
     *
     * @return Disp
     */
    public function getStatusShop2()
    {
        return $this->StatusShop2;
    }

    /**
     * Set StatusShop3
     *
     * @param Disp $statusShop3
     * @return Product
     */
    public function setStatusShop3(Disp $statusShop3 = null)
    {
        $this->StatusShop3 = $statusShop3;

        return $this;
    }

    /**
     * Get StatusShop3
     *
     * @return Disp
     */
    public function getStatusShop3()
    {
        return $this->StatusShop3;
    }

    public function getTags()
    {
        $tags = [];
        foreach ($this->getProductTag() as $item) {
            //TAG ID > 200の場合 表示しない
            if ($item->getTag()->getId() >= 200) continue;
            $tags[$item->getTag()->getId()] = $item->getTag();
        }
        usort($tags, function ($a, $b) {
            if ($a->getRank() == $b->getRank()) {
                return 0;
            }
            return ($a->getRank() < $b->getRank()) ? -1 : 1;

        });
        return $tags;
    }

    public function getShortTags()
    {
        /** @var Tag $tag */
        $tags = [];
        foreach ($this->getTags() as $tag) {
            if ($tag->getIsShort()) {
                $tags[] = $tag;
            }
        }
        return $tags;
    }

    public function getLongTags()
    {
        /** @var Tag $tag */
        $tags = [];
        foreach ($this->getTags() as $tag) {
            if (!$tag->getIsShort()) {
                $tags[] = $tag;
            }
        }
        return $tags;
    }


    public function getIsNewArrival()
    {
        foreach ($this->getProductTag() as $item) {
            if ($item->getTag()->getId() == 1) {
                return true;
            }
        }
        return false;

    }

    /**
     * @var string
     */
    private $agency;

    /**
     * @var integer
     */
    private $fine_passport_flg = '0';

    /**
     * @var integer
     */
    private $subscription_flg = '0';


    /**
     * Set agency
     *
     * @param string $agency
     * @return Product
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return string
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set fine_passport_flg
     *
     * @param integer $finePassportFlg
     * @return Product
     */
    public function setFinePassportFlg($finePassportFlg)
    {
        $this->fine_passport_flg = $finePassportFlg;

        return $this;
    }

    /**
     * Get fine_passport_flg
     *
     * @return integer
     */
    public function getFinePassportFlg()
    {
        return $this->fine_passport_flg;
    }

    /**
     * Set subscription_flg
     *
     * @param integer $subscriptionFlg
     * @return Product
     */
    public function setSubscriptionFlg($subscriptionFlg)
    {
        $this->subscription_flg = $subscriptionFlg;

        return $this;
    }

    /**
     * Get subscription_flg
     *
     * @return integer
     */
    public function getSubscriptionFlg()
    {
        return $this->subscription_flg;
    }

    /**
     * @var integer
     */
    private $escort_flg = '0';


    /**
     * Set escort_flg
     *
     * @param integer $escortFlg
     * @return Product
     */
    public function setEscortFlg($escortFlg)
    {
        $this->escort_flg = $escortFlg;

        return $this;
    }

    /**
     * Get escort_flg
     *
     * @return integer
     */
    public function getEscortFlg()
    {
        return $this->escort_flg;
    }

    /**
     * @var Maker
     */
    private $Maker;


    /**
     * Set maker_id
     *
     * @param integer $makerId
     * @return Product
     */
    public function setMakerId($makerId)
    {
        $this->maker_id = $makerId;

        return $this;
    }

    /**
     * Get maker_id
     *
     * @return integer
     */
    public function getMakerId()
    {
        return $this->maker_id;
    }

    /**
     * Set Maker
     *
     * @param Maker $maker
     * @return Product
     */
    public function setMaker(Maker $maker)
    {
        $this->Maker = $maker;

        return $this;
    }

    /**
     * Get Maker
     *
     * @return Maker
     */
    public function getMaker()
    {
        return $this->Maker;
    }

    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;
    }

    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ProductUnsoldableOffices;


    /**
     * Add ProductUnsoldableOffices
     *
     * @param \Eccube\Entity\ProductUnsoldableOffice $productUnsoldableOffices
     * @return Product
     */
    public function addProductUnsoldableOffice(\Eccube\Entity\ProductUnsoldableOffice $productUnsoldableOffices)
    {
        $this->ProductUnsoldableOffices[] = $productUnsoldableOffices;

        return $this;
    }

    /**
     * Remove ProductUnsoldableOffices
     *
     * @param \Eccube\Entity\ProductUnsoldableOffice $productUnsoldableOffices
     */
    public function removeProductUnsoldableOffice(\Eccube\Entity\ProductUnsoldableOffice $productUnsoldableOffices)
    {
        $this->ProductUnsoldableOffices->removeElement($productUnsoldableOffices);
    }

    /**
     * Get ProductUnsoldableOffices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductUnsoldableOffices()
    {
        return $this->ProductUnsoldableOffices;
    }

    /**
     * @var integer
     */
    private $maker_id;


}
