<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Eccube\Entity;

class CartItem extends \Eccube\Entity\AbstractEntity
{

    private $class_name;
    private $class_id;
    private $price;
    private $quantity;
    private $object;
    private $point;
    private $campaign_discount;
    private $campaign_discount_inc_tax;
    private $campaign_free_ship_flg;

    private $campaign_discount_price;
    private $campaign_discount_price_inc_tax;
    private $campaign_name;
    private $campaign_id;
    private $campaign_point_amount;

    public function __construct()
    {
    }

    public function __sleep()
    {
        return array('class_name', 'class_id', 'price', 'quantity', 'point', 'campaign_discount', 'campaign_discount_inc_tax', 'campaign_free_ship_flg', 'campaign_discount_price', 'campaign_discount_price_inc_tax', 'campaign_id', 'campaign_name', 'campaign_point_amount');
    }

    /**
     * @param  string $class_name
     * @return CartItem
     */
    public function setClassName($class_name)
    {
        $this->class_name = $class_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->class_name;
    }

    /**
     * @param  string $class_id
     * @return CartItem
     */
    public function setClassId($class_id)
    {
        $this->class_id = $class_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassId()
    {
        return $this->class_id;
    }

    /**
     * @param  integer $price
     * @return CartItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param  integer $quantity
     * @return CartItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return integer
     */
    public function getTotalPrice()
    {
        return $this->getPrice() * $this->getQuantity();
    }

    /**
     * @param  object $object
     * @return CartItem
     */
    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $point
     * @return CartItem
     */
    public function setPoint($point)
    {
        $this->point = $point;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getCampaignDiscount()
    {
        return $this->campaign_discount;
    }

    /**
     * @param mixed $campaign_discount
     * @return CartItem
     */
    public function setCampaignDiscount($campaign_discount)
    {
        $this->campaign_discount = $campaign_discount;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getCampaignFreeShipFlg()
    {
        return $this->campaign_free_ship_flg;
    }

    /**
     * @param mixed $campaign_free_ship_flg
     * @return CartItem
     */
    public function setCampaignFreeShipFlg($campaign_free_ship_flg)
    {
        $this->campaign_free_ship_flg = $campaign_free_ship_flg;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getCampaignDiscountPrice()
    {
        return $this->campaign_discount_price;
    }

    /**
     * @param mixed $campaign_discount_price
     * @return CartItem
     */
    public function setCampaignDiscountPrice($campaign_discount_price)
    {
        $this->campaign_discount_price = $campaign_discount_price;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getCampaignDiscountPriceIncTax()
    {
        return $this->campaign_discount_price_inc_tax;
    }

    /**
     * @param mixed $campaign_discount_price_inc_tax
     * @return CartItem
     */
    public function setCampaignDiscountPriceIncTax($campaign_discount_price_inc_tax)
    {
        $this->campaign_discount_price_inc_tax = $campaign_discount_price_inc_tax;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getCampaignDiscountIncTax()
    {
        return $this->campaign_discount_inc_tax;
    }

    /**
     * @param mixed $campaign_discount_inc_tax
     * @return CartItem
     */
    public function setCampaignDiscountIncTax($campaign_discount_inc_tax)
    {
        $this->campaign_discount_inc_tax = $campaign_discount_inc_tax;
        return $this;
    }

    public function getTotalCampaignDiscount()
    {
        return $this->getCampaignDiscount() * $this->getQuantity();
    }

    public function getTotalCampaignDiscountIncTax()
    {
        return $this->getCampaignDiscountIncTax() * $this->getQuantity();
    }

    public function getTotalCampaignPoint()
    {
        return $this->getCampaignPointAmount() * $this->getQuantity();
    }

    public function getTotalPoint()
    {
        return $this->getPoint() * $this->getQuantity();
    }

    /**
     * @return mixed
     */
    public function getCampaignName()
    {
        return $this->campaign_name;
    }

    /**
     * @param mixed $campaign_name
     * @return CartItem
     */
    public function setCampaignName($campaign_name)
    {
        $this->campaign_name = $campaign_name;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    /**
     * @param mixed $campaign_id
     * @return CartItem
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = $campaign_id;
        return $this;

    }

    public function getCampaignPointAmount()
    {
        return $this->campaign_point_amount;
    }

    public function setCampaignPointAmount($amount)
    {
        $this->campaign_point_amount = $amount;
        return $this;

    }
}
