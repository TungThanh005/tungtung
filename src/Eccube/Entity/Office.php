<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Office
 */
class Office extends AbstractEntity
{
    /**
     * @var string
     */
    private $office_no;

    /**
     * @var string
     */
    private $office_name;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Customers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Customers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set office_no
     *
     * @param string $officeNo
     * @return Office
     */
    public function setOfficeNo($officeNo)
    {
        $this->office_no = $officeNo;

        return $this;
    }

    /**
     * Get office_no
     *
     * @return string 
     */
    public function getOfficeNo()
    {
        return $this->office_no;
    }

    /**
     * Set office_name
     *
     * @param string $officeName
     * @return Office
     */
    public function setOfficeName($officeName)
    {
        $this->office_name = $officeName;

        return $this;
    }

    /**
     * Get office_name
     *
     * @return string 
     */
    public function getOfficeName()
    {
        return $this->office_name;
    }

    /**
     * Set update_date
     *
     * @param \DateTime $updateDate
     * @return Office
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Add Customers
     *
     * @param \Eccube\Entity\Customer $customers
     * @return Office
     */
    public function addCustomer(\Eccube\Entity\Customer $customers)
    {
        $this->Customers[] = $customers;

        return $this;
    }

    /**
     * Remove Customers
     *
     * @param \Eccube\Entity\Customer $customers
     */
    public function removeCustomer(\Eccube\Entity\Customer $customers)
    {
        $this->Customers->removeElement($customers);
    }

    /**
     * Get Customers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomers()
    {
        return $this->Customers;
    }
}
