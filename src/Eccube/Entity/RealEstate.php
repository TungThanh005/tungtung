<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RealEstate
 */
class RealEstate
{
    /**
     * @var string
     */
    private $real_estate_no;

    /**
     * @var string
     */
    private $jnet_code;

    /**
     * @var string
     */
    private $real_estate_name;

    /**
     * @var string
     */
    private $office_no;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Customers;

    /**
     * @var \Eccube\Entity\Office
     */
    private $Office;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Customers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set real_estate_no
     *
     * @param string $realEstateNo
     * @return RealEstate
     */
    public function setRealEstateNo($realEstateNo)
    {
        $this->real_estate_no = $realEstateNo;

        return $this;
    }

    /**
     * Get real_estate_no
     *
     * @return string 
     */
    public function getRealEstateNo()
    {
        return $this->real_estate_no;
    }

    /**
     * Set jnet_code
     *
     * @param string $jnetCode
     * @return RealEstate
     */
    public function setJnetCode($jnetCode)
    {
        $this->jnet_code = $jnetCode;

        return $this;
    }

    /**
     * Get jnet_code
     *
     * @return string 
     */
    public function getJnetCode()
    {
        return $this->jnet_code;
    }

    /**
     * Set real_estate_name
     *
     * @param string $realEstateName
     * @return RealEstate
     */
    public function setRealEstateName($realEstateName)
    {
        $this->real_estate_name = $realEstateName;

        return $this;
    }

    /**
     * Get real_estate_name
     *
     * @return string 
     */
    public function getRealEstateName()
    {
        return $this->real_estate_name;
    }

    /**
     * Set office_no
     *
     * @param string $officeNo
     * @return RealEstate
     */
    public function setOfficeNo($officeNo)
    {
        $this->office_no = $officeNo;

        return $this;
    }

    /**
     * Get office_no
     *
     * @return string 
     */
    public function getOfficeNo()
    {
        return $this->office_no;
    }

    /**
     * Set update_date
     *
     * @param \DateTime $updateDate
     * @return RealEstate
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Add Customers
     *
     * @param \Eccube\Entity\Customer $customers
     * @return RealEstate
     */
    public function addCustomer(\Eccube\Entity\Customer $customers)
    {
        $this->Customers[] = $customers;

        return $this;
    }

    /**
     * Remove Customers
     *
     * @param \Eccube\Entity\Customer $customers
     */
    public function removeCustomer(\Eccube\Entity\Customer $customers)
    {
        $this->Customers->removeElement($customers);
    }

    /**
     * Get Customers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomers()
    {
        return $this->Customers;
    }

    /**
     * Set Office
     *
     * @param \Eccube\Entity\Office $office
     * @return RealEstate
     */
    public function setOffice(\Eccube\Entity\Office $office = null)
    {
        $this->Office = $office;

        return $this;
    }

    /**
     * Get Office
     *
     * @return \Eccube\Entity\Office
     */
    public function getOffice()
    {
        return $this->Office;
    }
}
