<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignTargetProduct
 */
class CampaignTargetProduct
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Eccube\Entity\Campaign
     */
    private $Campaign;

    /**
     * @var \Eccube\Entity\Product
     */
    private $Product;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Campaign
     *
     * @param \Eccube\Entity\Campaign $campaign
     * @return CampaignTargetProduct
     */
    public function setCampaign(\Eccube\Entity\Campaign $campaign = null)
    {
        $this->Campaign = $campaign;

        return $this;
    }

    /**
     * Get Campaign
     *
     * @return \Eccube\Entity\Campaign 
     */
    public function getCampaign()
    {
        return $this->Campaign;
    }

    /**
     * Set Product
     *
     * @param \Eccube\Entity\Product $product
     * @return CampaignTargetProduct
     */
    public function setProduct(\Eccube\Entity\Product $product = null)
    {
        $this->Product = $product;

        return $this;
    }

    /**
     * Get Product
     *
     * @return \Eccube\Entity\Product 
     */
    public function getProduct()
    {
        return $this->Product;
    }
}
