<?php

namespace Eccube\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AeonregiTransaction
 */
class AeonregiTransaction
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $lid_m;

    /**
     * @var string
     */
    private $shop_code;

    /**
     * @var string
     */
    private $proc_id;

    /**
     * @var string
     */
    private $auth_id;

    /**
     * @var string
     */
    private $customer_id;

    /**
     * @var string
     */
    private $total_amount;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $raw;

    /**
     * @var \DateTime
     */
    private $create_date;

    /**
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var \Eccube\Entity\Order
     */
    private $Order;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lid_m
     *
     * @param string $lidM
     * @return AeonregiTransaction
     */
    public function setLidM($lidM)
    {
        $this->lid_m = $lidM;

        return $this;
    }

    /**
     * Get lid_m
     *
     * @return string 
     */
    public function getLidM()
    {
        return $this->lid_m;
    }

    /**
     * Set shop_code
     *
     * @param string $shopCode
     * @return AeonregiTransaction
     */
    public function setShopCode($shopCode)
    {
        $this->shop_code = $shopCode;

        return $this;
    }

    /**
     * Get shop_code
     *
     * @return string 
     */
    public function getShopCode()
    {
        return $this->shop_code;
    }

    /**
     * Set proc_id
     *
     * @param string $procId
     * @return AeonregiTransaction
     */
    public function setProcId($procId)
    {
        $this->proc_id = $procId;

        return $this;
    }

    /**
     * Get proc_id
     *
     * @return string 
     */
    public function getProcId()
    {
        return $this->proc_id;
    }

    /**
     * Set auth_id
     *
     * @param string $authId
     * @return AeonregiTransaction
     */
    public function setAuthId($authId)
    {
        $this->auth_id = $authId;

        return $this;
    }

    /**
     * Get auth_id
     *
     * @return string 
     */
    public function getAuthId()
    {
        return $this->auth_id;
    }

    /**
     * Set customer_id
     *
     * @param string $customerId
     * @return AeonregiTransaction
     */
    public function setCustomerId($customerId)
    {
        $this->customer_id = $customerId;

        return $this;
    }

    /**
     * Get customer_id
     *
     * @return string 
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Set total_amount
     *
     * @param string $totalAmount
     * @return AeonregiTransaction
     */
    public function setTotalAmount($totalAmount)
    {
        $this->total_amount = $totalAmount;

        return $this;
    }

    /**
     * Get total_amount
     *
     * @return string 
     */
    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return AeonregiTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set raw
     *
     * @param string $raw
     * @return AeonregiTransaction
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return string 
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return AeonregiTransaction
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set update_date
     *
     * @param \DateTime $updateDate
     * @return AeonregiTransaction
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Set Order
     *
     * @param \Eccube\Entity\Order $order
     * @return AeonregiTransaction
     */
    public function setOrder(\Eccube\Entity\Order $order = null)
    {
        $this->Order = $order;

        return $this;
    }

    /**
     * Get Order
     *
     * @return \Eccube\Entity\Order 
     */
    public function getOrder()
    {
        return $this->Order;
    }
}
