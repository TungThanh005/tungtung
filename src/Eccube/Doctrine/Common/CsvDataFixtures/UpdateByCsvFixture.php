<?php

namespace Eccube\Doctrine\Common\CsvDataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Eccube\Util\LogUtil;

/**
 * CSVファイルからデータを取り込んで、中身のIDにより既存テーブルを更新するフィクスチャ
 *
 * @see https://github.com/doctrine/data-fixtures/blob/master/lib/Doctrine/Common/DataFixtures/FixtureInterface.php
 */
class UpdateByCsvFixture implements FixtureInterface
{
    /** @var \SplFileObject $file */
    protected $file;

    protected $entity;

    protected $table_logical;

    protected $table_physical;

    protected $cols;

    protected $update_key_index;

    protected $header_read_flg;

    public function __construct(\SplFileObject $file, array $csv)
    {
        $this->file = $file;
        $this->entity = $csv['entity'];
        $this->table_logical = $csv['table_logical'];
        $this->table_physical = $csv['table_physical'];
        $this->cols = $csv['cols'];
        $this->update_key_index = $csv['update_key_index'];
        $this->type = 'real_estate';
    }

    public function setHeaderReadFlg($headerReadFlg)
    {
        $this->header_read_flg = $headerReadFlg;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {

        // CSV Reader に設定
        $this->file->setFlags(\SplFileObject::READ_CSV | \SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE);
        
        if (empty($this->file)) {
            throw new \Exception('CSVファイルが指定されていません');
        }
        
        if (empty($this->entity)) {
            throw new \Exception('保存先エンティティが指定されていません');
        }

        if (empty($this->table_physical)) {
            throw new \Exception('削除するテーブル名が指定されていません');
        }

        // ヘッダから保存先を取得する場合は、ヘッダ行をスキップ
        if ($this->header_read_flg) {
            $this->cols = $this->file->current();
            $this->file->next();
        } else {
            if (empty($this->cols)) {
                throw new \Exception('保存元カラムが指定されていません');
            }
        }

        // NO_AUTO_VALUE_ON_ZEROを設定
        //$Connection->exec("SET SESSION sql_mode='NO_AUTO_VALUE_ON_ZERO';");

        if (empty($this->entity)) {
            throw new \Exception("指定のエンティティクラスが存在しません：{$this->entity}");
        }

        if (is_null($this->update_key_index)) {
            throw new \Exception("更新キー位置指定がありません：{$this->update_key_index}");
        }

        $row_index = 0;
        //データ更新
        //1行ごとに処理し、問題が発生すれば行をスキップする。トランザクションをかけない
        $bk_manager = clone $manager;

        $dt = new \DateTime(date('Y-m-d H:i:s'));
        while ($fcols = $this->file->current()) {
            //$Connection = $manager->getConnection();
            //$Connection->beginTransaction();
            $row_index++;
            try {
                //途中のエラーによってはmanagerが閉じられてしまうのでその場合は、バックアップに接続しなおす
                if (!$manager->isOpen()) {
                    $manager = $bk_manager;
                    $bk_manager = clone $manager;
                }
                $entity = $manager->getRepository($this->entity)->findOneBy([
                    $this->cols[$this->update_key_index] => $fcols[$this->update_key_index]
                ]);
                if (empty($entity)) {
                    LogUtil::putFile("{$row_index}件目の更新キーが一致しないため、処理をスキップしました", 
                    config('command_log_output_path') . "/{$this->type}/" . date("ym") . "_{$this->type}.log");
                    $this->file->next();
                    //$Connection->close();
                    continue;
                }
                foreach ($fcols as $index => $fcol) {
                    $setter = "set" . $this->camelize($this->cols[$index]);
                    $entity->$setter($this->convertUTF8($fcol));
                }
                if (method_exists($entity, "setUpdateDate")) {
                    $entity->setUpdateDate($dt);
                }
                $manager->persist($entity);
                $manager->flush();
                $manager->clear();
                //$Connection->commit();
            } catch (\Exception $e) {
                LogUtil::putFile("{$this->table_logical}の{$row_index}件目でエラーが発生したため、処理をスキップしました\n{$e->getMessage()}\n{$e->getTraceAsString()}",
                config('command_log_output_path') . "/{$this->type}/" . date("ym") . "_{$this->type}.log");
                //$Connection->rollback();
                //$Connection->close();
            }
            $this->file->next();
            //$Connection->close();
        }
    }

    private function convertUTF8(string $row_str)
    {
        return mb_convert_encoding($row_str, 'UTF-8', 'SJIS-win');
    }

    private function camelize(string $s)
    {
        return str_replace([' ', '-', '_'], '', ucwords($s, ' -_'));
    }

    private function escapeIdent($table)
    {
        return preg_replace('/`/', '``', $table);
    }
}
