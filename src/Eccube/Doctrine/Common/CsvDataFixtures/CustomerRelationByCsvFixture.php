<?php

namespace Eccube\Doctrine\Common\CsvDataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Eccube\Util\LogUtil;
use Eccube\Entity\Customer;

/**
 * CSVファイルから会員系テーブルの初期データを投入するフィクスチャ
 *
 * @see https://github.com/doctrine/data-fixtures/blob/master/lib/Doctrine/Common/DataFixtures/FixtureInterface.php
 */
class CustomerRelationByCsvFixture implements FixtureInterface
{
    /** @var \SplFileObject $file */
    protected $file;

    protected $shop_id;

    protected $update_key_index;

    protected $no_csv_cols_map;

    protected $csv_cols_map;

    protected $get_customer_col_for_addr;

    protected $header_read_flg;

    public function __construct(\SplFileObject $file, array $csv)
    {
        $this->file = $file;
        $this->shop_id = $csv['shop_id'];
        $this->update_key_index = $csv['update_key_index'];
        $this->no_csv_cols_map = $csv['no_csv_cols_map'];
        $this->csv_cols_map = $csv['csv_cols_map'];
        $this->get_customer_col_for_addr = $csv['get_customer_col_for_addr'];
    }

    public function setHeaderReadFlg($headerReadFlg)
    {
        $this->header_read_flg = $headerReadFlg;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {

        // CSV Reader に設定
        $this->file->setFlags(\SplFileObject::READ_CSV | \SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY | \SplFileObject::DROP_NEW_LINE);
        
        if (empty($this->file)) {
            throw new \Exception('CSVファイルが指定されていません');
        }
        
        if (empty($this->shop_id)) {
            throw new \Exception('店舗IDが指定されていません');
        }

        if (empty($this->update_key_index) ||
            empty($this->no_csv_cols_map) ||
            empty($this->csv_cols_map) ||
            empty($this->get_customer_col_for_addr)
        ) {
            throw new \Exception('必要な設定ファイルが定義されていません');
        }

        // ヘッダがある場合は、ヘッダ行をスキップ
        if ($this->header_read_flg) {
            $this->file->next();
        }

        $app = \Eccube\Application::getInstance();
        $app->initialize();

        $row_index = 0;
        /*
            データ登録・更新
            ・1行ごとに会員情報・お届け先情報の登録をする。すでにデータが存在していれば会員情報については更新し、お届け先情報には何もしない
            ・1行ごとに会員情報・お届け先情報をトランザクションで処理し、問題が発生すればその行をロールバックしてスクリプトを停止する
            ・MySQLが初期値設定してくれるものは登録しない
            ・システムで初期値設定するものはYml参照して登録する
            ・それ以外は個別ロジックの関数を呼んで登録する
         */
        $dt = new \DateTime(date('Y-m-d H:i:s'));

        while ($fcols = $this->file->current()) {
            $Connection = $manager->getConnection();
            $Connection->beginTransaction();

            $row_index++;
            try {
                $entity = $manager->getRepository("Eccube\Entity\Customer")->findOneBy(
                    [
                        'customer_no' => $fcols[$this->update_key_index]
                    ]
                );
                //既存の会員データがない場合は新規登録
                if (empty($entity)) {
                    try {
                        $entity = new Customer();

                        $this->setShopIdByOrm($app['orm.em'], $entity, $this->shop_id);
                        $this->setSecretKeyByRandom($app, $entity);

                        $entity->setCreateDate($dt);
                        $entity->setUpdateDate($dt);

                        //初期値をYmlから入れるもの
                        //UTF-8コンバートは不要
                        foreach ($this->no_csv_cols_map as $setter => $ar) {
                            if ($ar['is_orm_ref']) {
                                $setter = "{$setter}ByOrmRef";
                                $this->$setter($app['orm.em'], $entity, $ar['value']);
                            } else {
                                $entity->$setter($ar['value']);
                            }
                        }
                        
                        //CSVから取得する、値をそのまま入れられるもの・個別ロジックが必要なもの
                        //UTF-8コンバートが必要
                        foreach ($this->csv_cols_map as $setter => $ar) {
                            $utf8value = $this->convertUTF8($fcols[$ar['index']]);
                            if ($ar['is_direct']) {
                                $entity->$setter($utf8value);
                            } elseif ($ar['is_orm_ref']) {
                                $setter = "{$setter}ByOrmRef";
                                $this->$setter($app['orm.em'], $entity, $utf8value);
                            } else {
                                $setter = "{$setter}ByLogic";
                                $this->$setter($entity, $utf8value);
                            }
                        }
                        $manager->persist($entity);
                        $manager->flush();
                    } catch (\Exception $e) {
                        throw new \Exception("{$row_index}行目の会員情報の登録でエラーが発生したため、処理を中止しました。\n{$e->getMessage()}\n{$e->getTraceAsString()}");
                    }
                } else {
                    //更新の場合
                    //初期値をYmlから入れるもの
                    try {
                        $this->setShopIdByOrm($app['orm.em'], $entity, $this->shop_id);
                        $entity->setUpdateDate($dt);
                        foreach ($this->no_csv_cols_map as $setter => $ar) {
                            if ($setter == 'setPoint' || $setter == 'setTemporaryPoint') {
                                continue;
                            }
                            if ($ar['is_orm_ref']) {
                                $setter = "{$setter}ByOrmRef";
                                $this->$setter($app['orm.em'], $entity, $ar['value']);
                            } else {
                                $entity->$setter($ar['value']);
                            }
                        }
                        //CSVから取得する、値をそのまま入れられるもの・個別ロジックが必要なもの
                        //UTF-8コンバートが必要
                        foreach ($this->csv_cols_map as $setter => $ar) {
                            $utf8value = $this->convertUTF8($fcols[$ar['index']]);
                            if ($ar['is_direct']) {
                                $entity->$setter($utf8value);
                            } elseif ($ar['is_orm_ref']) {
                                $setter = "{$setter}ByOrmRef";
                                $this->$setter($app['orm.em'], $entity, $utf8value);
                            } else {
                                $setter = "{$setter}ByLogic";
                                $this->$setter($entity, $utf8value);
                            }
                        }
                        $manager->persist($entity);
                        $manager->flush();
                    } catch (\Exception $e) {
                        throw new \Exception("{$row_index}行目の会員情報の更新でエラーが発生したため、処理を中止しました。\n{$e->getMessage()}\n{$e->getTraceAsString()}");
                    }
                }
                try {
                    //お届け先情報にコピー。存在しない場合の登録のみで更新はしない
                    $ref_entity = $manager->getRepository("Eccube\Entity\CustomerAddress")->findOneBy(
                        [
                            'Customer' => $entity->getId()
                        ]
                    );
                    if (empty($ref_entity)) {
                        $ref_entity = new \Eccube\Entity\CustomerAddress;
                        $ref_entity->setCreateDate($dt);
                        $ref_entity->setUpdateDate($dt);
                        $ref_entity->setCustomer($entity);
                        foreach ($this->get_customer_col_for_addr as $col) {
                            $getter = "get{$col}";
                            $setter = "set{$col}";
                            $ref_entity->$setter($entity->$getter());
                        }
                        $manager->persist($ref_entity);
                        $manager->flush();
                    }
                } catch (\Exception $e) {
                    throw new \Exception("{$row_index}行目のお届け先情報の更新でエラーが発生したため、処理を中止しました。\n{$e->getMessage()}\n{$e->getTraceAsString()}");
                }
                $Connection->commit();
            } catch (\Exception $e) {
                $Connection->rollback();
                throw $e;
                //LogUtil::putFile("{$e->getMessage()}", config('command_log_output_path'). date("md") . '.log');
            }
            $manager->clear();
            $Connection->close();
            $this->file->next();
        }
    }

    private function setShopIdByOrm($em, $entity, $value)
    {
        $BaseInfo = $em->getRepository('Eccube\Entity\BaseInfo')->findOneBy([
            'id' => $value
        ]);
        $entity->setShop($BaseInfo);
    }

    private function setSecretKeyByRandom($app, $entity)
    {
        $entity->setSecretKey($app['eccube.repository.customer']->getUniqueSecretKey($app));
    }

    private function setSecretKeyByOrmRef($em, $entity, $value)
    {
        $BaseInfo = $em->getRepository('Eccube\Entity\BaseInfo')->findOneBy([
            'id' => $value
        ]);
        $entity->setShop($BaseInfo);
    }

    private function setStatusByOrmRef($em, $entity, $value)
    {
        $CustomerStatus = $em->getRepository('Eccube\Entity\Master\CustomerStatus')->findOneBy([
            'id' => $value
        ]);
        $entity->setStatus($CustomerStatus);
    }

    private function setPrefNameByOrmRef($em, $entity, $value)
    {
        $Pref = $em->getRepository('Eccube\Entity\Master\Pref')->findOneBy([
            'name' => $value
        ]);
        $entity->setPref($Pref);
    }

    private function setRealEstateNoByOrmRef($em, $entity, $value)
    {
        $RealEstate = $em->getRepository('Eccube\Entity\RealEstate')->findOneBy(
            [
                'real_estate_no' => $value
            ]
        );
        if (empty($RealEstate)) {
            return;
        }

        $entity->setRealEstate($RealEstate);
        $Office = $em->getRepository('Eccube\Entity\Office')->findOneBy(
            [
                'office_no' => $RealEstate->getOfficeNo()
            ]
        );
        $entity->setOffice($Office);
    }

    private function setZipCodeByLogic($entity, $value)
    {
        //郵便番号のハイフン除去
        $zip_code = str_replace("-", "", $value);
        //郵便番号分割
        if (mb_strlen($zip_code, "UTF-8") == 7) {
            $entity->setZip01(substr($zip_code, 0, 3));
            $entity->setZip02(substr($zip_code, 3, 4));
        }
        $entity->setZipCode($zip_code);
    }

    private function setTelByLogic($entity, $value)
    {
        //電話番号分割
        $tel = explode("-", $value);
        if (empty($tel) || count($tel) !== 3) {
            $entity->setTel01($value);
            $entity->setTel02("");
            $entity->setTel03("");
        } else {
            $entity->setTel01($tel[0]);
            $entity->setTel02($tel[1]);
            $entity->setTel03($tel[2]);
        }
    }

    private function setEmailByLogic($entity, $value)
    {
        $domain = explode("@", $value);
        if ($this->shop_id == config("SHOP_ID_SURPASSNET")) {
            $entity->setStaffFlg(0);
        } else {
            if (empty($domain) || count($domain) !== 2 || $domain[1] !== config("mail_domain")) {
                $entity->setStaffFlg(0);
            } else {
                $entity->setStaffFlg(1);
            }
        }
        $entity->setEmail($value);
    }

    private function convertUTF8(string $rowStr)
    {
        return mb_convert_encoding($rowStr, 'UTF-8', 'SJIS-win');
    }
}
