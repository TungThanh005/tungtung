<?php

namespace Eccube\Doctrine\Common\DataFixtures\ForTest;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Eccube\Entity\Member;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;

class MemberFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $app = \Eccube\Application::getInstance();
        $app->initialize();

        $member = new Member();
        $Work = $app['orm.em']->getRepository('Eccube\Entity\Master\Work')->findOneBy([
                'id' => $app['faker']->numberBetween(0, 1)
            ]);
        $member->setWork($Work);
        $Authority = $app['orm.em']->getRepository('Eccube\Entity\Master\Authority')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 6)
            ]);
        $member->setAuthority($Authority);
        //$member->setCreator($manager->merge($this->getReference('creator1')));
        $member->setName($app['faker']->name);
        $member->setLoginId($app['faker']->numberBetween(1000, 10000));
        $member->setPassword($app['faker']->password);
        $member->setSalt($app['faker']->text);
        $member->setRank($app['faker']->numberBetween(1, 6));
        $member->setDelFlg(0);
        $manager->persist($member);
        $manager->flush();
    }

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}
