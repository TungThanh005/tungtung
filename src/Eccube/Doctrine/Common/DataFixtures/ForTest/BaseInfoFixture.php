<?php

namespace Eccube\Doctrine\Common\DataFixtures\ForTest;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Eccube\Entity\BaseInfo;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;

class BaseInfoFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $app = \Eccube\Application::getInstance();
        $app->initialize();

        $base_info = new BaseInfo();
        $Pref = $app['orm.em']->getRepository('Eccube\Entity\Master\Pref')->findOneBy([
                'id' => $app['faker']->numberBetween(1, 47)
            ]);
        $base_info->setPref($Pref);
        $Country = $app['orm.em']->getRepository('Eccube\Entity\Master\Country')->findOneBy([
                'id' => 392 // 日本
            ]);
        $base_info->setCountry($Country);//日本
        /*$base_info->setCompanyName();
        $base_info->setCompanyKana();
        $base_info->setZip01();
        $base_info->setZip02();
        $base_info->setZipcode();
        $base_info->setAddr01();
        $base_info->setAddr02();
        $base_info->setTel01();
        $base_info->setTel02();
        $base_info->setTel03();
        $base_info->setFax01();
        $base_info->setFax02();
        $base_info->setFax03();
        $base_info->setBusinessHour();
        $base_info->setEmail01();
        $base_info->setEmail02();
        $base_info->setEmail03();
        $base_info->setEmail04();
        $base_info->setShopCode();
        $base_info->setShopDomain();
        $base_info->setShopKana();
        $base_info->setShopNameEng();*/
        $base_info->setShopName($app['faker']->company);
        $base_info->setUpdateDate($app['faker']->dateTime->format('Y-m-d H:i:s'));
        /*$base_info->setGoodTraded();
        $base_info->setMessage();
        $base_info->setLatitude();
        $base_info->setLongitude();
        $base_info->setDeliveryFreeAmount();
        $base_info->setDeliveryFreeQuantity();
        $base_info->setOptionMultipleShipping();
        $base_info->setOptionMypageOrderStatusDisplay();
        $base_info->setNostockHidden();
        $base_info->setOptionFavoriteProduct();
        $base_info->setOptionProductDeliveryFee();
        $base_info->setOptionProductTaxRule();
        $base_info->setOptionCustomerActivate();
        $base_info->setOptionAllowGuest();
        $base_info->setOptionAllowUserRegister();
        $base_info->setOptionRememberMe();
        $base_info->setOptionAllowPoint();
        $base_info->setAuthenticationKey();*/



        $manager->persist($base_info);
        $manager->flush();
        $this->addReference('base_info1', $base_info);

    }

    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}
