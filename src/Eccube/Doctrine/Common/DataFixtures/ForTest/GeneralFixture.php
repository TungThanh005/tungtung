<?php

namespace Eccube\Doctrine\Common\DataFixtures\ForTest;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Eccube\Entity\Member;

class GeneralFixture implements FixtureInterface
{

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
   
        $Connection = $manager->getConnection();
        $Connection->beginTransaction();
        
        $app = \Eccube\Application::getInstance();
        $app->initialize();

        //SQLでいろいろ自由に入れる用　Sample
        $params = [
            'id' => $work_id,
            'name' => str_replace('"', '', $app['faker']->name),
            'rank' => $app['faker']->numberBetween(1, 6),
        ];
        $sql = $this->getSql('mtb_work', $params);
        $prepare = $Connection->prepare($sql);
        $result = $prepare->execute();
        $Connection->commit();
    }

    /**
     * INSERT を生成する.
     *
     * @param string $table_name テーブル名
     * @param array $params カラム名と値の連想配列
     * @return string INSERT 文
     */
    public function getSql($table_name, array $params)
    {
        return 'INSERT INTO '.$table_name.' ('.implode(', ', array_keys($params)).') VALUES ("'.implode('" , "', array_values($params)).'")';
    }

}
