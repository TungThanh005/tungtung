$(function() {
	$(".categorySwitch").on("click", function() {
		$(this).next().slideToggle();
		$(this).toggleClass("open");
	});
	return false;
});

$(function() {
	var menu = $('#sideMenu');
	var body = $(document.body);
	var menuWidth = 270;
	// メニューボタンをクリックした時の動き
	$('#sideButton').on('click', function() {
		// body に open クラスを付与する
		body.toggleClass('open');
		if (body.hasClass('open')) {
			// open クラスが body についていたらメニューをスライドインする
			body.animate({
				'right': menuWidth
			}, 300);
			menu.animate({
				'right': 0
			}, 300);
		} else {
			// open クラスが body についていなかったらスライドアウトする
			menu.animate({
				'right': -menuWidth
			}, 300);
			body.animate({
				'right': 0
			}, 300);
		}
	});
});

// scroll
$(function(){
  $('a[href^="#"]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});
